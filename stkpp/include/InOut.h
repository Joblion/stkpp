/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2017  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::InOut
 * created on: 14 juil. 2017
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file InOut.h
 *  @brief This file include all the header files of the project InOut.
 **/

/**
 * @defgroup InOut Input-Output
 * @brief The project InOut propose a set of classes for performing usual
 * input/output operations.
 **/

/** @ingroup InOut
 *  @namespace STK::InOut
 *  @brief This namespace encloses all variables and constant specific to the
 *  InOut project.
 **/



#ifndef INOUT_H
#define INOUT_H

#include <InOut/include/STK_AdditiveBSplineRegressionPage.h>
#include <InOut/include/STK_LocalVariancePage.h>

#endif /* INOUT_H */
