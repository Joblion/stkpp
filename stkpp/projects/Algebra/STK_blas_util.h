/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2023  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*    Project: stkpp::Algebra
 * created on: Feb 17, 2023
 *     Author: iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file STK_blas_util.h
 *  @brief In this file we  we define utilities classes and methods
 *  for the interface with blas.
 **/



#ifndef STK_BLAS_UTIL_H_
#define STK_BLAS_UTIL_H_


namespace STK
{
namespace blas
{
/** @ingroup Algebra
 *  @brief wrapper of the BLAS gemm routine: performs one of the matrix-matrix operations
@verbatim
    C := alpha*op( A )*op( B ) + beta*C,

 where  op( X ) is one of

    op( X ) = X   or   op( X ) = X**T,
@verbatim
alpha and beta are scalars, and A, B and C are matrices, with op( A )
an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
@endverbatim

@param[in] TRANSA  CHARACTER*1
@verbatim
           On entry, TRANSA specifies the form of op( A ) to be used in
           the matrix multiplication as follows:

           TRANSA = 'N' or 'n',  op( A ) = A.
           TRANSA = 'T' or 't',  op( A ) = A**T.
           TRANSA = 'C' or 'c',  op( A ) = A**T.
@endverbatim
@param[in] TRANSB Char[0]
@verbatim
           On entry, TRANSB specifies the form of op( B ) to be used in
           the matrix multiplication as follows:

           TRANSB = 'N' or 'n',  op( B ) = B.
           TRANSB = 'T' or 't',  op( B ) = B**T.
           TRANSB = 'C' or 'c',  op( B ) = B**T.
@endverbatim
@param[in]  M int
@verbatim
           On entry,  M  specifies  the number  of rows  of the  matrix
           op( A )  and of the  matrix  C.  M  must  be at least  zero.
@endverbatim
@param[in]  N int
@verbatim
           On entry,  N  specifies the number  of columns of the matrix
           op( B ) and the number of columns of the matrix C. N must be
           at least zero.
@endverbatim
@param[in]  K int
@verbatim
           On entry,  K  specifies  the number of columns of the matrix
           op( A ) and the number of rows of the matrix op( B ). K must
           be at least  zero.
@endverbatim

@param[in]  ALPHA Real.
@verbatim
           On entry, ALPHA specifies the scalar alpha.
@endverbatim
@param[in] A a Real array of dimension (LDA, ka), where ka is k when
           TRANSA = 'N' or 'n',  and is  m  otherwise.
@verbatim
           Before entry with  TRANSA = 'N' or 'n',  the leading  m by k
           part of the array  A  must contain the matrix  A,  otherwise
           the leading  k by m  part of the array  A  must contain  the
           matrix A.
@endverbatim
@param[in] LDA int
@verbatim
           On entry, LDA specifies the first dimension of A as declared
           in the calling (sub) program. When  TRANSA = 'N' or 'n' then
           LDA must be at least  max( 1, m ), otherwise  LDA must be at
           least  max( 1, k ).
@endverbatim
@param[in] B Real array of dimension (LDB, kb), where kb is
           n  when  TRANSB = 'N' or 'n',  and is  k  otherwise.
@verbatim
           Before entry with  TRANSB = 'N' or 'n',  the leading  k by n
           part of the array  B  must contain the matrix  B,  otherwise
           the leading  n by k  part of the array  B  must contain  the
           matrix B.
@endverbatim
@param[in] LDB INTEGER
@verbatim
           On entry, LDB specifies the first dimension of B as declared
           in the calling (sub) program. When  TRANSB = 'N' or 'n' then
           LDB must be at least  max( 1, k ), otherwise  LDB must be at
           least  max( 1, n ).
@endverbatim
@param[in]  BETA DOUBLE PRECISION.
@verbatim
           On entry,  BETA  specifies the scalar  beta.  When  BETA  is
           supplied as zero then C need not be set on input.
@endverbatim
@param[in,out]  C Real array of  dimension (LDC, N)
@verbatim
           Before entry, the leading  m by n  part of the array  C must
           contain the matrix  C,  except when  beta  is zero, in which
           case C need not be set on entry.
           On exit, the array  C  is overwritten by the  m by n  matrix
           ( alpha*op( A )*op( B ) + beta*C ).
@endverbatim
@param[in]  LDC int
@verbatim
           On entry, LDC specifies the first dimension of C as declared
           in  the  calling  (sub)  program.   LDC  must  be  at  least
           max( 1, m ).
@endverbatim
**/
inline void dgemm( int m, int n, int nrhs
                , Real * a, int lda
                , Real * b, int ldb
                , Real * s
                , Real *rcond, int *rank
                , Real *work, int lWork, int* iwork)
{
  int info = 1;
#ifdef STKUSEBLAS
#ifdef STKREALAREFLOAT
  sgemm_( &m, &n, &nrhs, a, &lda, b, &ldb, s, rcond, rank, work, &lWork, iwork, &info);
#else
  dgemm_( &m, &n, &nrhs
          , a, &lda
          , b, &ldb
          , s
          , rcond, rank
          , work, &lWork, iwork, &info);
#endif
#endif
  return info;
}

/** @ingroup Algebra
 *  @brief wrapper of the BLAS gesv routine:
 *
**/
void dgesv_(int * n, int * nrhs, double * A, int * lda,
              int * ipiv, double * B, int * ldb, int * info);
} // namespace blas
} // namespace SK

#endif /* STK_BLAS_UTIL_H_ */
