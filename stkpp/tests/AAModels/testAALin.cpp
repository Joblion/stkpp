/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)                                   */

/* Project : AAModels
 * File    : testAALin.cpp
 * Contents: test program for the class AALinModel
 * Author  : Serge Iovleff
*/

#include <STKpp.h>

using namespace STK;

/* main print */
template< class TYPE, template<class> class Container2D >
void printHead(Container2D<TYPE> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")        << A.isRef()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T(" | ");
  stk_cout << name << _T(".beginCols() =")   << A.beginCols()  << _T(" ");
  stk_cout << name << _T(".lastIdxCols() =")      << A.lastIdxCols()  << _T(" ");
  stk_cout << name << _T(".sizeCols() =")      << A.sizeCols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T(" | ");
  stk_cout << name << _T(".beginRows() =")   << A.beginRows()  << _T(" ");
  stk_cout << name << _T(".lastIdxRows() =")      << A.lastIdxRows()  << _T(" ");
  stk_cout << name << _T(".sizeRows() =")      << A.sizeRows()  << _T("\n");
  stk_cout << name << _T(".rangeCols().isRef() =")  << A.rangeCols().isRef() << _T("\n");
  stk_cout << name << _T(".rangeCols() =\n")  << A.rangeCols() << _T("\n");
}

template< class TYPE, template<class> class Container2D >
void print(Container2D<TYPE> const& A, String const& name)
{
  printHead(A, name);
  stk_cout << name << _T("=\n")               << A << _T(_T("\n\n"));
}

template< class Array >
void testLAAM()
{
  Array A(Range(1,300), Range(1,3));
  RandBase ran;
  // simulate 1D manifold
  for (int i=1; i<=100; i++)
  {
    Real z = 8.* ran.randUnif() -4.;
    A(i, 1) = sin((double)z*(4.*Const::_PI_))/2. + ran.randGauss(0., 0.1);
    A(i, 2) = cos((double)z*(4.*Const::_PI_))/2. + ran.randGauss(0., 0.1);
    A(i, 3) = z + ran.randGauss(0., 0.1);
  }
  // simulate 2D manifold
  for (int i=101; i<=300; i++)
  {
    Real x = 2.* ran.randUnif() - 1.;
    Real y = 2 * ran.randUnif() - 1;
    double xd = (double)x, yd = (double)y;
    A(i, 3) = (cos(Const::_PI_*sqrt(xd*xd + yd*yd))* (1. - exp(-64*(xd*xd + yd*yd))))
               / 2.0
            + ran.randGauss(0., 0.1);
    A(i, 1) = x + ran.randGauss(0., 0.1);
    A(i, 2) = y + ran.randGauss(0., 0.1);
  }

  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ Test LinearAAModel class.                          \n";
  LocalVariance<Array> Ind(A, Reduct::distance_, 2);
  LinearAAModel<Array> test(A);
  //test.center();
  test.setReducer(&Ind);
  if (!test.run(1))
  {
    stk_cerr << _T("An error occur.\nWhat: ") << test.error();
    STKRUNTIME_ERROR_NO_ARG(testLAAM,test.run failed);
  }
  PointX statistics = Stat::variance(*(test.p_residuals()));
  stk_cout << "residualVariance = \n" << statistics << _T("\n";);

  stk_cout << _T("\nTest LinearAAModel class with weights \n");
  VectorX weights(A.rows(), 1.);
  weights.sub(Range(1,100)) *=100.0;
  weights /= 10200.0;
  if (!test.run(weights, 1))
  {
    stk_cerr << _T("An error occur.\nWhat: ") << test.error();
    STKRUNTIME_ERROR_NO_ARG(testLAAM,test.run failed);
  }
  stk_cout << "results with first manifold weighted\n";
  stk_cout << "cov =\n" << Ind.covariance() << _T("\n";);
  stk_cout << "local cov =\n" << Ind.localCovariance() << _T("\n";);
  stk_cout << "Axis =\n" << Ind.axis() << _T("\n";);
  stk_cout << "Index values =\n" << Ind.criteriaValues() << _T("\n";);
  stk_cout << "Projected covariance =\n" << test.projectedCovariance() << _T("\n";);
  stk_cout << "Residual covariance =\n" << test.residualCovariance() << _T("\n";);

  weights = 1.0;
  weights.sub(Range(101,200)) *= 100.0;
  weights /= 20100.0;
  if (!test.run(weights, 2))
  {
    stk_cerr << _T("An error occur.\nWhat: ") << test.error();
    STKRUNTIME_ERROR_NO_ARG(testLAAM,test.run failed);
  }
  stk_cout << _T("results with second manifold weighted\n");
  stk_cout << "cov =\n" << Ind.covariance() << _T("\n";);
  stk_cout << "local cov =\n" << Ind.localCovariance() << _T("\n";);
  stk_cout << "Axis =\n" << Ind.axis() << _T("\n";);
  stk_cout << "Index values =\n" << Ind.criteriaValues() << _T("\n";);
  stk_cout << "Projected covariance =\n" << test.projectedCovariance() << _T("\n";);
  stk_cout << "Residual covariance =\n" << test.residualCovariance() << _T("\n";);

  stk_cout << "+End of Test LinearAAModel class (small data set)   +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
}

/* main. */
int main(int argc, char *argv[])
{
  try
  {
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";)
  stk_cout << _T("+ TestAALin                                         +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test LocalVariance class (on a small data set)    +\n");
  ArrayXX A(Range(1,5),Range(1,2));
  A <<-5.0, -5.1
     ,-1.5, -1.4
     , 0.0,  0.1
     , 1.0,  1.0
     , 5.0,  4.9;

  stk_cout << _T("A =\n") << A << _T("\n");
  LocalVariance<ArrayXX> Ind(A, Reduct::distance_, 2);
  Ind.setDimension(2);
  Ind.run();
  stk_cout << _T("pred = \n") << Ind.pred() << _T("\n";);
  stk_cout << _T("covariance =\n") << Ind.covariance() << _T("\n";);
  stk_cout << _T("local covariance =\n") << Ind.localCovariance() << _T("\n";);
  stk_cout << _T("criteria Values = ") << Ind.criteriaValues() << _T("\n";);
  stk_cout << _T("Axis =\n") << Ind.axis() << _T("\n";);
  stk_cout << _T("\n\n");
  stk_cout << "+ End of Test LocalVariance class (small data set)  +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test LAAM with Array2D                            +\n");
  testLAAM<ArrayXX>();
  stk_cout << _T("\n\n");
  stk_cout << "+ End of Test LAAM with Array2D                        +\n";
  stk_cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test LAAM with CArrayXX                           +\n");
  testLAAM<CArrayXX>();
  stk_cout << _T("\n\n");
  stk_cout << "+ End of Test LAAM with CArrayXX                       +\n";
  stk_cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

  stk_cout << _T("\n\n");
  stk_cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "Test LinearAAmodel simulation method\n";
  A.resize(Range(1,300), Range(1,3));
  Law::Normal law(0.0, 4.0);
  ArrayXX proj(Range(1,3), Range(1,2));
  VectorX mean(Range(1,3));
  mean.rand(law);
  stk_cout << "mean =" << mean;
  stk_cout << "std = 1.0\n";
  LinearAAModel<ArrayXX>::simul(law, mean, 1.0, proj, A);

  stk_cout << "Writing simulated data set to simul2.csv file\n";
  ExportToCsv csv_file2(A);
  csv_file2.p_readWriteCsv()->write("./tests/data/simul2.csv");
  stk_cout << "+ End of simulation test. \n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

  stk_cout << _T("\n\n");
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+   Successful completion of testing for TestAALin  +\n";
  stk_cout << "+   No errors detected.                             +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    std::cerr << error.error() << _T("\n";);
    return -1;
  }

  return 0;
}
