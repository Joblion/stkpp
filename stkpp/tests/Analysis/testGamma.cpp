/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA
    
    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Analysis
 * Purpose:  test program for testing Analysis classes and methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testAnalysis.cpp
 *  @brief In this file we test the Analysis classes (special functions).
 **/

#include "STKpp.h"

//#include <cmath>
#include <iomanip>

#include "../../../../../workspace/RInsideTests/Rmath.h"

using namespace STK;

/** test the Gamma function
 **/
static void testFunctGamma(Real const& Emax)
{
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Integer values.                             +\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("|\n\n");
  stk_cout << _T("| Test gamma\n");
  stk_cout << _T("|           x")
            << _T("|     gammafn")
            << _T("|     gamma  ")
            << _T("|     error  ")
            << _T("|\n");

  for (int i = 1; i<=171; i++)
  {
    Real x = i;
    Real error = ( gammafn(x)-Funct::gamma_raw(x) )/ gammafn(x);
      stk_cout << _T("|") << std::setw(12) << x
               << _T("|") << std::setw(12) << gammafn(x)
               << _T("|") << std::setw(12) << Funct::gamma_raw(x)
               << _T("|") << std::setw(12) << error
               << _T("|\n");
  }
  stk_cout << _T("|\n\n");
  stk_cout << _T("| Test lgamma\n");
  stk_cout << _T("|           x")
           << _T("|    lgammafn")
           << _T("|    lgamma ")
           << _T("|     error  ")
           << _T("|\n");
  for (int i = 2; i<=300; i++)
  {
    Real x = i;
    Real error = ( lgammafn(x)-Funct::lgamma_raw(x) )/ lgammafn(x);
    stk_cout << _T("|") << std::setw(12) << x
    << _T("|") << std::setw(12) << lgammafn(x)
    << _T("|") << std::setw(12) << Funct::lgamma(x)
    << _T("|") << std::setw(12) << error
    << _T("|\n");
  }
  stk_cout << _T("|\n\n");
  stk_cout << _T("+ End of test Integer values.                      +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Integer + 1/2 values.                       +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("| Test gamma\n");
  stk_cout << _T("|           x")
            << _T("|     gammafn")
            << _T("|     gamma  ")
            << _T("|     error  ")
            << _T("|\n");

  for (int i = -171; i<=171; i++)
  {
    Real x = i + 0.5;
    Real error = ( gammafn(x)-Funct::gamma_raw(x) )/ gammafn(x);
      stk_cout << _T("|") << std::setw(12) << x
               << _T("|") << std::setw(12) << gammafn(x)
               << _T("|") << std::setw(12) << Funct::gamma_raw(x)
               << _T("|") << std::setw(12) << error
               << _T("|\n");
  }
  stk_cout << _T("|\n\n");
  stk_cout << _T("| Test lgamma\n");
  stk_cout << _T("|           x")
           << _T("|    lgammafn")
           << _T("|    lgamma ")
           << _T("|     error  ")
           << _T("|\n");
  for (int i = -171; i<300; i++)
  {
    Real x = i+0.5;
    Real error = ( lgammafn(x)-Funct::lgamma_raw(x) )/ lgammafn(x);
    stk_cout << _T("|") << std::setw(12) << x
    << _T("|") << std::setw(12) << lgammafn(x)
    << _T("|") << std::setw(12) << Funct::lgamma(x)
    << _T("|") << std::setw(12) << error
    << _T("|\n");
  }
  stk_cout << _T("+ End of test Integer + 1/2 values.                +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("|\n\n");
  return;
}


// Main
int main(int argc, char *argv[])
{
  Real Emax = 1e-13;
  try
  {
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::Analysis.                            +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");

    testFunctGamma(Emax);
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for Analysis  +\n");
    stk_cout << _T("+               STK Analysis                     +\n");
    stk_cout << _T("+ No errors detected.                            +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n");
  }
  return 0;
}

