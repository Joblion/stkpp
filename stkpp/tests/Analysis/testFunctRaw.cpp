/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::Analysis
 * created on: 6 oct. 2013
 * Author:   iovleff,  S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testFunctRaw.cpp
 *  @brief In this file we test some of the raw function.
 **/

#include "Analysis.h"
#include "STatistiK.h"
#include "Arrays.h"
#include <STKernel.h>

#include <iomanip>

#include "../../../../../workspace/RInsideTests/Rmath.h"

using namespace STK;

int N = 100;
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test raw functions                             +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    Array2DVector<Real> rp1(N), rp2(N), rx(N), res1, res2;
    Array2DVector<int> ip1(N), ip2(N);

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test binomial_pdf_raw functions                +\n");
    Law::Uniform lunif(0, 1000), lunifx(0,1);
    rp1.rand(lunif);
    rp2.rand(lunif);
    rx.rand(lunifx);
    // write results
    res1.resize(N*N*N);
    res2.resize(N*N*N);
    int l = res1.begin();
    for (int i=rp1.begin(); i<rp1.end(); i++)
      for (int j=rp2.begin(); j<rp2.end(); j++)
      {
        Real x = std::floor(std::min(rp1[i], rp2[j]));
        Real n = std::floor(std::max(rp1[i], rp2[j]));
        int ix = (int)x;
        int in = (int)n;
        for (int k=rx.begin(); k<rx.end(); k++)
        {
          res1[l] = Funct::binomial_pdf_raw(x, n, rx[k]) - dbinom(x, n, rx[k], false);
          res2[l] = Funct::binomial_pdf_raw(ix, in, rx[k]) - dbinom(ix, in, rx[k], false);
          if (std::abs(res1[l]) > 0.1)
          {
            stk_cout << _T("An error occur\n")
                     << _T("l =") << l << _T("\n")
                     << _T("x =") << x << _T(", n =") << n << _T(", p =") << rx[l] << _T("\n")
                     << _T("binomial_pdf_raw(x, n, p) =") << Funct::binomial_pdf_raw(x, n, rx[k]) << _T("\n")
                     << _T("dbinom(x, n, p) =") << dbinom(x, n, rx[k], false) << _T("\n");
            return 0;
          }
          l++;
        }
      }
    stk_cout << _T("+ Test binomial_pdf_raw functions terminated     +\n");
    stk_cout << _T("res1.abs().maxElt() =") << res1.abs().maxElt() << _T("\n");
    stk_cout << _T("res1.mean() =") << res1.mean() << _T("\n");
    stk_cout << _T("res2.abs().maxElt() =") << res2.abs().maxElt() << _T("\n");
    stk_cout << _T("res2.mean() =") << res2.mean() << _T("\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test b1 function                               +\n");
    l = res1.begin();
    for (int i=rp1.begin(); i<rp1.end(); i++)
      for (int j=rp2.begin(); j<rp2.end(); j++)
      {
        Real x = std::floor(std::min(rp1[i], rp2[j])), n = std::floor(std::max(rp1[i], rp2[j]));
        if (x == 0) { x += 1; }
        Real y = n-x;
        if (y == 0) { y+= 1; x-=1;}
        for (int k=rx.begin(); k<rx.end(); k++)
        {
          res1[l] = Funct::b1(x, y, rx[k], false) - dbinom(x, n, rx[k], false)*(x*y/n);
          if (std::abs(res1[l]) > 0.1)
          {
            stk_cout << _T("An error occur\n")
                     << _T("l =") << l << _T("\n")
                     << _T("x =") << rx[l] << _T("\n")
                     << _T("b1(x, y, rx[k], false) =") << Funct::b1(x, y, rx[k], false)  << _T("\n")
                     << _T("dbinom(x, n, rx[k], false)*(x*y/n) =") << dbinom(x, n, rx[k], false)*(x*y/n) << _T("\n");
            return 0;
          }
          l++;
        }
      }
    stk_cout << _T("+ Test b1 function terminated                    +\n");
    stk_cout << _T("res1.abs().maxElt() =") << res1.abs().maxElt() << _T("\n");
    stk_cout << _T("res1.mean() =") << res1.mean() << _T("\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test beta_pdf_raw function                     +\n");
    l = res1.begin();
    for (int i=rp1.begin(); i<rp1.end(); i++)
      for (int j=rp2.begin(); j<rp2.end(); j++)
      {
        for (int k=rx.begin(); k<rx.end(); k++)
        {
          res1[l] = Funct::beta_pdf_raw(rx[k], rp1[i], rp2[j]) - dbeta(rx[k], rp1[i], rp2[j], false);
          if (std::abs(res1[l]) > 0.1)
          {
            stk_cout << _T("An error occur\n")
                     << _T("l =") << l << _T("\n")
                     << _T("rx[l] =") << rx[l] << _T(", rp1[i] =") << rp1[i]  << _T(", rp2[j] =") << rp2[j] << _T("\n")
                     << _T("Funct::beta_pdf_raw(rx[k], rp1[i], rp2[j]) =") << Funct::beta_pdf_raw(rx[k], rp1[i], rp2[j])  << _T("\n")
                     << _T("dbeta(rx[k], rp1[i], rp2[j], false) =") << dbeta(rx[k], rp1[i], rp2[j], false) << _T("\n");
            return 0;
          }
          l++;
        }
      }
    stk_cout << _T("+ Test beta_pdf_raw function terminated                    +\n");
    stk_cout << _T("res1.abs().maxElt() =") << res1.abs().maxElt() << _T("\n");
    stk_cout << _T("res1.mean() =") << res1.mean() << _T("\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing betaRatio.    +\n");
    stk_cout << _T("+ No errors detected.                            +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n");
  }
return 0;
}



