/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 6 mai 2014
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file SimulUtil.cpp
 *  @brief In this file we implement the mixture simulation utilities function.
 **/

#include "SimulUtil.h"
#include "STatistiK.h"

using namespace STK;
using namespace STK::Law;

/** Add randomly missing values to the array dataij */
void simulMissingZi(STK::Real prop, STK::CVectorXi& zi)
{
  int nbMiss  = zi.size() * prop;
  STK::CVectorXi index(zi.range());
  for (int i= index.begin(); i< index.end(); ++i) index[i] = i;

  int last=index.end()-1;
  for (int i=index.begin(); i< index.end(); ++i)
  {
    int ind = STK::Law::UniformDiscrete::rand(i, last);
    std::swap(index[i], index[ind]);
  }
  for (int i=index.begin(); i< index.begin()+nbMiss; ++i)
  { zi[index[i]] = STK::Arithmetic<int>::NA();}
}

//--------------------------------Categorical---------------------------
/* Simulate a Categorical model */
void simulCategoricalModel( int K, int d, int Lmax, STK::Clust::Mixture model, VectorOfArray& proba)
{
  switch (model)
  {
    case Clust::Categorical_pjk_:
      simulCategorical_pjkModel(K, d, Lmax, proba);
      break;
    case Clust::Categorical_pk_:
      simulCategorical_pkModel(K, d, Lmax, proba);
      break;
    default:
      break;
  }
}

/* Simulate a MixtureCategorical_pjk mixture model */
void simulCategorical_pjkModel( int K, int d, int Lmax, VectorOfArray& proba)
{
  proba.resize(K);
  for (int k= proba.begin(); k < proba.end(); ++k)
  {
    proba[k].resize(Lmax, d);
    proba[k].setZeros();
  }
  //
  Range vars(d);
  for (int j= vars.begin(); j < vars.end(); ++j)
  {
    // create represents the probability of the number of modalities
    VectorX p(Range(2,Lmax, 0));
    p.randUnif();  // generate numbers in ]0,1[
    p /= p.sum(); // normalize to 1
    Law::Categorical law(p);
    int L = law.rand(); // sample number of modalities of the jth variable
    VectorX lawj(L);
    // for each
    for (int k= proba.begin(); k < proba.end(); ++k)
    {
      lawj.randUnif();
      lawj /= lawj.sum();
      proba[k].col(lawj.range(), j) = lawj;
    }
  }
}

/* Simulate a MixtureCategorical_pk mixture model */
void simulCategorical_pkModel( int K, int d, int L, VectorOfArray& proba)
{
  // create arrays of porbabilities
  proba.resize(K);
  for (int k= proba.begin(); k < proba.end(); ++k)
  {
    proba[k].resize(L,d);
    VectorX prob(L);
    prob.randUnif();    // generate numbers in ]0,1[
    prob /= prob.sum(); // normalize to 1
    for (int j=proba[k].beginCols(); j< proba[k].endCols(); ++j)
    { proba[k].col(j) = prob;}
  }
}

/* Simulate a Categorical mixture data set with given parameter and class */
void simulCategoricalMixture( int n, int d, VectorOfArray const& proba
                            , VectorXi const& z, Array2D<int>& dataij)
{
  dataij.resize(n, d);
  for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
    for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    {
      dataij(i,j) = Law::Categorical::rand(proba[z[i]].col(j));
    }
}

//--------------------------------Gaussian---------------------------
/* Simulate a Gaussian model */
void simulGaussianModel( int K, int d, STK::Clust::Mixture model, VectorOfVector& mu, VectorOfVector& sigma)
{
  switch (model)
  {
    case Clust::Gaussian_sjk_:
      simulGaussian_sjkModel( K, d, mu, sigma);
      break;
    case Clust::Gaussian_sj_:
      simulGaussian_sjModel(K, d, mu, sigma);
      break;
    case Clust::Gaussian_sk_:
      simulGaussian_skModel(K, d, mu, sigma);
      break;
    case Clust::Gaussian_s_:
      simulGaussian_sModel(K, d, mu, sigma);
      break;
    default:
      break;
  }
}

/* Simulate a Gaussian mixture data set */
void simulGaussian_sjkModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma)
{
  // create arrays of porbabilities
  mu.resize(K);
  sigma.resize(K);
  // generate moments
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    mu[k].resize(d); sigma[k].resize(d);
    for (int j= mu[k].begin(); j < mu[k].end(); ++j)
    {
      mu[k][j]   = Law::Normal::rand(0.,10.);
      sigma[k][j] = Law::Exponential::rand(1.);
    }
  }
}
/* Simulate a MixtureGaussian_sj mixture model */
void simulGaussian_sjModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma)
{
  mu.resize(K);
  sigma.resize(K);
  VectorX s(d);
  for (int j= s.begin(); j < s.end(); ++j)
  { s[j] = Law::Exponential::rand(1.);}
  // generate moments
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    mu[k].resize(d); sigma[k].resize(d);
    for (int j= mu[k].begin(); j < mu[k].end(); ++j)
    { mu[k][j]   = Law::Normal::rand(0.,10.);}
    sigma[k] = s;
  }
}
/* Simulate a MixtureGaussian_sk mixture model */
void simulGaussian_skModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma)
{
  mu.resize(K);
  sigma.resize(K);
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    mu[k].resize(d); sigma[k].resize(d);
    for (int j= mu[k].begin(); j < mu[k].end(); ++j)
    { mu[k][j]   = Law::Normal::rand(0.,10.);}
    sigma[k] = Law::Exponential::rand(1.);
  }
}
/* Simulate a MixtureGaussian_sk mixture model */
void simulGaussian_sModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma)
{
  mu.resize(K);
  sigma.resize(K);
  Real s = Law::Exponential::rand(1.);
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    mu[k].resize(d); sigma[k].resize(d);
    for (int j= mu[k].begin(); j < mu[k].end(); ++j)
    { mu[k][j]   = Law::Normal::rand(0.,10.);}
    sigma[k] = s;
  }
}

/** Simulate a Poisson  model */
void simulPoissonModel( int K, int d, STK::Clust::Mixture model, VectorOfVector& lambda)
{
  switch (model)
  {
    case Clust::Poisson_ljk_:
      simulPoisson_ljkModel(K, d, lambda);
      break;
    case Clust::Poisson_lk_:
      simulPoisson_lkModel(K, d, lambda);
      break;
    case Clust::Poisson_ljlk_:
      simulPoisson_ljlkModel(K, d, lambda);
      break;
    default:
      break;
  }

}

/* Simulate a MixturePoisson_ljk mixture model */
void simulPoisson_ljkModel( int K, int d, VectorOfVector& lambda)
{
  lambda.resize(K);
  // generate moments
  for (int k= lambda.begin(); k < lambda.end(); ++k)
  {
    lambda[k].resize(d);
    for (int j= lambda[k].begin(); j < lambda[k].end(); ++j)
    {
      lambda[k][j] = Law::Exponential::rand(1.);
    }
  }
}
/* Simulate a MixturePoisson_ljk mixture model */
void simulPoisson_lkModel( int K, int d, VectorOfVector& lambda)
{
  lambda.resize(K);
  // generate moments
  for (int k= lambda.begin(); k < lambda.end(); ++k)
  {
    lambda[k].resize(d);
    lambda[k] = Law::Exponential::rand(1.);
  }
}

/** Simulate a MixturePoisson_ljk mixture model */
void simulPoisson_ljlkModel( int K, int d, VectorOfVector& lambda)
{
  lambda.resize(K);
  VectorX lambdak(K);
  // generate moments
  for (int k= lambda.begin(); k < lambda.end(); ++k)
  {
    lambda[k].resize(d);
    lambdak[k] = Law::Exponential::rand(1.);
  }
  Range var(d);
  for (int j= var.begin(); j < var.end(); ++j)
  {
    Real lambdaj = Law::Exponential::rand(1.);
    for (int k= lambda.begin(); k < lambda.end(); ++k)
    {
      lambda[k][j] = lambdak[k]* lambdaj;
    }
  }
}

/* Simulate a Poisson mixture data set with given parameters and class */
void simulPoissonMixture( int n, int d, VectorOfVector const& lambda
                        , VectorXi const& zi, ArrayXXi& dataij)
{
  dataij.resize(n, d);
  for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
    for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    { dataij(i,j) = Law::Poisson::rand(lambda[zi[i]][j]);}
}

/* Simulate a Gaussian mixture data set with given parameters and class */
void simulGammaMixture( int n, int d, VectorOfVector const& shape, VectorOfVector const& scale
                         , VectorXi const& zi, ArrayXX& dataij)
{
  dataij.resize(n, d);
  for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
    for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    { dataij(i,j) = Law::Gamma::rand(shape[zi[i]][j], scale[zi[i]][j]);}
}


//--------------------------Old simulation method--------------------------
/* Simulate a MixtureCategorical_pjk mixture data set */
void simulCategorical_pkMixture( int n, int d, VectorOfArray const& proba, VectorX const& pk
                               , Array2D<int>& dataij, Array2DVector<int>& zi)
{
  simulZi(n, pk, zi);
  // create Categorical laws
  simulCategoricalMixture(n, d, proba, zi, dataij);
}


/* Simulate a Gaussian mixture data set */
void simulGaussian_sModel( int K, Array2Param& param)
{
  // create arrays of porbabilities
  param.resize(K);
  Real s = Law::Exponential::rand(1.);
  for (int k= baseIdx; k < param.end(); ++k)
  {
    param[k][baseIdx]   = Law::Normal::rand(0.,10.);
    param[k][baseIdx+1] = s;
  }
}

/** Simulate a Gaussian mixture data set with uinique variance */
void simulGaussian_sMixture( int n, int d, Array2Param const& param, VectorX const& pk
                           , Array2D<Real>& dataij, Array2DVector<int>& zi)
{
  // create Categorical and Gaussian laws
  Categorical zilaw(pk);
  Array1D< Law::Gaussian > law(param.range());
  for (int k= baseIdx; k < param.end(); ++k)
  { law[k].setMu(param[k][baseIdx]);
    law[k].setSigma(param[k][baseIdx+1]);
  }
  // generate data
  zi.resize(n);
  dataij.resize(n, d);
  for (int i= baseIdx; i < dataij.endRows(); ++i)
  {
    int k= zilaw.rand();
    zi[i] = k;
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    { dataij(i,j) = law[k].rand();}
  }
}

/** Simulate a MixturePoisson_lk mixture model */
void simulPoisson_Model( int K, VectorX& lambda)
{
  // create arrays of lambdas
  lambda.resize(K);
  Law::Exponential law(3);
  lambda.rand(law);
}

/** Simulate a MixturePoisson_ljlk  mixture data set */
void simulPoisson_ljlkMixture( int n, int d, VectorX const& lambda, STK::VectorX const& pk
                         , STK::ArrayXXi& dataij, STK::VectorXi& zi)
{
  // create Categorical
  Categorical zilaw(pk);
  // create Poisson laws a (dxK) array with \lambda_j\lmabda_k = \lambda_{jk}
  Array2D< Law::Poisson > laws(lambda.range(), dataij.cols());
  for (int k= lambda.begin(); k < lambda.end(); ++k)
  {
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    {
      laws(k,j).setLambda( lambda[k] * (j+1));
    }
  }
  // generate data
  zi.resize(n);
  dataij.resize(n, d);
  for (int i= baseIdx; i < dataij.endRows(); ++i)
  {
    int k= zilaw.rand();
    zi[i] = k;
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    { dataij(i,j) = laws(k,j).rand();}
  }
}

/** Simulate a MixturePoisson_lk  mixture data set */
void simulPoisson_lkMixture( int n, int d, VectorX const& lambda, STK::VectorX const& pk
                           , STK::ArrayXXi& dataij, STK::VectorXi& zi)
{
  // create Categorical
  Categorical zilaw(pk);
  // create Poisson laws a (dxK) array with \lambda_j\lmabda_k = \lambda_{jk}
  Array2DPoint< Law::Poisson > laws(lambda.range());
  for (int k= lambda.begin(); k < lambda.end(); ++k)
  { laws[k].setLambda( lambda[k] );}
  // generate data
  zi.resize(n);
  dataij.resize(n, d);
  for (int i= baseIdx; i < dataij.endRows(); ++i)
  {
    int k= zilaw.rand();
    zi[i] = k;
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    { dataij(i,j) = laws[k].rand();}
  }
}

/** Simulate the scales of the gamma mixture model */
static void simulGamma_bk( Array2Param& param)
{
  for (int k= baseIdx; k < param.end(); ++k)
  { param[k][baseIdx+1] = Law::Exponential::rand(1.);}
}

/** Simulate the scale of the gamma mixture model */
static void simulGamma_b( Array2Param& param)
{
  Real scale = Law::Exponential::rand(1.);
  for (int k= baseIdx; k < param.end(); ++k)
  { param[k][baseIdx+1] = scale;}
}

/** Simulate the scales of the gamma mixture model */
static void simulGamma_ak( Array2Param& param)
{
  for (int k= baseIdx; k < param.end(); ++k)
  { param[k][baseIdx] = Law::Exponential::rand(10.);}
}

/** Simulate the scale of the gamma mixture model */
static void simulGamma_a( Array2Param& param)
{
  Real scale = Law::Exponential::rand(10.);
  for (int k= baseIdx; k < param.end(); ++k)
  { param[k][baseIdx] = scale;}
}

/* Simulate a gamma_a_bk mixture model */
void simulGamma_a_bk_Model( int K, Array2Param& param)
{
  // create arrays of probabilities
  param.resize(K);
  simulGamma_a(param);
  simulGamma_bk(param);
}

/* Simulate a gamma_ak_bk mixture model */
void simulGamma_ak_bk_Model( int K, Array2Param& param)
{
  // create arrays of probabilities
  param.resize(K);
  simulGamma_ak(param);
  simulGamma_bk(param);
}

/* Simulate a gamma_ak_b mixture model */
void simulGamma_ak_b_Model( int K, Array2Param& param)
{
  // create arrays of probabilities
  param.resize(K);
  simulGamma_ak(param);
  simulGamma_b(param);
}

/* Simulate a gamma_bk mixture data set */
void simulGamma_Mixture( int n, int d
                       , Array2Param const& param
                       , VectorX const& pk
                       , Array2D<Real>& dataij
                       , Array2DVector<int>& zi)
{
  simulZi(n, pk, zi);
  // create Categorical laws
  Array1D< Law::Gamma > law(param.range());
  for (int k= baseIdx; k < param.end(); ++k)
  { law[k].setShape(param[k][baseIdx]);
    law[k].setScale(param[k][baseIdx+1]);
  }
  // generate data
  dataij.resize(n, d);
  for (int i= baseIdx; i < dataij.endRows(); ++i)
  {
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    { dataij(i,j) = law[zi[i]].rand();}
  }
}

/** Simulate a gamma_bk mixture model */
void simulGamma_bkModel( int d, int K, Array2Param& param)
{
  // create arrays of probabilities
  param.resize(K);
  for (int k= baseIdx; k < param.end(); ++k)
  {
    param[k][baseIdx]   = Law::Exponential::rand(10.);
    param[k][baseIdx+1] = Law::Exponential::rand(1.);
  }
}

/** Simulate a gamma_b mixture model */
void simulGamma_bModel( int d, int K, Array2Param& param)
{
  // create arrays of probabilities
  param.resize(K);
  Real scale = Law::Exponential::rand(1.);
  for (int k= baseIdx; k < param.end(); ++k)
  {
    param[k][baseIdx]   = Law::Exponential::rand(10.);
    param[k][baseIdx+1] = scale;
  }
}

/** Simulate a gamma_bk mixture data set */
void simulGamma_bkMixture( int n, int d, Array2Param const& param, VectorX const& pk
                         , Array2D<Real>& dataij, Array2DVector<int>& zi)
{
  simulZi(n, pk, zi);
  // create Categorical laws
  Array1D< Law::Gamma > law(param.range());
  for (int k= baseIdx; k < param.end(); ++k)
  { law[k].setShape(param[k][baseIdx]);
    law[k].setScale(param[k][baseIdx+1]);
  }
  // generate data
  dataij.resize(n, d);
  for (int i= baseIdx; i < dataij.endRows(); ++i)
  {
    for (int j= baseIdx; j < dataij.endCols(); ++j)
    { dataij(i,j) = law[zi[i]].rand();}
  }
}


