/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllPoissonMixtureModels.cpp
 *  @brief In this file we test the Poisson Mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"


using namespace STK;
void estimateModel( std::string const& idData,std::string const& idModel
                  , ArrayXXi const& data
                  , DataHandler& handler
                  , PoissonMixtureManager<DataHandler>& manager
                  , IMixtureComposer*& composer
                  , Array2DVector<Real> const& pk
                  , VectorX const& lambda
                  )
{
  handler.readDataFromArray2D(data, idData, idModel); // register data in handler
  if (composer->createMixture(manager, idData))
  {
    stk_cout << _T("+ createMixture done.                               +\n");
    stk_cout << _T("data=\n");
    stk_cout << manager.getData(idData).sub(10, Range(4));
    testFullStrategy(composer, Clust::randomParamInit_, 10, Clust::emAlgo_
                    , 1, 2, 2
                    , Clust::semiSemAlgo_
                    , Clust::semiSemAlgo_
                    );
    MixtureComposerFixedProp* p_composerFixed = static_cast<MixtureComposerFixedProp*>(composer);
    stk_cout << _T("+ True Parameters of the model.                     +\n");
    stk_cout << _T("Proportions = ") << pk.transpose() << _T("\n");
    for (int k= lambda.begin(); k < lambda.end(); ++k)
    {
      stk_cout << _T("---> Component ") << k << _T("\n");
      stk_cout << _T("Parameter[1] =")<< lambda[k] << _T("\n");
    }
    ArrayXX param;
    stk_cout << _T("+++++++++++++++++++\n");
    p_composerFixed->getParameters(manager,idData, param);
    stk_cout << _T("Test getParameters:\n") << param;
    stk_cout << _T("+++++++++++++++++++\n");
    stk_cout << _T("Test setParameters\n");
    p_composerFixed->setParameters(manager,idData, param);
    stk_cout <<  _T("getParameters:\n") << param;
    p_composerFixed->getParameters(manager,idData, param);
  }
  else
  {
    stk_cout << _T("+estimateModel: An error occur in createMixture !!!!  +\n");
  }
}


int n=5000, d=4, K=3;
Real pmiss = 0.1;


int main(int argc, char *argv[])
{
  if (argc >=5)
  {
    n = atoi(argv[1]);
    d = atoi(argv[2]);
    K = atoi(argv[3]);
    pmiss = atof(argv[4]);
  }
  // arrays with simulated data
  ArrayXXi data(n,d), data_lk(n,d);
  VectorX  pk(n);
  VectorXi zi(n);
  // parameters of each variables
  VectorX lambda(K);
  // simulate pk (uniform Dirichlet if false)
  simulZiLaw(K, pk, true);
  ArrayXX param;

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test PoissonMixture models                           +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create DataHandler, MixtureManager, FixedPropComposer+\n");
    std::string idData, idModel;
    DataHandler handler;
    PoissonMixtureManager<DataHandler> manager(handler);
    IMixtureComposer* composer = new MixtureComposerFixedProp(n, K);
    MixtureComposerFixedProp* p_composerFixed = static_cast<MixtureComposerFixedProp*>(composer);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ simulate Poisson data set                            +\n");
    // generate model
    simulPoisson_Model(K, lambda);
    stk_cout << _T("lambda = ") << lambda.transpose();
    // generate data
    simulPoisson_ljlkMixture(n, d, lambda, pk, data, zi);
    simulPoisson_lkMixture(n, d, lambda, pk, data_lk, zi);
    // add missing values
    simulMissingData(pmiss, data);
    simulMissingData(pmiss, data_lk);
    //stk_cout << _T("data = ") << data.transpose();

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Poisson_ljk                        +\n");
    idData = "IdPoisson_ljk"; idModel ="Poisson_ljk";
    estimateModel( idData, idModel, data, handler, manager, composer, pk, lambda);

    p_composerFixed = static_cast<MixtureComposerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Poisson_ljlk                       +\n");
    idData = "IdPoisson_ljlk"; idModel ="Poisson_ljlk";
    estimateModel( idData, idModel, data, handler, manager, composer, pk, lambda);

    p_composerFixed = static_cast<MixtureComposerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Poisson_lk                         +\n");
    idData = "IdPoisson_lk"; idModel ="Poisson_lk";
    estimateModel( idData, idModel, data_lk, handler, manager, composer, pk, lambda);

    p_composerFixed = static_cast<MixtureComposerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    delete composer;
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for Poisson         +\n");
  stk_cout << _T("+ mixture models.                                      +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

