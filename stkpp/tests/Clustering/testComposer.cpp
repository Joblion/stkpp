/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * Purpose:  test the Composer
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 *
 **/

/** @file testComposer.cpp
 *  @brief In this file we test the composer
 **/
#include "StrategiesUtil.h"
#include "DManager.h"

using namespace STK;


int main(int argc, char *argv[])
{
  int nbCluster = 1;
  MixtureComposerFacade<DataHandler> facade1;
  MixtureComposerFacade<DataHandler> facade2;
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test composer                                     +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Read first file                                   +\n");
  if (!facade1.handler_.readDataFromCsvFile( "./tests/Clustering/data/composite1.csv"
                                           , "./tests/Clustering/data/descriptor1.csv"))
  { return -1;}
  if (!facade2.handler_.readDataFromCsvFile( "./tests/Clustering/data/composite1.csv"
                                           , "./tests/Clustering/data/descriptor1.csv"))
  { return -1;}
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Read second file                                   +\n");
  if (!facade1.handler_.readDataFromCsvFile( "./tests/Clustering/data/composite2.csv"
                                           , "./tests/Clustering/data/descriptor2.csv"))
  { return -1;}
  if (!facade1.handler_.readDataFromCsvFile( "./tests/Clustering/data/composite2.csv"
                                           , "./tests/Clustering/data/descriptor2.csv"))
  { return -1;}
  facade1.handler_.writeInfo(std::cout);
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Create facade1, facade2 and Mixtures              +\n");
  facade1.createMixtureComposer(nbCluster);
  facade2.createMixtureComposerFixedProp(nbCluster);
  facade1.createMixtures();
  facade2.createMixtures();
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Testing strategies: creating composer and mixtures+\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ testSimpleStrategy: facade1                       +\n");
  testSimpleStrategy(facade1.p_composer_);
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ testSimpleStrategy: facade2                       +\n");
  testSimpleStrategy(facade2.p_composer_);

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ testFullStrategy: facade1                        +\n");
  testFullStrategy(facade1.p_composer_, Clust::randomClassInit_
                  , 5, Clust::semAlgo_
                  , 2, 10, 5
                  , Clust::semiSemAlgo_
                  , Clust::semiSemAlgo_);

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ testFullStrategy: facade2.p_composer()             +\n");
  testFullStrategy(facade2.p_composer_, Clust::randomClassInit_
                  , 5, Clust::semAlgo_
                  , 2, 10, 5
                  , Clust::semiSemAlgo_
                  , Clust::semiSemAlgo_);
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ End of test composer : no error detected          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

