/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllKernelMixtureModels.cpp
 *  @brief In this file we test the Kernel Mixture models estimation methods.
 **/
#include <STKpp.h>
#include "DataSets.h"

using namespace STK;

void testKMM( IMixtureComposer*& p_composer
            , Clust::initType init, int nbTryInInit
            , int nbTry
            , int nbInitRun
            , int nbShortRun
            )
{
  StrategyFacade facade(p_composer);
  facade.createFullStrategy( Clust::randomClassInit_, nbTryInInit, Clust::emAlgo_, 5, 0
                           , nbTry, nbInitRun, nbShortRun
                           , Clust::emAlgo_, 5, 0.04
                           , Clust::emAlgo_, 5, 1e-08);
  if (!facade.run())
  { stk_cout << _T("+ FullStrategy terminated without success\n");}
  else
  { stk_cout << _T("+ FullStrategy terminated with success\n");}
  stk_cout << _T("Computed lnLikelihood =") <<p_composer->computeLnLikelihood() << _T("\n");
  p_composer->writeParameters(stk_cout);
  stk_cout << _T("\n\n");
}


bool launchTest( IMixtureComposer*& p_icomposer, KernelMixtureManager& manager, std::string const& idData)
{
  ArrayXX param1, param2;
  MixtureComposer* p_composer = static_cast<MixtureComposer*>(p_icomposer);
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ create mixture, set dimension, set kernel         +\n");
  if (!p_composer->createMixture(manager, idData))
  {
    stk_cout << _T("An error occurs in p_composer->createMixture(manager,") << idData << _T("\n");
    return false;
  };
  manager.setDim(p_composer->getMixture(idData), 10);
//  Kernel::IKernel const* p_kernel = manager.p_handler()->getKernel(idData);
//  manager.setKernel(p_composer->getMixture(idData), p_kernel);
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++\n");
  stk_cout << _T("+ launch estimation +\n");
  testKMM(p_icomposer, Clust::randomFuzzyInit_, 1, 1, 5, 5);
  stk_cout << _T("+ Get results\n");
  p_composer = static_cast<MixtureComposer*>(p_icomposer);
  stk_cout << _T("zi = ") << p_composer->zi().transpose() << "\n";
  p_composer->getParameters(manager, idData, param1);
  stk_cout << _T("param1=\n") << param1;
  stk_cout << _T("setParameters and getParameters\n");
  p_composer->setParameters(manager,idData, param1);
  stk_cout << _T("writeParameters\n");
  p_composer->writeParameters(stk_cout);
  stk_cout << _T("getParameters\n");
  p_composer->getParameters(manager,idData, param2);
  stk_cout << _T("param2=\n") << param2;
  stk_cout <<  _T("max (param1-param2):\n") << (param1-param2).abs().maxElt() << _T("\n");
  stk_cout << _T("+ remove mixture +\n");
  p_composer->removeMixture(manager, idData);
  stk_cout << _T("+ estimation done +\n");
  stk_cout << _T("+++++++++++++++++++\n");
  return true;
}

int K=2;

int main(int argc, char *argv[])
{
  if (argc >=4)
  { K = atoi(argv[3]);}

  typedef CArray<double, 320, UnknownSize> Array;
  KernelHandler handler;
  CArray<double, 320, UnknownSize> data(320, 3);
  BullEyes(data);

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Gaussian Kernel models with bullsEye data    +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    ArrayXX param;

    // get data using 2 first columns
    ArrayXX data2;
    data2 = data.col(Range(2));
    data = data2;
    stk_cout << data.rows() << ", " << data.cols()<<"\n";

    Kernel::Gaussian<Array>*          p_kerGauss   = new Kernel::Gaussian<Array>(data);
    Kernel::Laplace<Array>*           p_kerLaplace = new Kernel::Laplace<Array>(data);
    Kernel::Linear<Array>*            p_kerLin     = new Kernel::Linear<Array>(data);
    Kernel::Polynomial<Array>*        p_kerPoly    = new Kernel::Polynomial<Array>(data, 2., 3);
    Kernel::RationalQuadratic<Array>* p_kerRat     = new Kernel::RationalQuadratic<Array>(data, 1.);
    p_kerGauss->run();
    p_kerLin->run();
    p_kerPoly->run();
    p_kerRat->run();

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Set bullEyes kernels to data handler +\n");

    std::string idModel_pk_sk("Kmm_sk"), idModel_p_sk("Kmm_sk");
    std::string idModel_pk_s("Kmm_s"),   idModel_p_s("Kmm_s");

    std::string idGauss_s("idGauss_s"), idGauss_sk("idGauss_sk");
    std::string idLaplace_s("IdLaplace_s"), idLaplace_sk("IdLaplace_sk");
    std::string idLin_s("IdLin_s"), idLin_sk("IdLin_sk");
    std::string idPoly_s("IdPoly_s"), idPoly_sk("IdPoly_sk");
    std::string idRat_s("idRat_s"), idRat_sk("idRat_sk");

    handler.addKernel(p_kerGauss,   idGauss_sk,   idModel_pk_sk);
    handler.addKernel(p_kerGauss,   idGauss_s,    idModel_pk_s);
    handler.addKernel(p_kerLaplace, idLaplace_sk, idModel_pk_sk);
    handler.addKernel(p_kerLaplace, idLaplace_s,  idModel_pk_s);
    handler.addKernel(p_kerPoly,    idPoly_sk,    idModel_pk_sk);
    handler.addKernel(p_kerPoly,    idPoly_s,     idModel_pk_s);
    handler.addKernel(p_kerRat,     idRat_sk,     idModel_pk_sk);
    handler.addKernel(p_kerRat,     idRat_s,      idModel_pk_s);

    stk_cout << _T("handler.nbSample() = ") << handler.nbSample() << _T("\n");
    stk_cout << _T("\nhandler.writeInfo(stk_cout)\n");
    handler.writeInfo(stk_cout);

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ CreateKernelMixtureManager and composer +\n");
    KernelMixtureManager manager(handler);
    IMixtureComposer* p_icomposer = new MixtureComposer(handler.nbSample(), K);
    stk_cout << _T("+ Launch test for ") << idGauss_sk << _T("\n");
    launchTest(p_icomposer, manager, idGauss_sk);
    stk_cout << _T("+ Launch test for ") << idGauss_sk << _T(" done\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Launch test for ") << idRat_sk << _T("\n");
    launchTest(p_icomposer, manager, idRat_sk);
    stk_cout << _T("+ Launch test for ") << idRat_sk << _T(" done\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Launch test for ") << idLaplace_s << _T("\n");
    launchTest(p_icomposer, manager, idLaplace_s);
    stk_cout << _T("+ Launch test for ") << idLaplace_s << _T(" done\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");


    delete p_icomposer;
  }
  catch (Exception const& e)
  {
    stk_cerr << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                 +\n");
  stk_cout << _T("+ Kernel Mixture Models.                               +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

