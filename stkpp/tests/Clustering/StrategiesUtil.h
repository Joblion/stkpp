/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Purpose:  test the Normal and MultiLaw::Normal classes.
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 *
 **/

/** @file StrategiesUtil.h
 *  @brief Common functions used for testing mixture models
 **/

#ifndef STRATEGIESUTIL_H
#define STRATEGIESUTIL_H

#include "DManager.h"
#include "Clustering.h"

// Gaussian and gamma models
typedef STK::Array1D< STK::CArrayVector<STK::Real, 2> > Array2Param;

namespace STK
{

/** Test a simple strategy estimation
 * @param p_composer an instance of a composer
 **/
void testSimpleStrategy( IMixtureComposer*& p_composer);

/** Test a full strategy estimation
 * @param p_composer an instance of a composer
 **/
void testFullStrategy( IMixtureComposer*& p_composer
                     , Clust::initType init = Clust::randomClassInit_
                     , int nbTryInInit =5, Clust::algoType initAlgo = Clust::semAlgo_
                     , int nbTry =2, int nbInitRun =10, int nbShortRun =5
                     , Clust::algoType shortAlgo= Clust::semiSemAlgo_
                     , Clust::algoType longAlgo= Clust::semiSemAlgo_);

void testPredict( MixtureComposerFacade<DataHandler>& facade
                , int nbBurn = 50
                , int nbLong = 100);

template<class DerivedManager>
void estimateModel( std::string const& idData
                  , std::string const& idModel
                  , Array2D<Real> const& data
                  , IMixtureComposer* p_composer
                  , DataHandler& handler
                  , IMixtureManager<DerivedManager>& manager
                  )
{
  // auxiliary array
  ArrayXX   parameters;
  // reference data in handler
  handler.readDataFromArray2D(data, idData, idModel);
  // create mixture and launch full strategy
  p_composer->createMixture(manager, idData);
  testFullStrategy( p_composer, Clust::randomParamInit_, 5, Clust::emAlgo_
                  , 2, 5, 5
                  , Clust::semiSemAlgo_
                  , Clust::semiSemAlgo_);

  stk_cout << _T("+++++++++++++++++++\n");
  p_composer->getParameters(manager, idData, parameters);
  stk_cout << _T("getParameters:\n") << parameters;

  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test setParameters. getParameters:\n");
  p_composer->setParameters(manager, idData, parameters);
  p_composer->getParameters(manager, idData, parameters);
  stk_cout << parameters;
}

template<class DerivedManager>
void predict( IMixtureComposer* const& p_composer
            , std::string   const& idData
            , std::string   const& idModel
            , CVectorXi     const& zi
            , Array2D<Real> const& data2
            , CVectorXi     const& zi2
            , IMixtureManager<DerivedManager> const& manager
            )
{
  stk_cout << _T("+ Create MixtureComposerFacade\n");
  MixtureComposerFacade<DataHandler> facade;
  stk_cout << _T("+ Add new data set to handler in facade\n");
  facade.handler_.readDataFromArray2D(data2, idData, idModel);
  stk_cout << _T("+ Create composer in facade\n");
  facade.createMixtureComposer(p_composer->nbCluster());
  stk_cout << _T("+ Create mixtures in facade\n");
  facade.createMixtures();

  // auxiliary array
  ArrayXX   parameters;
  stk_cout << _T("+ getParameters from existing model\n");
  p_composer->getParameters(manager,idData, parameters);
  stk_cout << _T("+ setting Parameters to facade\n");
  facade.setParameters( idData, parameters);
  stk_cout << _T("+ setting proportions to facade\n");
  facade.setProportions(p_composer->pk());
  stk_cout << _T("+ write parameters from facade\n");
  facade.p_composer()->writeParameters(stk_cout);

  stk_cout << _T("+ launching testPredict with nbIterBurn=20 and nbIterLong=100\n");
  testPredict(facade, 20, 100);
  stk_cout << _T("True Labels     =\n")<< zi2.transpose();
  stk_cout << _T("Predicted Labels=\n")<< facade.p_composer()->zi().transpose();
  // delete composer from facade
  facade.deleteComposer();
}


} // namespace STK

#endif
