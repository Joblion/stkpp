/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllGaussianMixtureModels.cpp
 *  @brief In this file we test the Gaussian Mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;

int n=300, d=4, K=3, npred = 50;
Real pmiss = 0.2;

DataHandler handler;
IMixtureComposer* p_composer;

int main(int argc, char *argv[])
{
  if (argc >=5)
  {
    n = atoi(argv[1]);
    d = atoi(argv[2]);
    K = atoi(argv[3]);
    pmiss = atof(argv[4]);
  }
  std::string idData, idModel;

  // arrays with model
  CVectorX  pk(K);
  VectorOfVector mu(K);
  VectorOfVector sigma(K);

  // arrays with data
  CVectorXi zi(n), zi2(npred);
  ArrayXX data(n,d), data2(npred, d);

  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test GaussianMixture models                          +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Simulate Gaussian_s mixture data set                 +\n");
  // simulate pk (uniform Derichlet if false) ad class labels
  simulZiLaw(K, pk, true);
  simulGaussianModel(K, d, Clust::Gaussian_s_, mu, sigma);
  stk_cout << _T("+ True Parameters of the model.                        +\n");
  stk_cout << "Proportions =\n" << pk;
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    stk_cout << _T("---> Component ") << k << _T("\n");
    stk_cout << _T("mean  = ")<< mu[k].transpose();
    stk_cout << _T("sigma = ")<< sigma[k].transpose();
  }
  // simulate data sets
  simulZi(n, pk, zi);
  simulZi(npred, pk, zi2);
  simulGaussianMixture(mu, sigma, zi, data);
  simulGaussianMixture(mu, sigma, zi2, data2);

  // add missing values
  simulMissingData(pmiss, data);
  stk_cout << _T("data(1:10,:) =\n")<< data(Range(10), data.cols());
  simulMissingData(pmiss, data2);
  stk_cout << _T("data2(1:10,:) =\n")<< data2(Range(10), data.cols());
  stk_cout << _T("zi2=\n")<< zi2.transpose();
  stk_cout << _T("+ Simulation done                                      +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create DataHandler, MixtureManager, FixedPropComposer+\n");
    DiagGaussianMixtureManager<DataHandler> manager(handler);
    p_composer = new MixtureComposerFixedProp(n, K);
    stk_cout << _T("+ Creation done                                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sjk                       +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    idData = "IdGaussian_sjk";
    idModel ="Gaussian_sjk";

    stk_cout << _T("+ estimateModel: Gaussian_sjk +\n");
    estimateModel( idData, idModel, data, p_composer, handler, manager);

    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testPredict: Gaussian_sjk   +\n");
    predict(p_composer, idData, idModel, zi, data2, zi2, manager);

    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ remove mixture: Gaussian_sjk+\n");
    p_composer->removeMixture(manager, idData);
    handler.removeData(idData);


    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sk                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    idData = "IdGaussian_sk";
    idModel ="Gaussian_sk";

    stk_cout << _T("+ estimateModel: Gaussian_sk  +\n");
    estimateModel( idData, idModel, data, p_composer, handler, manager);
    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testPredict: Gaussian_sk    +\n");
    predict(p_composer, idData, idModel, zi, data2, zi2, manager);

    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ remove mixture: Gaussian_sk +\n");
    p_composer->removeMixture(manager, idData);
    handler.removeData(idData);


    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sj                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    idData = "IdGaussian_sj";
    idModel ="Gaussian_sj";

    stk_cout << _T("+ estimateModel: Gaussian_sj  +\n");
    estimateModel( idData, idModel, data, p_composer, handler, manager);
    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testPredict: Gaussian_sj    +\n");
    predict(p_composer, idData, idModel, zi, data2, zi2, manager);

    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ remove mixture: Gaussian_sj +\n");
    p_composer->removeMixture(manager, idData);
    handler.removeData(idData);


    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_s                         +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    idData = "IdGaussian_s";
    idModel ="Gaussian_s";

    stk_cout << _T("+ estimateModel: Gaussian_s   +\n");
    estimateModel( idData, idModel, data, p_composer, handler, manager);
    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testPredict: Gaussian_s     +\n");
    predict(p_composer, idData, idModel, zi, data2, zi2, manager);

    stk_cout << _T("+++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ remove mixture: Gaussian_s  +\n");
    p_composer->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Delete FixedPropComposer                             +\n");
    delete p_composer;
    stk_cout << _T("+ Deletion done                                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                 +\n");
  stk_cout << _T("+ GaussianMixture Models.                              +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

