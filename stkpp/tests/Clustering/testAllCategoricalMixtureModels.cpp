/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 mai 2014
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testCategoricalMixtureModels.cpp
 *  @brief In this file we test the Categorical mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;

int n=300, d=4, K=3, npred = 50;
int L=5;
Real pmiss = 0.2;

DataHandler handler;
IMixtureComposer* p_composer;
MixtureComposerFixedProp* p_composerFixed;


void predict( std::string const& idData
            , std::string const& idModel
            , CVectorXi const& zi
            , Array2D<int> const& data2
            , CVectorXi const& zi2
            , CategoricalMixtureManager<DataHandler> const& manager
            )
{
  stk_cout << _T("+ Create facade\n");
  MixtureComposerFacade<DataHandler> facade(new MixtureComposer(npred, K));
  stk_cout << _T("+ Add new data set to handler\n");
  facade.handler_.readDataFromArray2D(data2, idData, idModel);
  stk_cout << _T("+ Create mixtures\n");
  facade.createMixtures();

  // auxiliary array
  ArrayXX   parameters;
  stk_cout << _T("+ getParameters\n");
  p_composer->getParameters(manager,idData, parameters);
  stk_cout << _T("+ setting Parameters\n");
  facade.setParameters( idData, parameters);
  stk_cout << _T("+ setting proportions\n");
  facade.setProportions(p_composerFixed->pk());
  stk_cout << _T("+ write parameters\n");
  facade.p_composer()->writeParameters(stk_cout);

  stk_cout << _T("+ launching testPredict\n");
  testPredict(facade, 20, 100);
  stk_cout << _T("True Labels     =\n")<< zi2.transpose();
  stk_cout << _T("Predicted Labels=\n")<< facade.p_composer()->zi().transpose();
}


int main(int argc, char *argv[])
{
  // arrays with model
  VectorX  pk(K);
  VectorOfVector mu(K);
  VectorOfVector sigma(K);

  // arrays with data
  VectorXi zi(n), zi2(npred);
  Array2D<int> data2(npred, d);
  Array2D<int> c1;
  VectorOfArray proba;

  Array2D<Real> parameters;
  std::vector< std::pair< std::pair<int,int>, int > > miss;

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test CategoricalMixture models                    +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Simulate data and fill DataHandler                +\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Simulate Categorical_pk mixture data set             +\n");
    // simulate pk (uniform Derichlet if false) ad class labels
    simulZiLaw(K, pk, true);
    simulCategorical_pkModel(K, d, L, proba);
    stk_cout << _T("+ True Parameters of the model. +\n");
    stk_cout << "Proportions =\n" << pk;
    for (int k= proba.begin(); k < proba.end(); ++k)
    {
      stk_cout << _T("---> Component ") << k << _T("\n");
      stk_cout << _T("Probabilities =\n")<< proba[k];
    }

    // generate data
    simulCategorical_pkMixture(n, d, proba, pk, c1, zi);
    simulCategorical_pkMixture(npred, d, proba, pk, data2, zi2);

    writeSimulatedData(c1, 10);

    // add missing values and duplicate data set
    std::vector< std::pair< std::pair<int,int>, int > > tmiss;
    simulMissingData(pmiss, c1, tmiss);
    writeMissingValues(tmiss, 10);
    Array2D<int> c2(c1); // copy data

    // add data to datahandler
    std::string idData_pjk("IdCategorical_pjk");
    std::string idModel_pjk("Categorical_pjk");
    std::string idData_pk("IdCategorical_pk");
    std::string idModel_pk("Categorical_pk");
    handler.readDataFromArray2D(c1, idData_pjk, idModel_pjk);
    handler.readDataFromArray2D(c2, idData_pk, idModel_pk);
    handler.writeInfo(std::cout);

    CategoricalMixtureManager<DataHandler> manager(handler);
    //

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create composers.                                 +\n");
    p_composer = new MixtureComposerFixedProp(handler.nbSample(),K);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Categorical_pjk                 +\n");
    p_composerFixed = static_cast<MixtureComposerFixedProp*>(p_composer);
    p_composerFixed->createMixture(manager, idData_pjk);
    testFullStrategy( p_composer, Clust::randomClassInit_
                    , 10, Clust::semAlgo_
                    , 1, 2, 2
                    , Clust::semiSemAlgo_
                    , Clust::semiSemAlgo_
                    );
    p_composerFixed = static_cast<MixtureComposerFixedProp*>(p_composer);
//    stk_cout << _T("+++++++++++++++++++++++\n");
//    stk_cout << _T("+ Get Missing values. +\n");
//    manager.getMissingValues(idData_pjk, miss);
    writeMissingValues(miss, 10);
    stk_cout << _T("+++++++++++++++++++\n");
    p_composerFixed->getParameters(manager,idData_pjk, parameters);
    stk_cout << _T("getParameters:\n") << parameters;
    stk_cout << _T("+++++++++++++++++++\n");
    stk_cout << _T("Test setParameters\n");
    p_composerFixed->setParameters(manager,idData_pjk, parameters);
    stk_cout <<  _T("getParameters:\n") << parameters;
    p_composerFixed->getParameters(manager,idData_pjk, parameters);

    stk_cout << _T("+ testPredict: Categorical_pjk   +\n");
    predict(idData_pjk, idModel_pjk, zi, data2, zi2, manager);

    p_composerFixed->releaseMixture(idData_pjk);
    delete p_composer;

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Categorical_pk                  +\n");
    p_composer  = new MixtureComposerFixedProp(handler.nbSample(),K);
    p_composerFixed = static_cast<MixtureComposerFixedProp*>(p_composer);
    p_composerFixed->createMixture(manager, idData_pk);

    testFullStrategy(p_composer, Clust::randomClassInit_
                     , 10, Clust::semAlgo_
                     , 1, 2, 2
                     , Clust::semiSemAlgo_
                     , Clust::semiSemAlgo_);
    p_composerFixed = static_cast<MixtureComposerFixedProp*>(p_composer);
//    stk_cout << _T("+++++++++++++++++++++++\n");
//    stk_cout << _T("+ Get Missing values. +\n");
//    manager.getMissingValues(idData_pk, miss);
//    writeMissingValues(miss, 10);
    stk_cout << _T("+++++++++++++++++++\n");
    p_composerFixed->getParameters(manager,idData_pk, parameters);
    stk_cout << _T("getParameters:\n") << parameters;
    stk_cout << _T("+++++++++++++++++++\n");
    stk_cout << _T("Test setParameters\n");
    p_composerFixed->setParameters(manager,idData_pk, parameters);
    stk_cout <<  _T("getParameters:\n") << parameters;
    p_composerFixed->getParameters(manager,idData_pk, parameters);

    stk_cout << _T("+++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testPredict: Categorical_pk   +\n");
    predict(idData_pk, idModel_pk, zi, data2, zi2, manager);

    delete p_composer;
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error();
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ CategoricalMixture Models.                        +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

