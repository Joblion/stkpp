/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 6 mai 2014
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 *
 **/

/** @file SimulUtil.h
 *  @brief Common functions used for simulating mixture models
 **/

#ifndef SIMULUTIL_H
#define SIMULUTIL_H

#include <vector>
#include "Arrays.h"
#include "Clustering.h"

typedef STK::Array1D< STK::VectorX > VectorOfVector;
typedef STK::Array1D< STK::ArrayXX > VectorOfArray;

// Gaussian and gamma models
typedef STK::Array1D< STK::CArrayVector<STK::Real, 2> > Array2Param;

/** Simulate proportions of a mixture model */
template<class Array_>
void simulZiLaw( int K, Array_& pk, bool fixed = false)
{
  pk.resize(K);
  if (fixed) { pk = 1./K;}
  else
  {
    pk.randUnif();
    pk /= pk.sum();
  }
}

/** Simulate class labels */
template<class ArrayPk_,class ArrayZi_>
void simulZi( int n, ArrayPk_ const& pk, ArrayZi_& zi)
{
  STK::Law::Categorical zilaw(pk);
  zi.resize(n);
  zi.rand(zilaw);
}

/** Add randomly missing values to the array dataij */
template<typename Type>
void simulMissingData(STK::Real proba, STK::Array2D<Type>& dataij, std::vector< std::pair< std::pair<int,int>, Type > >& miss)
{
  STK::RandBase generator;
  for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
  {
    for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    {
      if(generator.randUnif()<proba)
      {
        miss.push_back( std::make_pair( std::make_pair(i, j), dataij(i,j) ));
        dataij(i,j) = STK::Arithmetic<Type>::NA();
      }
    }
  }
}

/** Add randomly missing values to the array dataij */
void simulMissingZi(STK::Real prop, STK::CVectorXi& zi);

/** Add randomly missing values to the array dataij */
template<typename Type>
void simulMissingData(STK::Real proba, STK::Array2D<Type>& dataij)
{
  STK::RandBase generator;
  for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
  {
    for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    {
      STK::Real value = generator.randUnif();
      if (value < 0) { stk_cout << value << " "; }
      if(generator.randUnif()<proba)
      { dataij(i,j) = STK::Arithmetic<Type>::NA();}
    }
  }
}

/** write the missing values to the standard output */
template<typename Type>
void writeMissingValues(std::vector< std::pair< std::pair<int,int>, Type > > const& miss, size_t max)
{
  stk_cout << _T("Missing values=\n");
  for (size_t i=0; i< std::min(miss.size(), max); ++i)
  {
    stk_cout << _T("Miss(") << miss[i].first.first << _T(",") << miss[i].first.second << _T(") = ")
             << miss[i].second << _T("\n");
  }
  stk_cout << _T("...\n\n");
}

/** write the missing values to the standard output */
template<typename Type>
void writeSimulatedData( STK::Array2D<Type> const& dataij, int max)
{
  stk_cout << _T("Simulated data values=\n");
  stk_cout << dataij.row(STK::Range(std::min(dataij.sizeRows(), max)));
  stk_cout << _T("...\n\n");
}


/** Simulate a Categorical model */
void simulCategoricalModel( int K, int d, int Lmax, STK::Clust::Mixture model, VectorOfArray& proba);
/** Simulate a MixtureCategorical_pk model */
void simulCategorical_pkModel( int K, int d, int Lmax, VectorOfArray& proba);
/** Simulate a MixtureCategorical_pjk model */
void simulCategorical_pjkModel( int K, int d, int Lmax, VectorOfArray& proba);
/** Simulate a Categorical mixture data set using known class label */
void simulCategoricalMixture( int n, int d, VectorOfArray const& proba
                            , STK::VectorXi const& z
                            , STK::ArrayXXi& dataij);

/** Simulate a Gaussian model */
void simulGaussianModel( int K, int d, STK::Clust::Mixture model, VectorOfVector& mu, VectorOfVector& sigma);

/** Simulate a Gaussian_sjk mixture model */
void simulGaussian_sjkModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma);
/** Simulate a MixtureGaussian_sj mixture model */
void simulGaussian_sjModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma);
/** Simulate a MixtureGaussian_sk mixture model */
void simulGaussian_skModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma);
/** Simulate a MixtureGaussian_sk mixture model */
void simulGaussian_sModel( int K, int d, VectorOfVector& mu, VectorOfVector& sigma);

/** Simulate the data of a Gaussian mixture data set using known class label */
template<class ArrayZi_, class ArrayDataij_>
void simulGaussianMixture( VectorOfVector const& mu, VectorOfVector const& sigma
                         , ArrayZi_ const& zi, ArrayDataij_& dataij)
{
  dataij.resize(zi.rows(), mu.front().range());
  for (int i= dataij.beginRows(); i < dataij.endRows(); ++i)
    for (int j= dataij.beginCols(); j < dataij.endCols(); ++j)
    { dataij(i,j) = STK::Law::Normal::rand(mu[zi[i]][j], sigma[zi[i]][j]);}
}

/** Simulate a Poisson  model */
void simulPoissonModel( int K, int d, STK::Clust::Mixture model, VectorOfVector& lambda);
/** Simulate a MixturePoisson_ljk mixture model */
void simulPoisson_ljkModel( int K, int d, VectorOfVector& lambda);
/** Simulate a MixturePoisson_lk mixture model */
void simulPoisson_lkModel( int K, int d, VectorOfVector& lambda);
/** Simulate a MixturePoisson_ljlk mixture model */
void simulPoisson_ljlkModel( int K, int d, VectorOfVector& lambda);



/** Simulate a Poisson mixture data set using known class label */
void simulPoissonMixture( int n, int d, VectorOfVector const& lambda
                        , STK::VectorXi const& zi, STK::ArrayXXi& dataij);



//-------------------------Old methods
/** Simulate a MixtureCategorical_pjk mixture data set */
void simulCategorical_pkMixture( int n, int d, VectorOfArray const& proba, STK::VectorX const& pk
                               , STK::ArrayXXi& dataij, STK::VectorXi& zi);

/** Simulate a Gaussian_s mixture model */
void simulGaussian_sModel( int K, Array2Param& param);
/** Simulate a Gaussian_s  mixture data set */
void simulGaussian_sMixture( int n, int d, Array2Param const& param, STK::VectorX const& pk
                           , STK::ArrayXX& dataij, STK::VectorXi& zi);

/** Simulate a MixturePoisson_lk mixture model */
void simulPoisson_Model( int K, STK::VectorX& lambda);
/** Simulate a MixturePoisson_ljlk  mixture data set */
void simulPoisson_ljlkMixture( int n, int d, STK::VectorX const& lambda, STK::VectorX const& pk
                              , STK::ArrayXXi& dataij, STK::VectorXi& zi);

/** Simulate a MixturePoisson_ljlk  mixture data set */
void simulPoisson_lkMixture( int n, int d, STK::VectorX const& lambda, STK::VectorX const& pk
                           , STK::ArrayXXi& dataij, STK::VectorXi& zi);


/** Simulate a gamma_a_bk mixture model */
void simulGamma_a_bk_Model( int K, Array2Param& param);
/** Simulate a gamma_ak_bk mixture model */
void simulGamma_ak_bk_Model( int K, Array2Param& param);
/** Simulate a gamma_ak_b mixture model */
void simulGamma_ak_b_Model( int K, Array2Param& param);

/** Simulate a Gamma mixture data set */
void simulGamma_Mixture( int n, int d, Array2Param const& param, STK::VectorX const& pk
                       , STK::ArrayXX& dataij, STK::VectorXi& zi);

/** Simulate a MixtureGamma_ajk_bj mixture model */
void simulGamma_bjModel( int d, int K, Array2Param& param);
/** Simulate a MixtureGamma_ajk_bj mixture model */
void simulGamma_bkModel( int d, int K, Array2Param& param);
/** Simulate a MixtureGamma_ajk_bk mixture data set */
void simulGamma_bkMixture( int n, int d, Array2Param const& param, STK::VectorX const& pk
                         , STK::ArrayXX& dataij, STK::VectorXi& zi);


#endif /* SIMULUTIL_H */
