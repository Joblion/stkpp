/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllGaussianLearners.cpp
 *  @brief In this file we test the gaussian Mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;


int n=300, d=4, K=3;
Real pmiss = 0.2;
DataHandler handler;

/** utility method for testing a Gaussian learner */
void learnModel( std::string const& idModel
               , ArrayXX const& data, CVectorXi const& zi, VectorX const& pk
               , VectorOfVector const& mu, VectorOfVector const& sigma
               )
{
  std::string idData = "id";
  // add data set to the data handler
  handler.readDataFromArray2D(data, idData, idModel);
  // Create gaussian mixture manager
  DiagGaussianMixtureManager<DataHandler> manager(handler);
  // create learner
  MixtureLearner learner(n, K);
  // create gaussian mixture
  if (!learner.createMixture(manager, idData))
  {
    stk_cout << _T("Warning: createMixture failed.\n");
    return;
  }
  learner.setClassLabels(zi);
  LearnFacade facade(&learner);
  facade.createSimulAlgo(Clust::simulAlgo_, 200);
  if (facade.run())
  {
    stk_cout << _T("+ Learning terminated.                                 +\n");
    learner.writeParameters(stk_cout);

    stk_cout << _T("\n");
    stk_cout << _T("+ True Parameters of the model.                        +\n");
    stk_cout << "Proportions =\n" << pk;
    for (int k= mu.begin(); k < mu.end(); ++k)
    {
      stk_cout << _T("---> Component ") << k << _T("\n");
      stk_cout << _T("mean  = ")<< mu[k].transpose();
      stk_cout << _T("sigma = ")<< sigma[k].transpose();
    }
  }
  handler.removeData(idData);
}

int main(int argc, char *argv[])
{
  if (argc >=5)
  {
    n = atoi(argv[1]);
    d = atoi(argv[2]);
    K = atoi(argv[3]);
    pmiss = atof(argv[4]);
  }

  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Gaussian mixture Learners                       +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  std::string idModel;
  CVectorX  pk(n);
  CVectorXi zi(n);
  VectorOfVector mu(K);
  VectorOfVector sigma(K);
  ArrayXX   data;

  try
  {

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Simulate Gaussian_s mixture data set                 +\n");
    // simulate pk (uniform Dirichlet if false) ad class labels
    simulZiLaw(K, pk, true);
    simulZi(n, pk, zi);
    // simul parameters model and data set
    simulGaussianModel(K, d, Clust::Gaussian_s_, mu, sigma);
    simulGaussianMixture(mu, sigma, zi, data);
    // add missing values
    simulMissingData(pmiss, data);
    stk_cout << _T("data(1:10,:) =\n")<< data(Range(10), data.cols());
    stk_cout << _T("zi[1:10] =\n")<< zi[Range(10)].transpose() << _T("\n");

    stk_cout << _T("+ Simulation done                                      +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ learn Gaussian_sjk mixture                           +\n");
    learnModel("gaussian_sjk", data, zi, pk, mu, sigma);
    stk_cout << _T("+ learn Gaussian_sjk mixture done                      +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ learn Gaussian_sk mixture                            +\n");
    learnModel("gaussian_sk", data, zi, pk, mu, sigma);
    stk_cout << _T("+ learn Gaussian_sk mixture done                       +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ learn Gaussian_sj mixture                            +\n");
    learnModel("gaussian_sj", data, zi, pk, mu, sigma);
    stk_cout << _T("+ learn Gaussian_sj mixture done                       +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ learn Gaussian_s mixture                             +\n");
    learnModel("gaussian_s", data, zi, pk, mu, sigma);
    stk_cout << _T("+ learn Gaussian_s mixture done                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for Gaussian        +\n");
  stk_cout << _T("+ mixture Learners.                                    +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

