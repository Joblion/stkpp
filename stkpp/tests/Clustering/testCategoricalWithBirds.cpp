/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 mai 2014
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testCategoricalMixtureModels.cpp
 *  @brief In this file we test the Categorical mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;

int main(int argc, char *argv[])
{
  int K=3;
  Array2D<Real> parameters;
  MixtureComposerFacade<DataHandler> facade1;
  MixtureComposerFacade<DataHandler> facade2;

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Categorical models with Birds data           +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    facade1.handler_.setWithNames(true);
    facade2.handler_.setWithNames(true);
    std::string idData_pjk("IdCategorical_pjk");
    std::string idData_pk("IdMixtureCategorical_pk");
    std::string idModel_pjk("MixtureCategorical_pjk");
    std::string idModel_pk("MixtureCategorical_pk");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Read first file                                   +\n");
    if (!facade1.handler_.readDataFromCsvFile( "./tests/data/birds.csv",idData_pjk, idModel_pjk))
    { return -1;}
    if (!facade2.handler_.readDataFromCsvFile( "./tests/data/birds.csv",idData_pk,idModel_pk))
    { return -1;}
    facade1.handler().writeInfo(std::cout);
    stk_cout << facade1.handler().data();
    facade2.handler().writeInfo(std::cout);
    stk_cout << facade2.handler().data();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create composers.                                 +\n");
    facade1.createMixtureComposer(K);
    facade2.createMixtureComposer(K);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ CreateMixtures                                    +\n");
    facade1.createMixtures();
    facade2.createMixtures();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testSimpleStrategy: facade1, categorical_pjk      +\n");
    StrategyFacade sfacade1(facade1.p_composer_);
    sfacade1.createSimpleStrategy( Clust::randomInit_, 3, Clust::semAlgo_
                                 , 20, 1e-04
                                 , 5, Clust::emAlgo_, 500, 1e-08);
    sfacade1.run();
    stk_cout << _T("+ Get parameters.                                    +\n");
    facade1.getParameters(idData_pjk, parameters);
    stk_cout << _T("parameters=\n") << parameters;

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testSimpleStrategy: facade2, categorical_pk      +\n");
    StrategyFacade sfacade2(facade2.p_composer_);
    sfacade2.createSimpleStrategy( Clust::randomInit_, 3, Clust::semAlgo_, 20, 1e-04
                               , 5, Clust::emAlgo_, 500, 1e-08);
    sfacade2.run();
    stk_cout << _T("+ Get parameters.                                    +\n");
    facade2.getParameters(idData_pk, parameters);
    stk_cout << _T("parameters=\n") << parameters;
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error();
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ CategoricalMixture Models.                        +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

