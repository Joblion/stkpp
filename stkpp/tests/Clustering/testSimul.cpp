/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * Purpose:  test the simulation
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 *
 **/

/** @file testComposer.cpp
 *  @brief In this file we test the composer
 **/
#include "SimulUtil.h"
#include "DManager.h"

using namespace STK;


int main(int argc, char *argv[])
{
  // argc must be 5,7,9,.... [ +1 filename]
  if ((argc < 5))
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Usage: testSimul n k d modelname                  +\n")
             << _T("+ [d modelname]* [filename]                         +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    return 0;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Simulation                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  // read options
  int n = atoi(argv[1]), k = atoi(argv[2]), nbModel = (argc-3)/2;
  stk_cout << _T("n=") << n << _T(", k=") << k << ", nbModel=" << nbModel << _T("\n");

  VectorXi dim(nbModel);
  CArrayVector<String> modelName(nbModel);
  for (int i=3, m = dim.begin(); i<argc; i+=2, m++)
  {
    dim[m] = atoi(argv[i]);
    modelName[m] = String(argv[i+1]);
  }
  String filename;
  if (2*(argc/2)==argc) { filename = argv[argc-1];}

  // data containers
  VectorXi zi(n);

  ArrayXXi cat, pois;
  ArrayXX gauss, gam;

  // parameters containers
  VectorOfVector ajk;    // a_jk
  VectorOfVector bjk;    // b_jk

  // simul zi
  VectorX  pk(k);     // pik
  simulZiLaw(k, pk, false);
  simulZi(n, pk, zi);
  // save zi
  ExportToCsv xptdata(zi,_T("group")), xptparam(pk,_T("prop"));
  // start
  for (int m=modelName.begin(); m<modelName.end(); m++)
  {
     Clust::Mixture model = Clust::stringToMixture(modelName[m]);
     Clust::MixtureClass modelclass = Clust::mixtureToMixtureClass(model);
     int d = dim [m];
     switch (modelclass)
    {
      // simul categorical
      case Clust::Categorical_:
      {
        VectorOfArray catprob; // pjk
        simulCategoricalModel(k, d, 8, model, catprob);
        simulCategoricalMixture(n, d, catprob, zi, cat);
        xptdata.append(cat,Clust::mixtureToString(model));
        for(int c=catprob.begin(); c<catprob.end(); c++)
        { xptparam.append(catprob[c],Clust::mixtureToString(model)+_T(" proba")+typeToString(c));}
      }
      break;
      // simul Gaussian
      case Clust::DiagGaussian_:
      {
        VectorOfVector mujk;   // mu_jk
        VectorOfVector sjk;    // s_jk
        simulGaussianModel(k, d, model, mujk, sjk);
        simulGaussianMixture(mujk, sjk, zi, gauss);
        xptdata.append(gauss,Clust::mixtureToString(model));
        for(int c=mujk.begin(); c<mujk.end(); c++)
        { xptparam.append(mujk[c],Clust::mixtureToString(model)+_T(" mu")+typeToString(c));
          xptparam.append(sjk[c],Clust::mixtureToString(model)+_T(" s")+typeToString(c));
        }
      }
      break;
      // simul Poisson
      case Clust::Poisson_:
      {
        VectorOfVector ljk;    // lambda
        simulPoissonModel(k, d, model, ljk);
        simulPoissonMixture(n,d, ljk, zi, pois);
        xptdata.append(pois,Clust::mixtureToString(model));
        for(int c=ljk.begin(); c<ljk.end(); c++)
        { xptparam.append(ljk[c],Clust::mixtureToString(model)+_T(" lambda")+typeToString(c));}
      }
      break;
      // simul categorical
      case Clust::Gamma_:
      {
      // not implemented
      }
      break;
      default:
        break;
    }
  }

  if (filename.size() == 0)
  {
    xptdata.p_readWriteCsv()->write(stk_cout);
    xptparam.p_readWriteCsv()->write(stk_cout);
  }
  else
  {
    xptdata.p_readWriteCsv()->write(filename+_T(".dat"));
    xptparam.p_readWriteCsv()->write(filename+_T(".par"));
  }

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ End of test Simulation : no error detected        +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

