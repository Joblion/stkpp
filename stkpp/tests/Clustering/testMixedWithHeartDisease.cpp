/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 mai 2014
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testMixedWithHeartDisease.cpp
 *  @brief In this file we test the mixed data mixture model with the heart desease data set.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;

int main(int argc, char *argv[])
{
  int K=3;
  if (argc >1) { K= atoi(argv[1]);}
  Array2D<Real> parameters;
  MixtureComposerFacade<DataHandler> facade;

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Mixed models with HeartDisease data  +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    facade.handler_.setWithNames(true);
    std::string idData_pjk("IdCategorical_pjk");
    std::string idData_sjk("IdGaussian_sjk");
    std::string idModel_pjk("MixtureCategorical_pjk");
    std::string idModel_sjk("Gaussian_sjk");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Read first file                                   +\n");
    if (!facade.handler_.readDataFromCsvFile( "./tests/data/HeartDisease.cat.csv",idData_pjk, idModel_pjk))
    { return -1;}
    if (!facade.handler_.readDataFromCsvFile( "./tests/data/HeartDisease.cont.csv",idData_sjk, idModel_sjk))
    { return -1;}
    facade.handler().writeInfo(std::cout);
    stk_cout << facade.handler().data();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create composers.                                 +\n");
    facade.createMixtureComposer(K);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ CreateMixtures                                    +\n");
    facade.createMixtures();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testSimpleStrategy.                               +\n");
    StrategyFacade sfacade(facade.p_composer_);
    sfacade.createSimpleStrategy( Clust::randomInit_, 2, Clust::emAlgo_, 2, 1e-04
                               , 2
                               , Clust::emAlgo_, 500, 1e-08);
    sfacade.run();
    facade.p_composer()->writeParameters(stk_cout);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy.                                 +\n");
    sfacade.createFullStrategy( Clust::randomInit_, 2, Clust::emAlgo_, 2, 1e-04
                             , 2, 2, 2
                             , Clust::semiSemAlgo_, 100, 1e-04, Clust::semiSemAlgo_, 500, 1e-07);
    sfacade.run();
    facade.p_composer()->writeParameters(stk_cout);
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error();
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ Mixed models with HeartDisease data.      +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  return 0;
}

