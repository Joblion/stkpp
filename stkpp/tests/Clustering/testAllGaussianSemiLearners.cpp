/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllGaussianMixtureModels.cpp
 *  @brief In this file we test the gaussian Mixture models estimation methods.
 **/
#include "DManager.h"
#include "StrategiesUtil.h"
#include "SimulUtil.h"

using namespace STK;

int n=300, d=4, K=3;
Real pmiss = 0.2;
DataHandler handler;

/** */
void estimateModel( std::string const& idData,std::string const& idModel
                  , Array2D<Real> const& data
                  , CVectorXi const& zi
                  , DiagGaussianMixtureManager<DataHandler>& manager
                  , IMixtureComposer*& composer
                  , Array2DVector<Real> const& pk
                  , VectorOfVector const& mu, VectorOfVector const& sigma
                  )
{
  handler.readDataFromArray2D(data, idData, idModel);
  MixtureSemiLearnerFixedProp* p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
  p_composerFixed->createMixture(manager, idData);
  p_composerFixed->setLabels(zi);
  testFullStrategy(composer, Clust::randomClassInit_, 5, Clust::emAlgo_
                   , 2, 5, 5
                   , Clust::semiSemAlgo_
                   , Clust::semiSemAlgo_);
  p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
  stk_cout << _T("zi    = ")<< zi.transpose();
  stk_cout << _T("Labels= ")<< p_composerFixed->zi().transpose();
  std::vector<int>::const_iterator it;
  for (it = p_composerFixed->unknownLabels().begin(); it != p_composerFixed->unknownLabels().end(); ++it)
  { stk_cout << *it << _T(" ");}
  stk_cout << _T("\n");
  stk_cout << _T("+ True Parameters of the model.                        +\n");
  stk_cout << _T("Proportions =\n") << pk;
  for (int k= mu.begin(); k < mu.end(); ++k)
  {
    stk_cout << _T("---> Component ") << k << _T("\n");
    stk_cout << _T("mean  = ")<< mu[k].transpose();
    stk_cout << _T("sigma = ")<< sigma[k].transpose();
  }
}

int main(int argc, char *argv[])
{
  if (argc >=5)
  {
    n = atoi(argv[1]);
    d = atoi(argv[2]);
    K = atoi(argv[3]);
    pmiss = atof(argv[4]);
  }
  std::string idData, idModel;

  // arrays with simulations
  CVectorX  pk(n);
  CVectorXi zi(n);
  VectorOfVector mu(K);
  VectorOfVector sigma(K);
  ArrayXX   data;
  ArrayXX   parameters;

  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test GaussianMixture models                          +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Simulate Gaussian_s mixture data set                 +\n");
    // simulate pk (uniform Dirichlet if false) ad class labels
    simulZiLaw(K, pk, true);
    simulZi(n, pk, zi);
    // simul parameters model and data set
    simulGaussianModel(K, d, Clust::Gaussian_s_, mu, sigma);
    simulGaussianMixture(mu, sigma, zi, data);
    // add missing values
    simulMissingData(pmiss, data);
    stk_cout << _T("data(1:10,:) =\n")<< data(Range(10), data.cols());
    simulMissingZi(0.5, zi);
    stk_cout << _T("+ Simulation done                                      +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Create DataHandler, MixtureManager, FixedPropComposer+\n");
    DiagGaussianMixtureManager<DataHandler> manager(handler);
    IMixtureComposer* composer = new MixtureSemiLearnerFixedProp(n, K);
    MixtureSemiLearnerFixedProp* p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);

    stk_cout << _T("+ Creation done                                        +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sjk                       +\n");
    idData = "IdGaussian_sjk"; idModel ="Gaussian_sjk";
    estimateModel( idData, idModel, data, zi, manager, composer, pk, mu, sigma);
    p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sk                        +\n");
    idData = "IdMixtureGaussian_sk"; idModel ="Gaussian_sk";
    estimateModel( idData, idModel, data, zi, manager, composer, pk, mu, sigma);
    p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_sj                        +\n");
    idData = "IdGaussian_sj"; idModel ="Gaussian_sj";
    estimateModel( idData, idModel, data, zi, manager, composer, pk, mu, sigma);
    p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ testFullStrategy: Gaussian_s                         +\n");
    idData = "IdGaussian_s"; idModel ="Gaussian_s";
    estimateModel( idData, idModel, data, zi, manager, composer, pk, mu, sigma);
    p_composerFixed = static_cast<MixtureSemiLearnerFixedProp*>(composer);
    p_composerFixed->removeMixture(manager, idData);
    handler.removeData(idData);

    delete composer;
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                 +\n");
  stk_cout << _T("+ GaussianMixture Models.                              +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

