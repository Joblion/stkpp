/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA
  
    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)                                   */

/* Project : stkpp::Regress
 * File    : testBSpline.cpp
 * Contents: test program for the classes AdditiveBSplineRegression
 * Author  : Serge Iovleff
*/

/** @file testAdditiveBSplineRegression.cpp test program for the additive BSpline regression */

#include <STKpp.h>

using namespace STK;

template< class Array >
void simul(Array& X, Array& Y)
{
  RandBase ran;
  for (int i=X.beginRows(); i<X.endRows(); i++)
  {
    Real x = 2.* ran.randUnif() - 1.;
    Real y = 2 * ran.randUnif() - 1;
    double xd = (double)x, yd = (double)y;
    X(i, baseIdx)    = x + ran.randGauss(0., 0.1);
    X(i, baseIdx +1) = y + ran.randGauss(0., 0.1);

    Y(i, baseIdx) = (cos(Const::_PI_*sqrt(xd*xd + yd*yd))* (1. - exp(-64*(xd*xd + yd*yd))))/ 2.0
                  + ran.randGauss(0., 0.1);
  }
}

template< class Array >
void testARS(int nbControlPoint = 7, int degree = 3)
{
  Array X(300, 2), Y(300,1);
  simul(X,Y);
  RandBase ran;
  AdditiveBSplineRegression<Array, Array> reg(Y, X, nbControlPoint, degree, Regress::uniformKnotsPositions_ );
  reg.run();
  stk_cout << _T("Mean of the residuals =") << reg.residuals().mean() << _T("\n");
  stk_cout << _T("Variance of the residuals =") << reg.residuals().variance() << _T("\n");
  Array Xex(50, 2), Yex(50,1);
  simul(Xex,Yex);
  Array Yest = reg.extrapolate(Xex);
  stk_cout << _T("Mean of the extrapolated residuals =") << (Yex-Yest).mean() << _T("\n");
  stk_cout << _T("Variance of the extrapolated residuals =") << (Yex-Yest).variance() << _T("\n");
}
// main
int main(int argc, char *argv[])
{
  int nbControlPoint = 7, degree = 3;
  if (argc > 1)
  {
    nbControlPoint = int(atoi(argv[1]));
  }
  if (argc > 2)
  {
    degree = int(atoi(argv[2]));
  }

  try
  {
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::AdditiveBSplineRegression                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  stk_cout << _T("==================================================\n");
  stk_cout << _T("Test AdditiveBSplineRegression class with ArrayXX \n");
  stk_cout << _T("==================================================\n");
  testARS<ArrayXX>(nbControlPoint, degree);

  stk_cout << "\n\n";
  stk_cout << _T("===================================================\n");
  stk_cout << _T("Test AdditiveBSplineRegression class with CArrayXX \n");
  stk_cout << _T("===================================================\n");
  testARS<CArrayXX>(nbControlPoint, degree);

  stk_cout << "\n\n";
  stk_cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ Successful completion for AdditiveBSplineRegression. +\n";
  stk_cout << "+ No errors detected.                                  +\n";
  stk_cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "\n\n";
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
  }
  return 0;
}
