/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)                                   */

/* Project : stkpp::Regress
 * File    : testBSpline.cpp
 * Contents: test program for the classes BSplineCoefficients and
 * BSplineRegression
 * Author  : Serge Iovleff
*/

/** @file testBSpline.cpp test programm for the additive BSpline regression */
#include <STKpp.h>

using namespace STK;

template<class YArray, class XVector>
void testBspline(int nbControlPoint, int degree, int n)
{
    typedef BSplineRegression<YArray, XVector> Regressor;
    stk_cout << "\n\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Simulating 1D manifold                             \n";

    RandBase ran;

    YArray y(Range(1,n), Range(1,3));
    XVector x(Range(1,n), 0.);
    Real step = 1./n;
    // simulate 1D manifold
    for (int i=1; i<=n; i++)
    {
      Real z = -0.5 + i * step;
      x[i] = z;
      y(i, 1) = sin((double)z*(3.*Const::_PI_))/2. + ran.randGauss(0., 0.1);
      y(i, 2) = cos((double)z*(3.*Const::_PI_))/2. + ran.randGauss(0., 0.1);
      y(i, 3) = z + ran.randGauss(0., 0.1);
    }
    stk_cout << _T("\n";);
    stk_cout << "+ end of Simulating 1D manifold                     +\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

    stk_cout << "\n\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Estimating 1D manifold with uniform knots          \n";
    Regressor a_reg_unif(&y, &x, nbControlPoint, degree, Regress::uniformKnotsPositions_);
    a_reg_unif.run();
    // test extrapolate
    YArray Y, R;
    Y = a_reg_unif.extrapolate(x);
    R = *(a_reg_unif.p_predicted()) - Y;
    PointX mu;
    mu = Stat::mean(R);
    stk_cout << "Mean of the predicted values - extrapolated values = \n";
    stk_cout << mu << _T("\n";);
    //
    stk_cout << "knots =\n";
    stk_cout << a_reg_unif.knots() << _T("\n";);

    Stat::Multivariate<YArray, Real> statResiduals(a_reg_unif.p_residuals());
    statResiduals.run();
    stk_cout << "Mean of the residuals = \n";
    stk_cout << statResiduals.mean() << _T("\n";);
    stk_cout << "Variance of the residuals = \n";
    stk_cout << statResiduals.variance() << _T("\n";);
    stk_cout << "Covariance of the residuals = \n";
    stk_cout << statResiduals.covariance() << _T("\n";);
    stk_cout << _T("\n";);
    stk_cout << "+ end of Estimating 1D manifold with uniform knots  +\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

    stk_cout << "\n\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Estimating 1D manifold with periodic knots         \n";
    y.shift(0,0);
    x.shift(0);
    Regressor a_reg_period(&y, &x, nbControlPoint, degree, Regress::periodicKnotsPositions_);
    a_reg_period.run();
    stk_cout << "knots =\n";
    stk_cout << a_reg_period.knots() << _T("\n";);
    // test extrapolate
    Y = a_reg_period.extrapolate(x);
    R = *a_reg_period.p_predicted() - Y;
    mu = Stat::mean(R);
    //
    statResiduals.setData(*(a_reg_period.p_residuals()));
    statResiduals.run();
    stk_cout << "Mean of the predicted values - extrapolated values = \n";
    stk_cout << mu << _T("\n";);
    // test results
    stk_cout << "Mean of the residuals = \n";
    stk_cout << statResiduals.mean() << _T("\n";);
    stk_cout << "Variance of the residuals = \n";
    stk_cout << statResiduals.variance() << _T("\n";);
    stk_cout << "Covariance of the residuals = \n";
    stk_cout << statResiduals.covariance() << _T("\n";);
    stk_cout << _T("\n\n";);
    stk_cout << "+ end of Estimating 1D manifold with periodic knots +\n";
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
}

// main
int main(int argc, char *argv[])
{
  int nbControlPoint = 7, degree = 3, n=150;
  if (argc > 1) { nbControlPoint = int(atoi(argv[1]));}
  if (argc > 2) { degree = int(atoi(argv[2]));}
  if (argc > 3) { n = int(atoi(argv[3]));}

  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ test BSplineRegression computation                       +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << _T("\n\n";);

  testBspline<ArrayXX, VectorX>( nbControlPoint, degree, n);;
  testBspline<CArrayXX, CVectorX>( nbControlPoint, degree, n);;

  stk_cout << "\n\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ Successful completion of testing for BSplineRegression. +\n";
  stk_cout << "+ No errors detected.                                     +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "\n\n";

  return 0;
}
