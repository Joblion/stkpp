/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/* Project : stkpp::Regress
 * File    : testBSplineCeofficients.cpp
 * Contents: test program for the classes CosinesCoefficients
 * Author  : Serge Iovleff
*/

/** @file testBasisCeofficients.cpp test program for the computations of basis coefficients
 **/

#include <STKpp.h>

using namespace STK;

bool test(CVectorX const& x, int dim, bool useDataValues = true)
{
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Running CosinesCoefficients\n");
  stk_cout << _T("\n");
  CosinesCoefficients<CVectorX> x_cos(x, dim, useDataValues);
  x_cos.setMinValue(-Const::_2PI_);
  x_cos.setMaxValue(Const::_2PI_);
  if (!x_cos.run())
  {
    stk_cout << _T("In test CosinesCoefficients, an error occur.\nWhat:")
             << x_cos.error();
    return false;
  }
  stk_cout << _T("coefficients =\n") << x_cos.coefficients() << _T("\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Running SinesCoefficients\n");
  stk_cout << _T("\n");
  SinesCoefficients<CVectorX> x_sin(x, dim, useDataValues);
  x_sin.setMinValue(-Const::_2PI_);
  x_sin.setMaxValue(Const::_2PI_);
  if (!x_sin.run())
  {
    stk_cout << _T("In test SinesCoefficients, an error occur.\nWhat:")
             << x_sin.error();
    return false;
  }
  stk_cout << _T("coefficients =\n") << x_sin.coefficients() << _T("\n");

  stk_cout << _T("\n\n";);
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Running TrigonometricCoefficients\n");
  stk_cout << _T("\n");
  TrigonometricCoefficients<CVectorX> x_tri(x, dim, useDataValues);
  x_tri.setMinValue(-Const::_2PI_);
  x_tri.setMaxValue(Const::_2PI_);
  if (!x_tri.run())
  {
    stk_cout << _T("In test TrigonometricCoefficients, an error occur.\nWhat:")
             << x_tri.error();
    return false;
  }
  stk_cout << _T("coefficients =\n") << x_tri.coefficients() << _T("\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Running ChebyshevCoefficients\n");
  stk_cout << _T("\n");
  ChebyshevCoefficients<CVectorX> x_che(x, dim, useDataValues);
  x_che.setMinValue(-Const::_2PI_);
  x_che.setMaxValue(Const::_2PI_);
  if (!x_che.run())
  {
    stk_cout << _T("In test ChebyshevCoefficients, an error occur.\nWhat:")
             << x_che.error();
    return false;
  }
  stk_cout << _T("coefficients =\n") << x_che.coefficients() << _T("\n");
  return true;
}

// main
int main(int argc, char *argv[])
{
  int dim = 7, n =14;
  if (argc > 1) { dim = int(atoi(argv[1]));}
  if (argc > 2) { n = int(atoi(argv[2]));}

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ test BasisCoefficients computation                        +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  try
  {
    // create data set
    CVectorX x(Range(1,n));
    Law::Normal l(0., 5.);
    x.rand(l);
    // shift to range [-2pi,2pi]
    double xmin = x.minElt(), xmax = x.maxElt(), tmin = -Const::_2PI_, tmax = Const::_2PI_;
    x = 0.5*( (2* x - (xmax+xmin))*(tmax-tmin)/(xmax-xmin) + (tmax+tmin) );
    heapSort(x);

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ x created:\n");
    stk_cout << _T("x = ") << x.transpose() << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Running Coefficients computation with defaults\n");
    stk_cout << _T("\n");
    if (!test(x, dim))
    {
      stk_cout << _T("In test BasisCoefficients, an error occur.\n");
      return -1;
    }

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Running Coefficients computation with values set\n");
    stk_cout << _T("\n");
    if (!test(x, dim, false))
    {
      stk_cout << _T("In test BasisCoefficients, an error occur.\n");
      return -1;
    }

  }
  catch (Exception const& e)
  {
    stk_cout << _T("In test BasisCoefficients, an error occur.\nWhat:") << e.error();
    return -1;
  }

  stk_cout << "\n\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ Successful completion of testing for BasisCoefficients.   +\n";
  stk_cout << "+ No errors detected.                                       +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  return 0;
}
