/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA
  
    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/* Project : stkpp::Regress
 * File    : testBSplineCeofficients.cpp
 * Contents: test program for the classes BSplineCoefficients
 * Author  : Serge Iovleff
*/

/** @file testBSplineCeofficients.cpp test programm for the computations of
 * the BSpline coefficients
 **/

#include <STKpp.h>

using namespace STK;


// main
int main(int argc, char *argv[])
{
  int nbControlPoints = 7, degree = 3, n =14;
  if (argc > 1) { nbControlPoints = int(atoi(argv[1]));}
  if (argc > 2) { degree = int(atoi(argv[2]));}

  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ test BSplineCoefficients computation                      +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << _T("\n\n";);
  try
  {
    // create data set
    CVectorX x(Range(1,n));
    Law::Normal l(0., 5.);
    x.rand(l);
    heapSort(x);
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ x created:\n";
    stk_cout << "x = " << x.transpose() << _T("\n";);

    stk_cout << _T("\n\n";);
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Running BSplineCoefficients with defaults\n";
    stk_cout << _T("\n";);
    BSplineCoefficients<CVectorX> x_coefs(x, nbControlPoints, degree);
    if (!x_coefs.run())
    {
      stk_cout << _T("In test BSplineCoefficients, an error occur.\nWhat:")
               << x_coefs.error();
      return -1;
    }
    stk_cout << "knots = " << x_coefs.knots().transpose() << _T("\n";);
    stk_cout << "coefficients =\n" << x_coefs.coefficients() << _T("\n";);

    stk_cout << _T("\n\n";);
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Running BSplineCoefficients with periodicKnotsPositions_\n";
    stk_cout << _T("\n";);
    x_coefs.setPosition(Regress::periodicKnotsPositions_);
    if (!x_coefs.run())
    {
      stk_cout << _T("In test BSplineCoefficients, an error occur.\nWhat:")
               << x_coefs.error();
      return -1;
    }
    stk_cout << "knots =" << x_coefs.knots().transpose() << _T("\n";);
    stk_cout << "coefficients =\n" << x_coefs.coefficients() << _T("\n";);

    stk_cout << _T("\n\n";);
    stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    stk_cout << "+ Running BSplineCoefficients with densityKnotsPositions_\n";
    stk_cout << _T("\n";);
    x_coefs.setPosition(Regress::densityKnotsPositions_);
    if (!x_coefs.run())
    {
      stk_cout << _T("In test BSplineCoefficients, an error occur.\nWhat:")
               << x_coefs.error();
      return -1;
    }
    stk_cout << "knots =" << x_coefs.knots().transpose() << _T("\n";);
    stk_cout << "coefficients =\n" << x_coefs.coefficients() << _T("\n";);
  }
  catch (Exception const& e)
  {
    stk_cout << _T("In test BSplineCoefficients, an error occur.\nWhat:") << e.error();
    return -1;
  }
  stk_cout << "\n\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  stk_cout << "+ Successful completion of testing for BSplineCoefficients. +\n";
  stk_cout << "+ No errors detected.                                       +\n";
  stk_cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  return 0;
}
