/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STKernel::Base
 * Purpose:  test program for testing Base classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testNAValues.cpp
 *  @brief In this file we test the conversion to NA value.
 **/

#include "Sdk.h"
#include <STKernel.h>

using namespace STK;

template<class Type>
void testNA()
{
  Type x = Arithmetic<Type>::NA();
  stk_cout << _T("x=") << x << _T("\n");
  stk_cout << _T("x=") << Proxy<Type>(x) << _T("\n\n");
  Type y = STK::stringToType<Type>(stringNa);
  stk_cout << _T("y=") << y << _T("\n");
  stk_cout << _T("y=") << Proxy<Type>(y) << _T("\n\n");
  Type z = Arithmetic<Type>::infinity();
  stk_cout << _T("z=") << z << _T("\n");
  stk_cout << _T("z=") << Proxy<Type>(z) << _T("\n\n");
}
/* main. */
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ test NA values                                      +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");

    // Test Real
    stk_cout << _T("test Real type\n");
    testNA<Real>();
    stk_cout << _T("test int type\n");
    testNA<int>();
    stk_cout << _T("test Binary type\n");
    testNA<Binary>();
    stk_cout << _T("test Range type\n");
    testNA<Range>();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for STK::Base      +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
