/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STKernel
 * created on: 19 oct. 2012
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testFunctors.cpp
 *  @brief In this file we test the Functors.
 **/
#include "Sdk.h"
#include <STKernel.h>

using namespace STK;


/* main.   */
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Functors                                      +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    Real x=10, y=20, z=18;
    int a=8, b=18;

    stk_cout << _T("MinOp<int, Real>()(") << a << _T(",") << x <<_T(") = ")
             << MinOp<int, Real>()(a,x) << _T("\n");
    stk_cout << _T("DivisionWithOp<Real>(") << z << _T(")(") << y <<_T(") = ")
             << DivisionWithOp<Real>(z)(y) << _T("\n");
    stk_cout << _T("DivisionWithOp<int>(") << a << _T(")(") << b <<_T(") = ")
             << DivisionWithOp<int>(a)(b) << _T("\n");
    stk_cout << _T("DivisionWithOp<int>(") << b << _T(")(") << a <<_T(") = ")
                 << DivisionWithOp<int>(b)(a) << _T("\n");
    stk_cout << _T("ProductWithOp<Real>(") << x << _T(")(") << y <<_T(") = ")
                 << ProductWithOp<Real>(x)(y) << _T("\n");
    stk_cout << _T("DivisionOp<Real, Real>(") << x << _T(")(") << y <<_T(") = ")
             << DivisionOp<Real, Real>()(x,y) << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for Functors      +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}





