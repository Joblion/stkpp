/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 8 août 2011
 * Purpose:  test the Normal and MultiLaw::Normal classes.
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 *
 **/

/** @file testJointModels.cpp
 *  @brief In this file we test the joint Statistical models.
 **/

#include "Arrays.h"
#include "DManager.h"
#include "STatistiK.h"
#include "StatModels.h"

using namespace STK;

template<class Array, class Model, class Law >
void testModel(Array& data, Law const& law)
{
  typedef typename hidden::Traits<Array>::Row RowVector;
  // simulate data set
  for (Integer i= data.beginRows(); i < data.endRows(); ++i)
  {
    RowVector row(data.row(i), true); // reference on the ith row
    law.rand(row);
  }
  // create model and initialize parameters
  DataBridge<Array> bridge("id",data);
  Model model(bridge);

  // estimate model
  if (!model.run())
  { stk_cout << _T("An error occurred in testModel.\nWhat: ") << model.error() << _T("\n");}

  stk_cout << "lnLikelihood =" << model.lnLikelihood() << _T("\n");
  stk_cout << "nbFreeParameter =" << model.nbFreeParameter() << _T("\n");
  stk_cout << _T("Estimated parameters=\n");
  model.writeParameters(stk_cout);
}

template<class Array, class Model, class Law >
void testWeightedModel(Array const& data, Law const& law, CVectorX const& weights)
{
  // create model and initialize parameters
  DataBridge<Array> bridge("id",data);
  Model model(bridge);

  // estimate model
  if (!model.run(weights))
  { stk_cout << _T("An error occurred in testModel.\nWhat: ") << model.error() << _T("\n");}

  stk_cout << "lnLikelihood =" << model.lnLikelihood() << _T("\n");
  stk_cout << "nbFreeParameter =" << model.nbFreeParameter() << _T("\n");
  stk_cout << _T("Estimated parameters=\n");
  model.writeParameters(stk_cout);
}

int main(int argc, char *argv[])
{
  int N = (argc < 2) ? 200 : int(atoi(argv[1]));
  // weights are all equals
  CVectorX w(N, 1./Real(N));

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test ModelBernoulli_pj                            +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  // create Bernoulli joint law
  MultiLaw::JointBernoulli<Array2DPoint<Binary> > bLaw(20);
  PointX prob(20);
  for (Integer j= bLaw.JointLaw().begin(); j <= bLaw.JointLaw().lastIdx(); ++j)
  {
    prob[j] = Law::generator.randUnif();
    bLaw.setProb(j, prob[j]);
  }
  // create data set
  Array2D<Binary> bData(N, 20);
  stk_cout << _T("True probabilities=\n") << prob;
  stk_cout << _T("\nTest without weights\n");
  testModel< Array2D<Binary>, ModelBernoulli_pj<Array2D<Binary> > >(bData, bLaw);
  stk_cout << _T("\nTest with weights\n");
  testWeightedModel< Array2D<Binary>, ModelBernoulli_pj<Array2D<Binary> > >(bData, bLaw, w);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ ModelBernoulli_pj.                                +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test ModelDiagGaussian_muj_sj                     +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  // run model
  MultiLaw::JointNormal< PointX > nLaw(20);
  PointX mu(20);
  PointX sigma(20);
  for (Integer j= nLaw.JointLaw().begin(); j <= nLaw.JointLaw().lastIdx(); ++j)
  {
    mu[j] = Law::generator.randGauss();
    sigma[j] = Law::generator.randExp();
    nLaw.setMu(j, mu[j]);
    nLaw.setSigma(j, sigma[j]);
  }
  // create data set
  Array2D<Real> nData(N, 20);
  // test Joint Gaussian model
  stk_cout << _T("True mu and sigma=\n") << mu << sigma << _T("\n");
  stk_cout << _T("\nTest without weights\n");
  testModel< Array2D<Real>, ModelDiagGaussian_muj_sj< Array2D<Real> > >(nData, nLaw);
  stk_cout << _T("\nTest with weights\n");
  testWeightedModel< Array2D<Real>, ModelDiagGaussian_muj_sj<Array2D<Real> > >(nData, nLaw, w);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ DiagGaussian_muj_sj Model.                        +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Model Gamma_aj_bj                             +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  // run model
  // create Bernoulli joint law
  MultiLaw::JointGamma<PointX > gLaw(20);
  PointX alpha(20), beta(20);
  for (Integer j= gLaw.JointLaw().begin(); j < gLaw.JointLaw().end(); ++j)
  {
    alpha[j] = Law::generator.randExp()*5;
    beta[j] = Law::generator.randExp();
    gLaw.setShape(j, alpha[j]);
    gLaw.setScale(j, beta[j]);
  }
  // create data set
  Array2D<Real> gData(N, 20);
  // create model
  stk_cout << _T("True alpha and beta=\n") << alpha << beta;
  stk_cout << _T("\nTest without weights\n");
  testModel< Array2D<Real>, ModelGamma_aj_bj<Array2D<Real>, CVectorX >, MultiLaw::JointGamma<PointX > >(gData, gLaw);
  stk_cout << _T("\nTest with weights\n");
  testWeightedModel< Array2D<Real>, ModelGamma_aj_bj<Array2D<Real>, CVectorX >, MultiLaw::JointGamma<PointX > >(gData, gLaw, w);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for              +\n");
  stk_cout << _T("+ Model Gamma_aj_bj Model.                           +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

