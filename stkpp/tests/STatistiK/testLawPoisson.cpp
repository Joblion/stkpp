/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Analysis
 * Purpose:  test program for testing gamma class and static methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testLawGamma.cpp
 *  @brief In this file we test the Poisson distribution law
 **/

#include "STKpp.h"

using namespace STK;

/**
 * test the Poisson function
 **/
#define N 100000

static void testPoisson()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Poisson law.\n");
  Law::Poisson law;
  Array2DVector<int> tab;
  for (Real lambda = 0.5; lambda<=100; lambda += 10.)
  {
    law.setLambda(lambda);
    tab.rand(law);
    Stat::Univariate< Array2DVector<int>, Real  > stat(tab);
    stk_cout << _T("lambda = ") << lambda << _T("\n");
    stk_cout << _T("mean = ") <<  stat.mean() << _T("\n");
    stk_cout << _T("variance = ") <<  stat.variance() << _T("\n\n");
    stk_cout << _T("1/sqrt(lambda) = ") << 1/std::sqrt(lambda) << _T("\n");
    stk_cout << _T("skewness = ") <<  stat.skewness() << _T("\n\n");
    stk_cout << _T("1/lambda = ") << 1./lambda << _T("\n");
    stk_cout << _T("kurtosis = ") <<  stat.kurtosis() << _T("\n\n");
  }
}


// Main
int main(int argc, char *argv[])
{
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::Law::Poisson.                          +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  testPoisson();

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for           +\n");
  stk_cout << _T("+               STK::Law::Poisson                  +\n");
  stk_cout << _T("+ No errors detected.                            +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

