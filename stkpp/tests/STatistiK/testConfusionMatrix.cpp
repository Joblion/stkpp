/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STatistiKs
 * created on: 05 Aug. 2016
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllKernels.cpp
 *  @brief In this file we test the MultiFactor class.
 **/

#include <STKpp.h>

using namespace STK;

CVectorXi datai(13);
CVectorXi dataj(13);

// Main
int main(int argc, char *argv[])
{
  datai << 1, 2, 1, 2, 3, 1, 3, 2, 1, 3, 1, 3, 2;
  dataj << 1, 1, 3, 4, 3, 2, 3, 2, 3, 3, 4, 3, 2;

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test confusionMatrix.                          +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  CSquareXi res;
  res.move(Stat::confusionMatrix(datai, dataj));

  stk_cout << _T("datai=") << datai.transpose();
  stk_cout << _T("dataj=") << dataj.transpose();
  stk_cout << _T("res=\n") << res;
  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for           +\n");
  stk_cout << _T("  confusionMatrix.                               +\n");
  stk_cout << _T("+ No errors detected.                            +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}




