/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA
    
    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Analysis
 * Purpose:  test program for testing gamma class and static methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testLawUniformDiscrete.cpp
 *  @brief In this file we test the Unoiform Discrete distribution law
 **/

#include "STKpp.h"

using namespace STK;

#define N 100

/**
 * test the Uniform function
 **/
static void testUniformDiscrete(int a, int b)
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test UniformDiscrete law with a=")<< a <<_T(" and b=")<<b << _T("\n");
  Law::UniformDiscrete law(a,b);
  CVectorXi tab(N);
  tab.rand(law);
  std::cout << "min = " << tab.minElt() << _T("\n");
  std::cout << "max = " << tab.maxElt() << _T("\n");
  CVectorX freq(Range(a, (b-a+1)), 0.);
  for(int i=tab.begin(); i<tab.end(); i++) { freq[tab[i]] += 1.;}
  freq /= N;
  stk_cout << _T("frequences =") << freq.transpose();

  stk_cout << _T("\n+ test static rand\n");
  for(int i=tab.begin(); i<tab.end(); i++) { tab[i] = Law::UniformDiscrete::rand(a, b) ;}
  tab.rand(law);
  std::cout << "min = " << tab.minElt() << _T("\n");
  std::cout << "max = " << tab.maxElt() << _T("\n");
  freq = 0.;
  for(int i=tab.begin(); i<tab.end(); i++) { freq[tab[i]] += 1.;}
  freq /= N;
  stk_cout << _T("frequences =") << freq.transpose();
  stk_cout << _T("+ UniformDiscrete law with a=")<< a <<_T(" and b=")<<b << _T(" done\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n");
}

// Main
int main(int argc, char *argv[])
{
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::Law::UniformDiscrete.                +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  testUniformDiscrete(0,0);
  testUniformDiscrete(1,1);
  testUniformDiscrete(0,9);
  testUniformDiscrete(1,10);

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for           +\n");
  stk_cout << _T("+ STK::Law::UniformDiscrete. No errors detected. +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

