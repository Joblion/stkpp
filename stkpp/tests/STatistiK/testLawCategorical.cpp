/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Analysis
 * Purpose:  test program for testing gamma class and static methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testLawCategorical.cpp
 *  @brief In this file we test the Categorical distribution law
 **/

#include "STKpp.h"

using namespace STK;

#define N 10

/**
 * test the categorical function
 **/
static void testCategorical()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test Categorical law.\n");
  Law::Categorical law;
  VectorX prob(3); prob  << 0.1, 0.2, 0.2;
  CArrayVector<int, N> tab;
  law.setProb(prob);
  tab.rand(law);
  Stat::Univariate< VectorX, Real  > stat(tab);
  stk_cout << _T("tab = ") << tab.transpose() << _T("\n");
  stk_cout << _T("prob = ") << prob.transpose() << _T("\n");
  stk_cout << _T("mean = ") <<  stat.mean() << _T("\n");
  stk_cout << _T("variance = ") <<  stat.variance() << _T("\n\n");
}


// Main
int main(int argc, char *argv[])
{
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::Law::Categorical.                    +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  testCategorical();

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for           +\n");
  stk_cout << _T("+ STK::Law::Categorical. No errors detected.     +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}

