/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STatistiK::StatDesc
 * Purpose:  test program for testing Analysis classes and methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testStatFunctors.cpp
 *  @brief In this file we test the statistical functors classes.
 **/

#include "STKpp.h"

using namespace STK;
using namespace STK::Stat;


/* main.*/
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+   Test Stat Transform classes                     +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");

    CArray<Real, 100,5> A(100, 5);
    CArrayVector<Real, 100> w(100);
    Law::Normal law(1,2);
    Law::Exponential exp(1);
    A.rand(law);
    w.rand(exp);

    CPointX mu, std, aux;

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Usual versions by Columns                         +\n");
    stk_cout << _T("mean(A) =") << (aux=Stat::meanByCol(A));
    stk_cout << _T("variance(A) =") << STK::Stat::varianceWithFixedMean(A, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("centerByCol(A, mu);\n");
    centerByCol(A, mu);
    stk_cout << _T("mean(A) =") << (aux=STK::Stat::meanByCol(A));
    stk_cout << _T("variance(A) =") << STK::Stat::varianceWithFixedMean(A, aux, false) << _T("\n");
    stk_cout << _T("uncenterByCol(A, mu);\n");
    uncenterByCol(A, mu);
    stk_cout << _T("mean(A) =") << (aux=STK::Stat::meanByCol(A));
    stk_cout << _T("variance(A) =") << STK::Stat::varianceWithFixedMean(A, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("standardizeByCol(A, mu, std);\n");
    standardizeByCol(A, mu, std);
    stk_cout << _T("mean(A) =") << (aux=STK::Stat::meanByCol(A));
    stk_cout << _T("variance(A) =") << STK::Stat::varianceWithFixedMean(A, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("unstandardizeByCol(A, mu, std);\n");
    unstandardizeByCol(A, mu, std);
    stk_cout << _T("mean(A) =") << (aux=STK::Stat::meanByCol(A));
    stk_cout << _T("variance(A) =") << STK::Stat::varianceWithFixedMean(A, aux, false) << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Weighted versions by columns                      +\n");
    stk_cout << _T("mean(A,w) =") << (aux=Stat::meanByCol(A,w));
    stk_cout << _T("variance(A,w) =") << STK::Stat::varianceWithFixedMean(A, w, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("centerByCol(A, w, mu);\n");
    centerByCol(A, w, mu);
    stk_cout << _T("mean(A,w) =") << (aux=Stat::meanByCol(A,w));
    stk_cout << _T("variance(A,w) =") << STK::Stat::varianceWithFixedMean(A, w, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("uncenterByCol(A, mu);\n");
    uncenterByCol(A, mu);
    stk_cout << _T("mean(A,w) =") << (aux=STK::Stat::meanByCol(A,w));
    stk_cout << _T("variance(A,w) =") << STK::Stat::varianceWithFixedMean(A, w, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("standardizebyCol(A, w, mu, std);\n");
    standardizeByCol(A, w, mu, std);
    stk_cout << _T("mean(A,w) =") << (aux=STK::Stat::meanByCol(A,w));
    stk_cout << _T("variance(A,w) =") << STK::Stat::varianceWithFixedMean(A, w, aux, false) << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("unstandardizeByCol(A, mu, std);\n");
    unstandardizeByCol(A, mu, std);
    stk_cout << _T("mean(A,w) =") << (aux=STK::Stat::meanByCol(A,w));
    stk_cout << _T("variance(A,w) =") << STK::Stat::varianceWithFixedMean(A, w, aux, false) << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Usual versions by rows                            +\n");
    CArray<Real, 5, 100> At = A.transpose();
    CVectorX mut, stdt, auxt;
    stk_cout << _T("meanByRow") << (auxt=Stat::meanByRow(At)).transpose();
    stk_cout << _T("varianceByRow(A') =") << STK::Stat::varianceWithFixedMeanByRow(At, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("centerByRow(A', mu);\n");
    centerByRow(At, mut);
    stk_cout << _T("meanByRow(A') =") << (auxt=STK::Stat::meanByRow(At)).transpose();
    stk_cout << _T("varianceByRow(A') =") << STK::Stat::varianceWithFixedMeanByRow(At, auxt, false).transpose() << _T("\n");
    stk_cout << _T("uncenterByRow(A', mut);\n");
    uncenterByRow(At, mut);
    stk_cout << _T("meanByRow(A') =") << (auxt=STK::Stat::meanByRow(At)).transpose();
    stk_cout << _T("varianceByRow(A') =") << STK::Stat::varianceWithFixedMeanByRow(At, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("standardizeByRow(A', mu, std);\n");
    standardizeByRow(At, mut, stdt);
    stk_cout << _T("meanByRow(A') =") << (auxt=STK::Stat::meanByRow(At)).transpose();
    stk_cout << _T("varianceByRow(A') =") << STK::Stat::varianceWithFixedMeanByRow(At, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("unstandardizeByRow(A', mu, std);\n");
    unstandardizeByRow(At, mut, stdt);
    stk_cout << _T("meanByRow(A') =") << (auxt=STK::Stat::meanByRow(At)).transpose();
    stk_cout << _T("varianceByRow(A') =") << STK::Stat::varianceWithFixedMeanByRow(At, auxt, false).transpose() << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Weighted versions by rows                         +\n");
    stk_cout << _T("meanByRow(A',w)") << (auxt=Stat::meanByRow(At, w)).transpose();
    stk_cout << _T("varianceByRow(A',w) =") << STK::Stat::varianceWithFixedMeanByRow(At, w, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("centerByRow(A',w,mu);\n");
    centerByRow(At, w, mut);
    stk_cout << _T("meanByRow(A',w) =") << (auxt=STK::Stat::meanByRow(At,w)).transpose();
    stk_cout << _T("varianceByRow(A',w) =") << STK::Stat::varianceWithFixedMeanByRow(At, w, auxt, false).transpose() << _T("\n");
    stk_cout << _T("uncenterByRow(A',mut);\n");
    uncenterByRow(At, mut);
    stk_cout << _T("meanByRow(A',w) =") << (auxt=STK::Stat::meanByRow(At,w)).transpose();
    stk_cout << _T("varianceByRow(A',w) =") << STK::Stat::varianceWithFixedMeanByRow(At, w, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("standardizeByRow(A', mu, std);\n");
    standardizeByRow(At, w, mut, stdt);
    stk_cout << _T("meanByRow(A',w) =") << (auxt=STK::Stat::meanByRow(At,w)).transpose();
    stk_cout << _T("varianceByRow(A',w) =") << STK::Stat::varianceWithFixedMeanByRow(At, w, auxt, false).transpose() << _T("\n");
    stk_cout << _T("\n");
    stk_cout << _T("unstandardizeByRow(A', mu, std);\n");
    unstandardizeByRow(At, mut, stdt);
    stk_cout << _T("meanByRow(A',w) =") << (auxt=STK::Stat::meanByRow(At,w)).transpose();
    stk_cout << _T("varianceByRow(A',w) =") << STK::Stat::varianceWithFixedMeanByRow(At, w, auxt, false).transpose() << _T("\n");
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for Stat Transform+\n");
  stk_cout << _T("+ No errors detected.                               +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  return 0;
}
