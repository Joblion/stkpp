/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STatistiKs
 * created on: 31 janv. 2016
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllKernels.cpp
 *  @brief In this file we test the kernels.
 **/

#include <STKpp.h>

using namespace STK;

CArrayXX data(13, 3);
CArrayXXi datai(13, 3);

// Main
int main(int argc, char *argv[])
{
  data <<  1.03295958568726,-0.0578352385322887,0,
           0.266727240966317,-0.949475401845759,0,
          -0.686629092272256,0.601337658193694,0,
          -0.668615624382937,-0.731003952947612,0,
          -0.507595204512622,-0.540992830706731,0,
           0.350346745878971,0.748372867385125,0,
          -0.931363592366953,0.328393111483986,0,
          -0.148121679147009,0.758025566600668,0,
           4.87829186071152,1.53401924983587,1,
           4.77635932792231,0.79153324479032,1,
           1.60221700047392,4.79443549262974,1,
           0.631445223569527,5.03385474175952,1,
           4.32377027689325,-3.0930920569516,1;
  datai << 1, 0, 0,
           2, 1, 0,
           1, 0, 0,
           2, 0, 0,
           3, 0, 0,
           1, 1, 0,
           3, 2, 1,
           2, 2, 1,
           1, 2, 1,
           3, 1, 1,
           1, 0, 1,
           3, 0, 1,
           2, 2, 1;

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test All STK::Kernels.                         +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  Kernel::IKernel* p_kernel;
  p_kernel = new Kernel::Laplace<CArrayXX>(data,1.);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("Exponential Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  delete p_kernel;

  p_kernel = new Kernel::Gaussian<CArrayXX>(data,1.);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("Gaussian Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  delete p_kernel;

  p_kernel = new Kernel::Linear<CArrayXX>(data);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("Linear Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  delete p_kernel;

  p_kernel = new Kernel::Polynomial<CArrayXX>(data, 2., 1.);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("Polynomial Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  delete p_kernel;

  p_kernel = new Kernel::RationalQuadratic<CArrayXX>(data, 2.);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("RationalQuadratic Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  delete p_kernel;

  // datai is integer
  p_kernel = new Kernel::Hamming<CArrayXXi>(datai, 0.6);
  if (! p_kernel->run()) return -1;
  stk_cout << _T("Hamming Kernel. gram matrix =\n")
           << p_kernel->gram() <<_T("\n");
  stk_cout << _T("Hamming Kernel. factors level matrix =\n")
           << static_cast<Kernel::Hamming<CArrayXXi>* >(p_kernel)->factors().levels() <<_T("\n");
  delete p_kernel;

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for all       +\n");
  stk_cout << _T("+ STK::Kernels. No errors detected.              +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}




