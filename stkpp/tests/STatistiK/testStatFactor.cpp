/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STatistiKs
 * created on: 05 Aug. 2016
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testAllKernels.cpp
 *  @brief In this file we test the MultiFactor class.
 **/

#include <STKpp.h>

using namespace STK;

typedef CArray<char, 13, 3> CArray13_3i;
CArray13_3i datai(13, 3);

// Main
int main(int argc, char *argv[])
{
  CArray<char, 13, 3> datai;
  datai << 'b', 'a', 'a',
           'c', 'b', 'a',
           'b', 'a', 'a',
           'c', 'a', 'a',
           'd', 'a', 'a',
           'b', 'b', 'a',
           'd', 'c', 'b',
           'c', 'c', 'b',
           'b', 'c', 'b',
           'd', 'b', 'b',
           'b', 'a', 'b',
           'd', 'a', 'b',
           'c', 'c', 'b';
  try
  {

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Factor class.                               +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    Stat::Factor<CArrayVector<char, 13> > f1d(datai.col(1));
    f1d.run();
    stk_cout << _T("nbLevels= ")  << f1d.nbLevels() << _T("\n");
    stk_cout << _T("asInteger= ") << f1d.asInteger().transpose() << _T("\n");
    stk_cout <<_T("Levels: ")     << f1d.levels().transpose();
    stk_cout <<_T("Levels counts: ") << f1d.counts().transpose();
    Stat::Factor<CArrayVector<char, 13> >::EncodingMap::const_iterator it;
    for (it = f1d.encoder().begin(); it != f1d.encoder().end(); ++it)
    stk_cout <<_T("encoding ")<< it->first << _T(" --> ")<< it->second << _T("\n");
    stk_cout << _T("\n");
    Stat::Factor<CArrayVector<char, 13> >::DecodingMap::const_iterator itd;
    for (itd = f1d.decoder().begin(); itd != f1d.decoder().end(); ++itd)
    stk_cout <<_T("decoding ")<< itd->first << _T(" --> ")<< itd->second << _T("\n");
    stk_cout << _T("\n");

    stk_cout << _T("\n\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test MultiFactor class.                          +\n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    Stat::MultiFactor<CArray13_3i> f2d(datai);
    f2d.run();
    stk_cout << _T("nbLevels= ")   << f2d.nbLevels() << _T("\n");
    stk_cout << _T("asInteger=\n") << f2d.asInteger() << _T("\n");
    for (int i=f2d.levels().begin(); i<f2d.levels().end(); ++i)
    { stk_cout <<_T("Levels variable ")<< i << _T(": ")<< f2d.levels()[i].transpose();}
    stk_cout << _T("\n");
    for (int i=f2d.levels().begin(); i<f2d.levels().end(); ++i)
    { stk_cout <<_T("Levels counts ")<< i << _T(": ")<< f2d.counts()[i].transpose();}
    stk_cout << _T("\n");
    for (int i=f2d.encoder().begin(); i<f2d.encoder().end(); ++i)
    {
      Stat::MultiFactor<CArray13_3i>::EncodingMap::const_iterator it;
      for (it = f2d.encoder()[i].begin(); it != f2d.encoder()[i].end(); ++it)
      stk_cout <<_T("encoding ")<< it->first << _T(" --> ")<< it->second << _T("\n");
      stk_cout << _T("\n");
    }
    stk_cout << _T("\n");
    for (int i=f2d.encoder().begin(); i<f2d.encoder().end(); ++i)
    {
      Stat::MultiFactor<CArray13_3i>::DecodingMap::const_iterator it;
      for (it = f2d.decoder()[i].begin(); it != f2d.decoder()[i].end(); ++it)
      stk_cout <<_T("decoding ")<< it->first << _T(" --> ")<< it->second << _T("\n");
      stk_cout << _T("\n");
    }
  }
  catch (Exception const& e)
  {
    stk_cout << _T("An error occur:") << e.error() << _T("\n");
    return -1;
  }

  stk_cout << _T("\n\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for Factor.     +\n");
  stk_cout << _T("+ No errors detected.                              +\n");
  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}




