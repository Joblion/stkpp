/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::STatistiK::StatDesc
 * Purpose:  test program for testing Analysis classes and methods.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testStatCovairance.cpp
 *  @brief In this file we test the covariance methods.
 **/

#include "STKpp.h"

using namespace STK;
using namespace STK::Stat;

/// utility method generatin a correlated data set
CArraySquare<Real, 5> create(CArray<Real, 100, 5>& A)
{
  // create arbitrary data
  CArray<Real, 20, 5> x;
  x.randGauss();
  // covariance matrix
  CArraySquare<Real, 5> res = x.transpose() * x/20;
  Array2DLowerTriangular<Real> L;
  Array2DDiagonal<Real> D;
  cholesky(res, D, L);
  // create correlated data set
  A = A.randGauss() * D.sqrt() * L.transpose();
  A += 1;
  // return covariance matrix
  return res;
}

/* main.*/
int main(int argc, char *argv[])
{
  try
  {
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test covariance methods                           +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  CArray<Real, 100, 5> A;
  CArrayVector<Real, 100> w;
  Law::Exponential exp(1);
  w.rand(exp);
  CArraySquare<Real, 5> sigma = create(A);

  stk_cout << _T("sigma =\n")<< sigma;
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covariance(A) =\n")<< covariance(A);
  stk_cout << _T("covariance(A,true) =\n")<< covariance(A,true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covarianceByRow(A') =\n")<< covarianceByRow(A.transpose());
  stk_cout << _T("covarianceByRow(A',true) =\n")<< covarianceByRow(A.transpose(),true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covariance(A,w) =\n")<< covariance(A,w);
  stk_cout << _T("covariance(A,w,true) =\n")<< covariance(A,w,true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covarianceByRow(A',w) =\n")<< covarianceByRow(A.transpose(),w);
  stk_cout << _T("covarianceByRow(A',w,true) =\n")<< covarianceByRow(A.transpose(),w,true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covarianceWithFixedMean(A,1) =\n")<< covarianceWithFixedMean(A,Const::Vector<Real,5>());
  stk_cout << _T("covarianceWithFixedMean(A,1,true) =\n")<< covarianceWithFixedMean(A,Const::Vector<Real,5>(),true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("covarianceWithFixedMeanByRow(A',1) =\n")<< covarianceWithFixedMeanByRow(A.transpose(),Const::Vector<Real,5>());
  stk_cout << _T("covarianceWithFixedMeanByRow(A',1,true) =\n")<< covarianceWithFixedMeanByRow(A.transpose(),Const::Vector<Real,5>(),true);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for covariance   +\n");
  stk_cout << _T("+ No errors detected.                               +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  }
  catch (Exception const& e)
  {
    stk_cout << _T("In TestStatCovariance, an error occur:") << e.error();
    return -1;
  }
  return 0;
}
