/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testSArray2D.cpp
 *  @brief In this file we test the TContainer 2D classes.
 **/

#include <Arrays.h>

using namespace STK;

/** @ingroup Arrays
  * @brief allow to number to the element of an Arrays
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline  Type const& operator() ()
  {
    if ( STK::Law::generator.randUnif() <0.5)
    { value_ = 0;}
    else  { ++value_;}
    return value_;
  }
};

/** Utility function for numbering an array */
template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}



/* main print */
template< class Type, template<class> class SArray2D >
void printHead(SArray2D<Type> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")                 << A.isRef()  << _T("\n");
  stk_cout << name << _T(".allocator().isRef() =")     << A.allocator().isRef() << _T("\n");
  stk_cout << name << _T(".rangeCols().isRef() =")     << A.rangeCols().isRef() << _T("\n");
  stk_cout << name << _T(".rows() =")                  << A.rows()  << _T("\n");
  stk_cout << name << _T(".cols() =")                  << A.cols()  << _T("\n");
  stk_cout << name << _T(".allocator().range() =")     << A.allocator().range()  << _T("\n");
  stk_cout << name << _T(".rangeCols().range() =")     << A.rangeCols().range() << _T("\n");
  stk_cout << name << _T(".availableCols() =")         << A.availableCols()  << _T("\n");
  stk_cout << name << _T(".rangeCols() = ")            << A.rangeCols();
  stk_cout << name << _T(".allocator() = ")
                   << Array1D< SArray1D<Type>* >(A.allocator(), A.cols(), true)  << _T("\n");
}

template< class Type, template<class> class SArray2D >
void print(SArray2D<Type> const& A, String const& name)
{
  printHead(A, name);
  stk_cout << name << _T("=\n") << A << _T("\n\n");
}

template< class Type>
void TestSArray2D( int M, int N, Range I, Range J, bool output)
{
  Range Iaux1, Iaux2;
  //
  if (output)
  {
    stk_cout << _T("=============Test constructors================\n");
  }
  if (output) stk_cout << _T("Test constructor A(M,N); numbering(A);\n");
  SArray2D<Type> A(M,N);
  numbering(A);
  if (output) { print(A, "A");}

  if (output) stk_cout << _T("Test constructor B(N, M, 3) \n");
  SArray2D<Type> B(M, N, 3);
  if (output) { print(B,"B");}

  //
  if (output) stk_cout << _T("Test copy constructor Testing C(A, true) \n");
  SArray2D<Type> C(A, true);
  if (output) { print(C, "C");}

  if (output)
  {
    stk_cout << _T("=============Test copy and assign================\n");
  }
  //
  if (output) stk_cout << _T("Testing  A.pushBackCols(1);A.col(A.lastIdxCols()) = Type(0); \n");
  A.pushBackCols(1);
  A.col(A.lastIdxCols()) = Type(0);
  if (output) { print(A, "A");}
  if (output) stk_cout << _T("Testing A.col(A.lastIdxCols()=(B.row(1))\n");
  A.col(A.lastIdxCols())=(B.col(1));
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Test A(I,J).copy(B(I,J))\n");
  A(I,J)=(B(I,J));
  if (output)
  {
    stk_cout << _T("I= ")  << I  << _T(" ") << _T("J= ")  << J   << _T("\n");
    print(A, "A");
  }

  //
  Range I1=I, J1=J;
  if (output)
  { stk_cout << _T("Testing Inx + operators: A(I+1,J-1) = 1;\n");
    stk_cout << _T("I= ")  << I  << _T(" ") << _T("J= ")  << J   << _T("\n");
    stk_cout << _T("I1.inc()= ")<< I1.inc() << _T(" ") << _T("J1.dec()= ")<< J1.dec() << _T("\n");
  }
  SArray2D<Type> Aref(A, I1, J1);
  Aref = Type(1);
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing  G = A(I,2):\n");
  typename SArray2D<Type>::Col G = A(I,2);
  if (output) { print(A(I,2), "A(I,2)");}
  if (output) { print(G, "G");}

  //
//  I1 = I; I1.inc(2);
//  if (output) stk_cout << _T("Testing  A(I+2,1)=(G)\n");
//  if (output) { print(A(I1,1), "A(I+2,1)");}
//  A(I1,1)=(G);
//  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing  H=A(3,J)\n");
  typename SArray2D<Type>::Row H = A(3,J);
  if (output) stk_cout << _T("H = \n") << H << _T("\n\n");

  //
//  if (output) stk_cout << _T("Testing  A(4,J+1)=(H)\n");
//  A(4,J.inc(1))=(H);
//  if (output) { print(A, "A");}

  if (output)
  {
    stk_cout << _T("=============Test shift================\n");
  }
  //
  if (output) stk_cout << _T("Testing  A.shift(-1,3)\n");
  A.shift(-1,3);
  if (output) { print(A, "A");}

  if (output)
  {
    stk_cout << _T("=============columns modifier================\n");
  }
  //
  if (output)
  { stk_cout << _T("Testing  A.insertCols(A.beginCols()+1,4) and A.col(A.beginCols()+Range(1,4))=4\n");}
  A.insertCols(A.beginCols()+1,4);
  A.col(Range(1,4).inc(A.beginCols())) = Type(4);
  if (output) { print(A, "A");}

   //
  if (output) stk_cout << _T("Testing  A.eraseCols(A.beginCols()+2)\n");
  if (output) { stk_cout << _T("A.beginCols()+2=") << A.beginCols()+2 << _T("\n");}
  A.eraseCols(A.beginCols()+2);
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing  A.popBackCols()\n");
  A.popBackCols();
  if (output) { print(A, "A");}

  if (output)
  {
    stk_cout << _T("=============rows modifier================\n");
  }
  //
  if (output) stk_cout << _T("Testing A.eraseRows(A.beginRows(), A.beginRows()+2) \n");
  if (output)
  { stk_cout << _T("pos = A.beginRows()=") << A.beginRows();
    stk_cout << _T(", n = A.beginRows()+2=") << A.beginRows()+2 << _T("\n");
  }
  A.eraseRows(A.beginRows(), A.beginRows()+2);
  if (output) { print(A, "A");}

  //
  Iaux1 = Range(A.beginRows()+1, 2);
  Iaux2 = Range(A.beginRows()+4, 2);
  if (output)
  { stk_cout << _T("Testing A.insertRows(") << Iaux1 << _T(")\n");
    stk_cout << _T("pos=") << Iaux1.begin() << _T(", n =") << Iaux1.size() << _T("\n");
  }
  A.insertRows(Iaux1.begin(), Iaux1.size());
//  if (output)
//  { stk_cout << _T("Testing A(") << Iaux1 << _T(",A.cols()).copy(A(") << Iaux2 << _T(",A.cols())\n");}
//  A(Iaux1, A.cols()).copy(A(Iaux2, A.cols()));
//  if (output) { print(A, "A");}

  if (output)
  {
    stk_cout << _T("=============resize, erase and insert combination================\n");
  }
  //
  if (output) stk_cout << _T("Testing A.resize(Inx(1, M), Inx(1, M))\n");
  A.resize(Range(1,M, 0), Range(1,M, 0));
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing A.eraseRows(A.beginRows(), A.sizeRows())\n");
  A.eraseRows(A.beginRows(),A.sizeRows());
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing A.insertCols(A.beginCols()+1,2) and A.pushBackRows(2), A=2\n");
  A.insertCols(A.beginCols()+1,2);
  A.pushBackRows(2);
  A = Type(2);
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing A.clear() and A = B\n");
  A.clear();
  A = B;
  if (output) { print(A, "A");}

  if (output) stk_cout << _T("Testing A.insertRows(A.beginRows()+2, 5) and A(A.beginRows()+2:7,A.cols()) = 3.\n");
  A.insertRows(A.beginRows()+2,5);
  A(Range(A.beginRows()+2,5), A.cols()) = Type(3);
  if (output) { print(A, "A");}

  // release array
  if (output)  stk_cout << _T("Testing A.eraseCols(A.beginCols(), A.sizeCols())\n");
  A.eraseCols(A.beginCols(),A.sizeCols());
  if (output) { print(A, "A");}

  //
  if (output) stk_cout << _T("Testing A.insertRows(1,2) and A.pushBackCols(2), A=2\n");
  A.insertRows(1,2);
  A.pushBackCols(2);
  A = Type(2);
  if (output) { print(A, "A");}

  //
  if (output)
    stk_cout << _T("Testing B.resize(1:M,1:N); B=2\n");
  B.resize(Range(1, M, 0), Range(1, N, 0)); B = Type(2);
  if (output) { print(B, "B");}

  //
  if (output) stk_cout << _T("Testing A.exchange(B) \n");
  A.exchange(B);
  if (output)
  { print(A, "A");  print(B, "B");}

  //
  if (output) stk_cout << _T("Testing A.resize(2*A.sizeRows(), A.sizeCols()) ")
                       << _T(" and A = 1 \n");
  A.resize(2*A.sizeRows(), A.sizeCols());
  A = Type(1);
  if (output)
  { print(A, "A"); print(B, "B");}

  //
  if (output) stk_cout << _T("Testing D(A); D = 2; \n");
  SArray2D<Type> D(A);
  D=Type(2);
  if (output)
  { print(A, "A"); print(D, "D");}
}


/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{

  //  try
//  {
    int M, N, Istart, Jstart, Iend, Jend, output, iter;
    if (argc < 7)
    {
      // 12 15 3 4 10 12 1 2
      M      = 11;
      N      = 12;
      Istart = 2;
      Jstart = 3;
      Iend   = 8;
      Jend   = 9;
      output = true;
      iter = 1;

    }
    else
    {
      // dimensions
      M      = atoi(argv[1]);
      N      = atoi(argv[2]);
      Istart = atoi(argv[3]);
      Jstart = atoi(argv[4]);
      Iend   = atoi(argv[5]);
      Jend   = atoi(argv[6]);
      // output
      output = true;
      if (argc >7 ) output = atoi(argv[7]);
      // number of test
      iter =1;
      if (argc >8 ) iter = atoi(argv[8]);
    }
    // ranges
    Range I(Istart, Iend, 0);
    Range J(Jstart, Jend, 0);

    for( int i=1; i<=iter; i++)
    {
      stk_cout << _T("\n\n");
      stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("+ Test STK::Arrays                                    +\n");
      stk_cout << _T("+ Using: \n");
      stk_cout << _T("M = ") << M << _T(", N = ") << N
               << _T(", I = ") << I << _T(", J = ") << J << _T("\n");
      stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("TestSArray2D<Real> : \n");
      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      // time variables
      Chrono::start();
      TestSArray2D<Real>(M, N, I, J, output);
      Real test_time = Chrono::elapsed();
      stk_cout << _T("\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");
      stk_cout << _T("\n\n");

      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("TestSArray2D<int> : \n");
      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      // time variables
      Chrono::start();
      TestSArray2D<int>(M, N, I, J, output);
      test_time = Chrono::elapsed();
      stk_cout << _T("\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");
      stk_cout << _T("\n\n");

      M *=2; N *=2; Istart *= 2; Jstart *= 2;  Iend   *= 2;  Jend   *= 2;
    }
//  }
//  catch (Exception const& error)
//  {
//    std::cerr << error.error() << _T("\n";);
//    return -1;
//  }

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for STK::SArray2D  +\n");
  stk_cout << _T("+ No errors detected.                                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  return 0;
}
