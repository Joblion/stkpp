/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-204  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/*
 * Project:  stkpp::Arrays
 * created on: 13 sept. 204
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testCAllocator.cpp
 *  @brief In this file the CAllocator class.
 **/


#include <STKernel.h>
/**
 *  @brief Write a pair in the form (first;last) in output stream.
 *  @param os output stream
 *  @param I the Range to write
 **/

#include "Arrays/include/allocators/STK_MemSAllocator1D.h"
#include "Arrays/include/STK_Array2D.h"
#include "Arrays/include/STK_Display.h"


using namespace STK;

template<typename Type>
ostream& operator<< (ostream& os, std::pair<int, Type> const& I)
{
  os << _T("(") <<  I.first << _T(";") << _T(")") << I.second;
  return os;
}

/* main print. */
template< typename Type_=Real, int NzMax_ = UnknownSize>
void print(MemSAllocator1D<Type_, NzMax_> const& A, String const& name)
{
  Range rows = A.range();
  Array1D<Type_> res(A.range(), Type_(0));

  for (int i = res.begin(); i<res.end(); ++i)
  { res[ i ] = A.elt(i);}

  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".idx() =")  << A.idx();
  stk_cout << name << _T(".val() =")  << A.val();
  stk_cout << _T("res =")  << res << _T("\n");
  stk_cout << "print: " << name << _T(" done\n\n";);
}

template< typename Type_=Real, int Size_=UnknownSize, int NzMax_ = UnknownSize>
void test()
{

  stk_cout << "------------------------------\n";
  MemSAllocator1D<Type_, NzMax_> m0;
  print(m0, "m0, Empty allocator");

  //
  MemSAllocator1D<Type_, NzMax_> m1(_R(1,10));
  print(m1, "m1(_R(1,10)), empty allocator");
  // add values
  stk_cout << "m1, add (2,1/10) entry\n";
  m1.setValue( 2, 1./10);
  print(m1, "1 entry");

  stk_cout << "m1, add (2,0) entry\n";
  m1.setValue( 2, 0.);
  print(m1, "0 entry");

  stk_cout << "m1, add (2,1/10) entry\n";
  m1.setValue( 2, 1./10);
  print(m1, "1 entry");

  stk_cout << "m1, add (4,2/10) entry\n";
  m1.setValue( 4, 2./10);
  print(m1, "2 entries");
  stk_cout << "m1, add (4,3/10) entry\n";
  m1.setValue( 4, 3./10);
  print(m1, "3 entries");
  stk_cout << "m1, add (1,4/10) entry\n";
  m1.setValue( 1, 4./10);
  print(m1, "4 entries");
  stk_cout << "m1, add (8,5/10) entry\n";
  m1.setValue( 8, 5./10);
  print(m1, "5 entries");

  stk_cout << "m1, add (6,6/10) entry\n";
  m1.setValue( 6, 6./10);
  print(m1, "6 entries");
  // remove entry
  stk_cout << "m1, add (6,0) entry\n";
  m1.setValue( 6, 0.);
  print(m1, "5 entries");

  stk_cout << "m1, add (6,6/10) entry\n";
  m1.setValue( 6, 6./10);
  print(m1, "6 entries");

  stk_cout << "m1, add (10,7/10) entry\n";
  m1.setValue( 10, 7./10);
  print(m1, "7 entries");

  // second test
  stk_cout << "------------------------------\n";
  MemSAllocator1D<Type_, NzMax_> m2(_R(1,10), 7); // nzmax = 7
  print(m2, "m2(_R(1,10), 7), empty allocator");
  // add values
  stk_cout << "m2, add (2,1/10) entry\n";
  m2.setValue( 2, 1./10);
  print(m2, "1 entry");

  stk_cout << "m2, add ( 2,0) entry\n";
  m2.setValue( 2, 0.);
  print(m2, "0 entry");

  stk_cout << "m2, add (2,1/10) entry\n";
  m2.setValue( 2, 1./10);
  print(m2, "1 entry");

  stk_cout << "m2, add (2,1/10) entry\n";
  m2.setValue(2, 1./10);
  print(m2, "1 entry");
  stk_cout << "m2, add (4,2/10) entry\n";
  m2.setValue( 4, 2./10);
  print(m2, "2 entries");
  stk_cout << "m2, add (4,3/10) entry\n";
  m2.setValue(4, 3./10);
  print(m2, "3 entries");
  stk_cout << "m2, add (1,4/10) entry\n";
  m2.setValue( 1, 4./10);
  print(m2, "4 entries");
  stk_cout << "m2, add (8,5/10) entry\n";
  m2.setValue( 8, 5./10);
  print(m2, "5 entries");

  stk_cout << "m2, add (6,6/10) entry\n";
  m2.setValue(6, 6./10);
  print(m2, "6 entries");

  stk_cout << "m2, add (6,0) entry\n";
  m2.setValue(6, 0);
  print(m2, "5 entries");

  stk_cout << "m2, add (6,6/10) entry\n";
  m2.setValue(6, 6./10);
  print(m2, "6 entries");

  stk_cout << "m2, add (10,7/10) entry\n";
  m2.setValue(10, 7./10);
  print(m2, "7 entries");
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ TestMemSAllocator.                                    +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with unknown sizes.                              +\n");
    test<Real, UnknownSize, UnknownSize>();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with NzMax_ fixed to 7.                          +\n");
    test<Real, UnknownSize, 7>();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with Size_ fixed to 10+1=11.                     +\n");
    test<Real, 11, UnknownSize>();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with Size_ fixed to 10+1=1 andNzMax_ fixed to 7. +\n");
    test<Real, 11, 7>();

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for TestMemSAllocator+\n");
    stk_cout << _T("+ No errors detected.                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    stk_cerr << _T("In TestCallocator: An error occured : ") << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
