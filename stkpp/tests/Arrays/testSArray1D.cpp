/*--------------------------------------------------------------------*/
/*   Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the
  Free Software Foundation, Inc.,
  59 Temple Place,
  Suite 330,
  Boston, MA 02111-1307
  USA

  Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testArray1D.cpp
 *  @brief In this file we test the TContainer 1D classes.
 **/

#include "Arrays.h"

using namespace STK;


template< class Type, int Size_, int NzMax_ >
void print(SArray1D<Type, Size_, NzMax_> const& A, String const& name)
{
  Array1D<int, NzMax_>  idx(A.allocator().idx(), true);
  Array1D<Type, NzMax_> val(A.allocator().val(), true);
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")     << A.isRef()  << _T("\n");
  stk_cout << name << _T(".range() =")     << A.range()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T(".capacity() = ") << A.capacity() << _T("\n");
  stk_cout << name << _T(".idx() = ")      << idx;
  stk_cout << name << _T(".val() = ")      << val << _T("\n");
  stk_cout << name << _T("= ")             << A << _T("\n\n");
}

template< class Container1D >
void write_Array( Container1D const& C, String const& name, String const& message)
{
  stk_cout << _T("\n==>") << message;
  print(C, name);
}

/* template main test function for 1D arrays. */
template< class TYPE, int Size_, int NzMax_ >
void testArray( int M, int N, Range J, bool output)
{
  enum{ size_ = hidden::Traits< SArray1D<TYPE, Size_, NzMax_> >::size_};
  typedef typename hidden::Traits< SArray1D<TYPE, Size_, NzMax_> >::SubVector SubVector;
  typedef typename hidden::Traits< SArray1D<TYPE, Size_, NzMax_> >::Type Type;

  SArray1D<TYPE, Size_, NzMax_> B(N);
  if (output)
  { write_Array(B, _T("B"), _T("Test constructor:  B(N)\n"));}

  SArray1D<TYPE, Size_, NzMax_> C(N, 4);
  if (output)
  { write_Array(C, _T("C"), _T("Test constructor:  C(N,4)\n"));}

  SArray1D<TYPE, Size_, NzMax_> D(TRange<size_>(0,M, 0));
  if (output)
  { write_Array(D, _T("D"), _T("Test constructor: D(Range(0,M,0))\n"));}

  D= 3;
  if (output)
  { write_Array(D, _T("D"), _T("Test D = 3\n"));}

  B.exchange(D);
  if (output)
  { write_Array(B, _T("B"), _T("Test B.exchange(D);\n"));
    write_Array(D, _T("D"), _T(""));
  }

  SArray1D<TYPE, Size_, NzMax_> RefB(B, false);
  if (output)
  { write_Array(RefB, _T("RefB"), _T("Test copy constructor RefB(B, false)\n"));}

  SArray1D<TYPE, Size_, NzMax_> DRefB(B, true);
  if (output)
  { write_Array(DRefB, _T("DRefB"), _T("Test copy constructor DRefB(B, true)\n"));}
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(D.sub(J), _T("D.sub(J)"), _T("Test D.sub(J)\n"));
  }

  SubVector Dref(D.sub(J), true);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(Dref, _T("Dref"), _T("Test Dref(D.sub(J), true)\n"));
  }
  //
  D.sub(J)= Type(5);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(D, _T("D"), _T("Test D.sub(J) = 5\n"));
  }

  DRefB.sub(J)= Type(7);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(DRefB, _T("DRefB"), _T("Test DRefB.sub(J) = 7\n"));
    write_Array(B, _T("B"), _T("Test DRefB.sub(J) = 7\n"));
  }

  D.shift(1);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.shift(1)\n"));}

  D.pushBack(2); D.sub(Range(M, D.lastIdx(), 0)) = (1);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.pushBack(2); D.sub(Range(M,D.lastIdx()), 0) = 1\n"));}

  D.insertElt(2, 8); D.sub( Range(2, 8)) = (0);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.insertElt(2, 8); D.sub( Range(2, 8)) = (0);\n"));}

  D.erase(D.lastIdx());
  if (output)
  { write_Array(D, _T("D"), _T("Test D.erase(D.lastIdx())\n"));}

  D.erase(2, 2);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.erase(2,2)\n"));}

  D.push_back(6);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.push_back(6);\n"));}

  D.push_front(4);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.push_front(4);\n"));}

  D.resize(J);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.resize(J)\n"));}
}


/* main. */
int main(int argc, char *argv[])
{
  int M, N;
  int Jbeg, Jend;
  bool output = true;

  if (argc < 5)
  {
    M=12;
    N=15;
    Jbeg = 3;
    Jend = 9;
  }
  else
  {
    M = atoi(argv[1]);
    N = atoi(argv[2]);
    Jbeg = atoi(argv[3]);
    Jend = atoi(argv[4]);
    if (argc == 6) output = atoi(argv[5]);
  }

  try
  {
    Range J(Jbeg, Jend, 0);
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test SArray1D                                       +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Using M  = ") << M    << _T(" N    = ") << N  << _T("\n")
             << _T("J = ") << J << _T("\n");
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test SArray1D<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Real, UnknownSize , UnknownSize>(M, N, J, output);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test SArray1D<Real, UnknownSize , 25> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Real, UnknownSize , 25>(M, N, J, output);
    stk_cout << _T("\n\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test SArray1D<Real, 25 , 25> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Real, 25 , 25>(M, N, J, output);
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for STK::SArray1D  +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
