/*--------------------------------------------------------------------*/
/*   Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the
  Free Software Foundation, Inc.,
  59 Temple Place,
  Suite 330,
  Boston, MA 02111-1307
  USA

  Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testArray1D.cpp
 *  @brief In this file we test the TContainer 1D classes.
 **/

#include "Arrays.h"

using namespace STK;

template< class TYPE>
void print(List1D<TYPE> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")        << A.isRef()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".sizeCols() =")      << A.sizeCols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T(".sizeRows() =")      << A.sizeRows()  << _T("\n\n");
  stk_cout << name << _T("=\n")               << A << _T("\n\n");
}

template< class TYPE >
void print(Array1D<TYPE> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")     << A.isRef()  << _T("\n");
  stk_cout << name << _T(".range() =")     << A.range()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T(".capacity() = ") << A.capacity() << _T("\n");
  stk_cout << name << _T(".p_data() = ")   << A.allocator().p_data() << _T("\n");
  stk_cout << name << _T("=\n")            << A << _T("\n\n");
}

template< class Container1D >
void write_Array( Container1D const& C, String const& name, String const& message)
{
  stk_cout << _T("==>") << message;
  print(C, name);
}

// modification functions and functors
template< class Type >
void initfunction(Type& i) { i=-1;}
template< class Type >
void testFunction(Type& i) { i+=2;}

template< class Type >
struct testclass
{
  void operator() (Type& i) {i-=1;}
};

// const functions and functors
Real testsum;
template< class Type >
struct testConstclass
{
  Type sum_;
  void operator() ( Type const& i) {testsum += i; sum_ +=i;}
};
template< class Type >
void testConstFunction (Type const& i) { testsum +=i;}

/* template main test function for 1D arrays. */
template< class Type >
void testInnerIterator(int N, Range J, bool output)
{
 Array1D<Type> A(N);
 typename Array1D<Type>::InnerOperator it(A);
 std::for_each(A.beginIterator(), A.endIterator(), initfunction<Type>);

 stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
 stk_cout << _T("Test constant Inner iterators for Array1D : \n");
 for ( ; it; ++it)
 {
   stk_cout << _T("value =") << it.value();
   stk_cout << _T(", row =")   << it.row();
   stk_cout << _T(", col =")   << it.col() << _T("\n");
 }
 typename Array1D<Type>::InnerOperator it2(A);
 stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
 stk_cout << _T("Test Inner iterators for Array1D : \n");
 for ( ; it2; ++it2)
 {
   it2.valueRef() = it2.value()+it2.row();
   stk_cout << _T("value =") << it2.value();
   stk_cout << _T(", row =") << it2.row();
   stk_cout << _T(", col =") << it2.col() << _T("\n");
 }
}

/* template main test function for 1D arrays. */
template< class Type >
void testIterator(int N, Range J, bool output)
{
  testclass<Type> testobject;
  Array1D<Type> A(N);
  List1D<Type> B(N);

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test forward iterators for Array1D : \n");
  std::for_each(A.beginIterator(), A.endIterator(), initfunction<Type>);
  write_Array(A, _T("A"), _T("Test initfunction\n"));
  std::for_each(A.beginIterator(), A.endIterator(), testobject);
  write_Array(A, _T("A"), _T("Test testobject\n"));
  std::for_each(A.beginIterator(), A.endIterator(), testFunction<Type>);
  write_Array(A, _T("A"), _T("Test testFunction\n"));

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test reversed iterators for Array1D : \n");
  std::for_each(A.rbeginIterator(), A.rendIterator(), testFunction<Type>);
  write_Array(A, _T("A"), _T("Test testFunction\n"));
  std::for_each(A.rbeginIterator(), A.rendIterator(), testobject);
  write_Array(A, _T("A"), _T("Test testobject\n"));

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test forward iterators for List1D : \n");
  std::for_each(B.beginIterator(), B.endIterator(), initfunction<Type>);
  write_Array(B, _T("B"), _T("Test initfunction\n"));
  std::for_each(B.beginIterator(), B.endIterator(), testobject);
  write_Array(B, _T("B"), _T("Test testobject\n"));
  std::for_each(B.beginIterator(), B.endIterator(), testFunction<Type>);
  write_Array(B, _T("B"), _T("Test testFunction\n"));

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test reversed iterators for List1D : \n");
  std::for_each(B.rbeginIterator(), B.rendIterator(), testFunction<Type>);
  write_Array(B, _T("B"), _T("Test testFunction\n"));
  std::for_each(B.rbeginIterator(), B.rendIterator(), testobject);
  write_Array(B, _T("B"), _T("Test testobject\n"));

}

/* template main test function for 1D arrays. */
template< class Type >
void testConstIterator(int N, Range J, bool output)
{
  testConstclass<Type> testobject;
  testobject.sum_ = 0;
  testsum = 0;
  Array1D<Type> A(N, 1);
  List1D<Type> B(N, 1);

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test const forward iterators for Array1D: \n");
  std::for_each (A.beginConstIterator(), A.endConstIterator(), testConstFunction<Type>);
  stk_cout << _T("testsum  = ") << testsum << _T("\n");
  std::for_each(A.beginConstIterator(), A.endConstIterator(), testobject);
  stk_cout << _T("testobject.sum_  = ") << testobject.sum_ << _T("\n");
  stk_cout << _T("testsum  = ") << testsum << _T("\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test const reversed iterators for Array1D: \n");
  std::for_each (A.rbeginConstIterator(), A.rendConstIterator(), testConstFunction<Type>);
  stk_cout << _T("testsum  = ") << testsum << _T("\n");
  std::for_each(A.rbeginConstIterator(), A.rendConstIterator(), testobject);
  stk_cout << _T("testobject.sum_  = ") << testobject.sum_ << _T("\n");
  stk_cout << _T("testsum  = ") << testsum << _T("\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test const forward iterators for List1D: \n");
  stk_cout << _T("testsum  = ") << testsum << _T("\n");
  stk_cout << _T("testobject.sum_  = ") << testobject.sum_ << _T("\n");
  stk_cout << _T("testsum  = ") << testsum << _T("\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Test const reversed iterators for List1D: \n");
  std::for_each (B.rbeginConstIterator(), B.rendConstIterator(), testConstFunction<Type>);
  stk_cout << _T("testsum  = ") << testsum << _T("\n");
  std::for_each(B.rbeginConstIterator(), B.rendConstIterator(), testobject);
  stk_cout << _T("testobject.sum_  = ") << testobject.sum_ << _T("\n");
  stk_cout << _T("testsum  = ") << testsum << _T("\n");
}

/* main. */
int main(int argc, char *argv[])
{
  int N;
  int Jbeg, Jend;
  bool output = true;

  if (argc < 4)
  {
    N=15;
    Jbeg = 3;
    Jend = 9;
  }
  else
  {
    N = atoi(argv[1]);
    Jbeg = atoi(argv[2]);
    Jend = atoi(argv[3]);
    if (argc == 5) output = atoi(argv[4]);
  }

  try
  {
    Range J(Jbeg, Jend, 0);
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test 1D Arrays iterators                            +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Using N  = ") << N << _T(", J = ") << J << _T("\n");
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Iterators : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testIterator<Real>(N, J, output);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Constant Iterators : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testConstIterator<Real>(N, J, output);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Inner Iterators : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testInnerIterator<Real>(N, J, output);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for 1D Arrays       \n");
    stk_cout << _T("+ iterators; No errors detected.                      +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
