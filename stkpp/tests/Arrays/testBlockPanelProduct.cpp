/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::Arrays
 * created on: 1 janv. 2013
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testProducts.cpp
 *  @brief In this file we test the behavior of the matrix multiplication.
 **/

#include <STKernel.h>
#include "Arrays.h"

#ifdef _OPENMP
#include<omp.h>
#endif

using namespace STK;

#define nbCore 2

TReadWriteCsv<Real> tab;
Variable<Real> results;


/** @ingroup Arrays
  * @brief Visitor allowing to number to the element of an Arrays
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline  Type const& operator() ()
  { ++value_; return value_;}
};

/** @ingroup Arrays
  * Utility function for numbering an array
  **/
template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

int T=1000;
template<class Array, class Funct>
void meanTest(Array const& lhs, Array const& rhs, Array const& res1, Array& res2, Variable<Real>& results)
{
  // time variables
  CVectorX times(T), mins(T), maxs(T);
  Array diff;
  results.clear();
  for (int t =times.begin(); t<times.end(); t++)
  {
    res2 = 0;
    Chrono::start();
    Funct::run(lhs, rhs, res2);
    times[t] = Chrono::elapsed();
    Array diff = res1 - res2;
    maxs[t]  = diff.maxElt();
    mins[t]  = diff.minElt();
  }
  results.push_back(times.mean());
  results.push_back(maxs.mean());
  results.push_back(mins.mean());
}

template<class Array>
void testMult(int m, int p, int n)
{
  Array lhs(m, p), rhs(p, n), res1(m,n, 0.0), res2(m,n, 0.0);
  numbering(lhs); numbering(rhs);

  tab.clear();

  Chrono::start();
  res1 = mult(lhs, rhs);
  results.push_back(Chrono::elapsed());
  results.setName("mult");
  results.push_back(0.);
  results.push_back(0.);
  tab.push_back(results);

  meanTest<Array, hidden::BlockByPanel<Array, Array, Array> >(lhs, rhs, res1, res2, results);
  results.setName(" BlockByPanel");
  tab.push_back(results);

  meanTest<Array, hidden::PanelByBlock<Array, Array, Array> >(lhs, rhs, res1, res2, results);
  results.setName(" PanelByBlock");
  tab.push_back(results);

  meanTest<Array, hidden::BlockPanelProduct<Array, Array, Array> >(lhs, rhs, res1, res2, results);
  results.setName(" BlockPanelProduct");
  tab.push_back(results);

  meanTest<Array, hidden::PanelBlockProduct<Array, Array, Array> >(lhs, rhs, res1, res2, results);
  results.setName(" PanelBlockProduct");
  tab.push_back(results);

  tab.write(stk_cout);
}
/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
#ifdef _OPENMP
  int cores = nbCore;
  if (cores > 1) { omp_set_num_threads(cores);}
  else { omp_set_num_threads(1);}
#endif

  // time variables
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with ArrayXX                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << "m =64, p=8, n=256\n";
    testMult<ArrayXX>(64,8,256);
    stk_cout << "m =22, p=19, n=700\n";
    testMult<ArrayXX>(22,19,700);
    stk_cout << "m =700, p=19, n=22\n";
    testMult<ArrayXX>(700,19,22);
    stk_cout << "m =220, p=19, n=700\n";
    testMult<ArrayXX>(220,19,700);
    stk_cout << "m =700, p=19, n=220\n";
    testMult<ArrayXX>(700,19,220);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with CArrayXX                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << "m =64, p=8, n=256\n";
    testMult<CArrayXX>(64,8,256);
    stk_cout << "m =22, p=19, n=700\n";
    testMult<CArrayXX>(22,19,700);
    stk_cout << "m =700, p=19, n=22\n";
    testMult<CArrayXX>(700,19,22);
    stk_cout << "m =220, p=19, n=700\n";
    testMult<CArrayXX>(220,19,700);
    stk_cout << "m =700, p=19, n=220\n";
    testMult<CArrayXX>(700,19,220);

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for general products+\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}
  return 0;
}


