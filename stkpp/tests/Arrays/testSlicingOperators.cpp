/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testSlicingOperators.cpp
 *  @brief In this file we test the row, col, and sub operators.
 **/


#include <STKernel.h>
#include "Arrays.h"
#include "STatistiK.h"

using namespace STK;

template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline void operator() ( Type& value)
  { value = value_; ++value_;}
};

template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

/* main print. */
template<class CArray >
void printHead(CArray const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")    << A.isRef()  << _T("\n");
  stk_cout << name << _T(".rows() =")   << A.rows() << _T("\n");
  stk_cout << name << _T(".cols() =")   << A.cols() << _T("\n");
  stk_cout << name << _T(".sizeRows() =")   << A.sizeRows() << _T("\n");
  stk_cout << name << _T(".sizeCols() =")   << A.sizeCols() << _T("\n");
}

/* main print.*/
template<class CArray >
void print(CArray const& A, String const& name)
{
  printHead(A, name);
  stk_cout << name << _T("=\n");
  stk_cout << A;
}


template< class Type>
int Test( int M, int N)
{
  TRange<3> rows(1,3);
  Range     cols(1,4);
  CArray3X a(rows,cols);
  a << 0, 1, 2, 3
     , 4, 5, 6, 7
     , 4, 4, 4, 4;
  stk_cout << _T("++++++++++\n");
  stk_cout << _T("Test with matrix (1:3,1:4)\n");
  print(a, _T("a"));

  // test col operator
  stk_cout << _T("cola = a.col(2)\n");
  CVector3 cola = a.col(2);
  print(cola.transpose(), _T("cola.transpose()"));
  stk_cout << _T("a.col(2) = 1\n");
  a.col(2) = 1;
  print(a, _T("a"));

  stk_cout << _T("mu = a.col(2).safe(1).mean()\n");
  Type mu = a.col(2).safe(1).mean();
  stk_cout << _T("mu =") << mu <<_T("\n");

  stk_cout << _T("cola = ColAccessor<CArray3X>(a, 2)\n");
  cola = ColAccessor<CArray3X>(a, 2);
  print(cola.transpose(), _T("cola.transpose()"));

  stk_cout << _T("mu = ColAccessor<CArray3X>(a, 2).safe(1).mean()\n");
  mu = ColAccessor<CArray3X>(a, 2).safe(1).mean();
  stk_cout << _T("mu =") << mu <<_T("\n");

  // test row operator
  CPointX rowa =  a.row(1);
  print(rowa, _T("rowa"));
  a.row(1) = 1;
  print(a, _T("a"));

  stk_cout << _T("mu = a.row(1).safe(1).mean()\n");
  mu = a.row(1).safe(1).mean();
  stk_cout << _T("mu =") << mu <<_T("\n");

  stk_cout << _T("rowa = RowAccessor<CArray3X>(a, 1)\n");
  rowa = RowAccessor<CArray3X>(a, 1);
  print(cola.transpose(), _T("cola.transpose()"));

  stk_cout << _T("mu = RowAccessor<CArray3X>(a, 1).safe(1).mean()\n");
  mu = RowAccessor<CArray3X>(a, 1).safe(1).mean();
  stk_cout << _T("mu =") << mu <<_T("\n");



  return true;
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  try
  {
    int M, N;
    if (argc < 7)
    { M = 3; N = 4;}
    else
    {
      // dimensions
      M      = std::atoi(argv[1]);
      N      = std::atoi(argv[2]);
    }
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test SlicingOperators.                                     +\n");
    stk_cout << _T("+ Using: \n");
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T("\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test<Real> : \n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    // time variables
    Chrono::start();
    Test<Real>(M, N);
    Real test_time = Chrono::elapsed();
    stk_cout << _T("\n");
    stk_cout << _T("Time used : ") << test_time << _T("\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for UnaryOperators.+\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n";);
    return -1;
  }

  return 0;
}
