/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::Arrays
 * created on: 21 June 2013
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testBiProducts.cpp
 *  @brief In this file we test the behavior of the all bi-products.
 **/

#include "Arrays.h"

using namespace STK;

/** @ingroup Arrays
  * @brief compute the difference between two 2D arrays
  */
template <typename Lhs, typename Rhs>
Real diff2D(Lhs const& lhs, Rhs const& rhs)
{
  int i,j;
//  stk_cout << "lhs.cols()="<< lhs.cols() << "\n";
//  stk_cout << "lhs.rows()="<< lhs.rows() << "\n";
//  stk_cout << "rhs.cols()="<< rhs.cols() << "\n";
//  stk_cout << "rhs.rows()="<< rhs.rows() << "\n";
//  stk_cout << "mult(lhs, rhs).cols()="<< (lhs * rhs).cols() << "\n";
//  stk_cout << "mult(lhs, rhs).rows()="<< mult(lhs, rhs).rows() << "\n";
//  stk_cout << "(lhs * rhs).cols()="<< (lhs * rhs).cols() << "\n";
//  stk_cout << "(lhs * rhs).rows()="<< (lhs * rhs).rows() << "\n";
//  stk_cout << "mult(lhs, rhs).cols()="<< (lhs * rhs).cols() << "\n";
//  stk_cout << "mult(lhs, rhs).rows()="<< mult(lhs, rhs).rows() << "\n";
  return ((lhs * rhs)-mult(lhs, rhs)).abs().maxElt(i,j);
}

/** @ingroup Arrays
  * @brief compute the difference between two 2D arrays
  */
template <typename Lhs, typename Rhs>
ArrayXX fastProd(Lhs const& lhs, Rhs const& rhs)
{
  ArrayXX res(lhs.rows(), rhs.cols(), 0.);
  if (lhs.cols() != rhs.rows()) return res;
  for (int i=lhs.beginRows(); i<lhs.endRows(); i++)
    for (int j=rhs.beginCols(); j<rhs.endCols(); j++)
    {
      Real sum = 0.0;
      for (int k=lhs.beginCols(); k< lhs.endCols(); k++)
      {
        sum += lhs(i,k) * rhs(k,j);
      }
      res(i,j) = sum;
    }
  return res;
}
/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test all products                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

  ArrayXX General10x8(10, 8), General8x12(8, 12);
  CArray<Real, 10, 8> CGeneral10x8;
  CArray<Real, 8, 12> CGeneral8x12;

  ArraySquareX Square8(8), Square10(10);
  CArraySquare<Real, 8> CSquare8;
  CArraySquare<Real, 10> CSquare10;

  VectorX Vector10(10), Vector8(8);
  CArrayVector<Real, 10> CVector10;
  CArrayVector<Real, 8> CVector8;

  PointX Point10(10), Point8(8);
  CArrayPoint<Real, 10> CPoint10;
  CArrayPoint<Real, 8> CPoint8;

  Array2DDiagonal<Real> Diagonal8(8), Diagonal10(10);

  Array2DLowerTriangular<Real> LowTriangular10x8(10, 8), LowTriangular8x12(8, 12);
  Array2DUpperTriangular<Real> UpTriangular10x8(10, 8), UpTriangular8x12(8, 12);

  Array2DNumber<Real> num;
  CArrayNumber<Real> cnum;

  // initialize
  General10x8.randGauss();  CGeneral10x8.randGauss();
  General8x12.randGauss();  CGeneral8x12.randGauss();

  Square10.randGauss();  CSquare10.randGauss();
  Square8.randGauss();   CSquare8.randGauss();

  Point10.randGauss();  Point8.randGauss();
  CPoint10.randGauss();  CPoint8.randGauss();

  Vector10.randGauss();  Vector8.randGauss();
  CVector10.randGauss();  CVector8.randGauss();

  Diagonal10.randGauss(); Diagonal8.randGauss();

  LowTriangular8x12.randGauss();  LowTriangular10x8.randGauss();
  UpTriangular8x12.randGauss();  UpTriangular10x8.randGauss();

  num.randGauss(); cnum.randGauss();

  // General lhs
  stk_cout << _T("\nGeneral Lhs.\n");
  try
  {
    stk_cout << _T("General by General. diff =") << diff2D(General10x8, General8x12)  << _T("\n");
    stk_cout << _T("General by CGeneral. diff =") << diff2D(General10x8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("General by Square. diff =") << diff2D(General10x8, Square8)  << _T("\n");
    stk_cout << _T("General by CSquare. diff =") << diff2D(General10x8, CSquare8)  << _T("\n");

    stk_cout << _T("General by LowerTriangular. diff =") << diff2D(General10x8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("General by UpperTriangular. diff =") << diff2D(General10x8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("General by Diagonal. diff =") << diff2D(General10x8, Diagonal8)  << _T("\n");
    stk_cout << _T("General by Vector. diff =") << diff2D(General10x8, Vector8)  << _T("\n");
    stk_cout << _T("General by CVector. diff =") << diff2D(General10x8, CVector8)  << _T("\n");

//    stk_cout << _T("General by Number. diff =") << diff2D(General10x8, num)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}
  // CGeneral lhs
  stk_cout << _T("\nCGeneral Lhs.\n");
  try
  {
    stk_cout << _T("CGeneral by General. diff =") << diff2D(CGeneral10x8, General8x12)  << _T("\n");
    stk_cout << _T("CGeneral by CGeneral. diff =") << diff2D(CGeneral10x8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("CGeneral by Square. diff =") << diff2D(CGeneral10x8, Square8)  << _T("\n");
    stk_cout << _T("CGeneral by CSquare. diff =") << diff2D(General10x8, CSquare8)  << _T("\n");

    stk_cout << _T("CGeneral by LowerTriangular. diff =") << diff2D(CGeneral10x8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("CGeneral by UpperTriangular. diff =") << diff2D(CGeneral10x8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("CGeneral by Diagonal. diff =") << diff2D(CGeneral10x8, Diagonal8)  << _T("\n");
    stk_cout << _T("CGeneral by Vector. diff =") << diff2D(CGeneral10x8, Vector8)  << _T("\n");
    stk_cout << _T("CGeneral by CVector. diff =") << diff2D(CGeneral10x8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // Square lhs
  stk_cout << _T("\nSquare Lhs.\n");
  try
  {
    stk_cout << _T("Square by General. diff =") << diff2D(Square8, General8x12)  << _T("\n");
    stk_cout << _T("Square by CGeneral. diff =") << diff2D(Square8, CGeneral8x12)  << _T("\n");
    stk_cout << _T("Square by Square. diff =") << diff2D(Square8, Square8)  << _T("\n");
    stk_cout << _T("Square by CSquare. diff =") << diff2D(Square8, CSquare8)  << _T("\n");

    stk_cout << _T("Square by LowerTriangular. diff =") << diff2D(Square8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("Square by UpperTriangular. diff =") << diff2D(Square8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("Square by Diagonal. diff =") << diff2D(Square8, Diagonal8)  << _T("\n");
    stk_cout << _T("Square by Vector. diff =") << diff2D(Square8, Vector8)  << _T("\n");
    stk_cout << _T("Square by CVector. diff =") << diff2D(Square8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // CSquare lhs
  stk_cout << _T("\nCSquare Lhs.\n");
  try
  {
    stk_cout << _T("CSquare by General. diff =") << diff2D(CSquare8, General8x12)  << _T("\n");
    stk_cout << _T("CSquare by CGeneral. diff =") << diff2D(CSquare8, CGeneral8x12)  << _T("\n");
    stk_cout << _T("CSquare by Square. diff =") << diff2D(CSquare8, Square8)  << _T("\n");
    stk_cout << _T("CSquare by CSquare. diff =") << diff2D(CSquare8, CSquare8)  << _T("\n");

    stk_cout << _T("CSquare by LowerTriangular. diff =") << diff2D(CSquare8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("CSquare by UpperTriangular. diff =") << diff2D(CSquare8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("CSquare by Diagonal. diff =") << diff2D(CSquare8, Diagonal8)  << _T("\n");
    stk_cout << _T("CSquare by Vector. diff =") << diff2D(CSquare8, Vector8)  << _T("\n");
    stk_cout << _T("CSquare by CVector. diff =") << diff2D(CSquare8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // Diagonal lhs
  stk_cout << _T("\nDiagonal Lhs.\n");
  try
  {
    stk_cout << _T("Diagonal by General. diff =") << diff2D(Diagonal8, General8x12)  << _T("\n");
    stk_cout << _T("Diagonal by CGeneral. diff =") << diff2D(Diagonal8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("Diagonal by Square. diff =") << diff2D(Diagonal8, Square8)  << _T("\n");
    stk_cout << _T("Diagonal by CSquare. diff =") << diff2D(Diagonal8, CSquare8)  << _T("\n");

    stk_cout << _T("Diagonal by LowerTriangular. diff =") << diff2D(Diagonal8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("Diagonal by UpperTriangular. diff =") << diff2D(Diagonal8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("Diagonal by Diagonal. diff =") << diff2D(Diagonal8, Diagonal8)  << _T("\n");
    stk_cout << _T("Diagonal by Vector. diff =") << diff2D(Diagonal8, Vector8)  << _T("\n");
    stk_cout << _T("Diagonal by CVector. diff =") << diff2D(Diagonal8, CVector8)  << _T("\n");

    CArray<Real, UnknownSize, UnknownSize, Arrays::by_col_> Ac(8, 4), aux;
    CArray<Real, UnknownSize, UnknownSize, Arrays::by_row_> Ar(8, 4);
    Ac.randGauss(); Ar.randGauss();
    aux = Diagonal8 * Ac; Ac = Diagonal8 * Ac;
    stk_cout << _T("Diagonal by Array by col in place. diff =") << (aux-Ac).abs().maxElt()  << _T("\n");
    aux = Diagonal8 * Ar; Ar = Diagonal8 * Ar;
    stk_cout << _T("Diagonal by Array by row in place. diff =") << (aux-Ar).abs().maxElt()  << _T("\n");

  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // Diagonal lhs
  stk_cout << _T("\nLowTriangular Lhs.\n");
  try
  {
    stk_cout << _T("LowTriangular by General. diff =") << diff2D(LowTriangular10x8, General8x12)  << _T("\n");
    stk_cout << _T("LowTriangular by CGeneral. diff =") << diff2D(LowTriangular10x8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("LowTriangular by Square. diff =") << diff2D(LowTriangular10x8, Square8)  << _T("\n");
    stk_cout << _T("LowTriangular by CSquare. diff =") << diff2D(LowTriangular10x8, CSquare8)  << _T("\n");

    stk_cout << _T("LowTriangular by LowerTriangular. diff =") << diff2D(LowTriangular10x8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("LowTriangular by UpperTriangular. diff =") << diff2D(LowTriangular10x8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("LowTriangular by Diagonal. diff =") << diff2D(LowTriangular10x8, Diagonal8)  << _T("\n");
    stk_cout << _T("LowTriangular by Vector. diff =") << diff2D(LowTriangular10x8, Vector8)  << _T("\n");
    stk_cout << _T("LowTriangular by CVector. diff =") << diff2D(LowTriangular10x8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // UpperTriangular lhs
  stk_cout << _T("\nUpperTriangular Lhs.\n");
  try
  {
    stk_cout << _T("UpperTriangular by General. diff =") << diff2D(UpTriangular10x8, General8x12)  << _T("\n");
    stk_cout << _T("UpperTriangular by CGeneral. diff =") << diff2D(UpTriangular10x8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("UpperTriangular by Square. diff =") << diff2D(UpTriangular10x8, Square8)  << _T("\n");
    stk_cout << _T("UpperTriangular by CSquare. diff =") << diff2D(UpTriangular10x8, CSquare8)  << _T("\n");

    stk_cout << _T("UpperTriangular by LowerTriangular. diff =") << diff2D(UpTriangular10x8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("UpperTriangular by UpperTriangular. diff =") << diff2D(UpTriangular10x8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("UpperTriangular by Diagonal. diff =") << diff2D(UpTriangular10x8, Diagonal8)  << _T("\n");
    stk_cout << _T("UpperTriangular by Vector. diff =") << diff2D(UpTriangular10x8, Vector8)  << _T("\n");
    stk_cout << _T("UpperTriangular by CVector. diff =") << diff2D(UpTriangular10x8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // vector lhs
  stk_cout << _T("\nVector Lhs.\n");
  try
  {
    stk_cout << _T("Vector by Point. diff =") << diff2D(Vector10, Point8)  << _T("\n");
    stk_cout << _T("Vector by CPoint. diff =") << diff2D(Vector10, CPoint8) << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}


  // cvector lhs
  stk_cout << _T("\nCVector Lhs.\n");
  try
  {
    stk_cout << _T("CVector by Point. diff =") << diff2D(CVector10, Point8)  << _T("\n");
    stk_cout << _T("CVector by CPoint. diff =") << diff2D(CVector10, CPoint8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // Point lhs
  stk_cout << _T("\nPoint Lhs.\n");
  try
  {
    stk_cout << _T("Point by General. diff =") << diff2D(Point8, General8x12)  << _T("\n");
    stk_cout << _T("Point by CGeneral. diff =") << diff2D(Point8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("Point by Square. diff =") << diff2D(Point8, Square8)  << _T("\n");
    stk_cout << _T("Point by CSquare. diff =") << diff2D(Point8, CSquare8)  << _T("\n");

    stk_cout << _T("Point by LowerTriangular. diff =") << diff2D(Point8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("Point by UpperTriangular. diff =") << diff2D(Point8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("Point by Diagonal. diff =") << diff2D(Point8, Diagonal8)  << _T("\n");

    stk_cout << _T("Point by Vector. diff =") << diff2D(Point8, Vector8)  << _T("\n");
    stk_cout << _T("Point by CVector. diff =") << diff2D(Point8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  // CPoint lhs
  stk_cout << _T("\nCPoint Lhs.\n");
  try
  {
    stk_cout << _T("CPoint by General. diff =") << diff2D(CPoint8, General8x12)  << _T("\n");
    stk_cout << _T("CPoint by CGeneral. diff =") << diff2D(CPoint8, CGeneral8x12)  << _T("\n");

    stk_cout << _T("CPoint by Square. diff =") << diff2D(CPoint8, Square8)  << _T("\n");
    stk_cout << _T("CPoint by CSquare. diff =") << diff2D(CPoint8, CSquare8)  << _T("\n");

    stk_cout << _T("CPoint by LowerTriangular. diff =") << diff2D(CPoint8, LowTriangular8x12)  << _T("\n");
    stk_cout << _T("CPoint by UpperTriangular. diff =") << diff2D(CPoint8, UpTriangular8x12)  << _T("\n");

    stk_cout << _T("CPoint by Diagonal. diff =") << diff2D(CPoint8, Diagonal8)  << _T("\n");

    stk_cout << _T("CPoint by Vector. diff =") << diff2D(CPoint8, Vector8)  << _T("\n");
    stk_cout << _T("CPoint by CVector. diff =") << diff2D(CPoint8, CVector8)  << _T("\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n");
    return -1;
  }


  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for all products   +\n");
  stk_cout << _T("+ No errors detected.                                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");


  stk_cout << _T("\n\n");
  return 0;
}



