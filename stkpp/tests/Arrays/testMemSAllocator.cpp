/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-204  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/*
 * Project:  stkpp::Arrays
 * created on: 13 sept. 204
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testCAllocator.cpp
 *  @brief In this file the CAllocator class.
 **/


#include <STKernel.h>

#include "Arrays/include/allocators/STK_MemSAllocator.h"
#include "Arrays/include/STK_Array2D.h"
#include "Arrays/include/STK_Display.h"


using namespace STK;

/** @brief Write a pair in the form (first;last) in output stream.
 *  @param os output stream
 *  @param I the Range to write
 **/
template<typename Type>
ostream& operator<< (ostream& os, std::pair<int, Type> const& I)
{
  os << _T("(") <<  I.first << _T(";") << _T(")") << I.second;
  return os;
}

/* main print. */
template< typename Type_=Real, int NzMax_ = UnknownSize, int Size_ = UnknownSize>
void print(MemSAllocator<Type_, NzMax_, Size_> const& A, String const& name)
{
  Range rows   = decLast(A.ptr().range());
  Array1D<int>   idxFirst;
  Array1D<Type_> idxSecond;
  Array2D<Type_> mat(rows, rows, Type_(0));

  for (int i = A.idx().begin(); i < A.idx().end(); ++i)
  {
    if(A.idx()[i].first == Arithmetic<Real>::NA()) break;
    idxFirst.push_back(A.idx()[i].first);
    idxSecond.push_back(A.idx()[i].second);
  }

  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".ptr().range() =")  << A.ptr().range() << _T("\n");
  stk_cout << name << _T(".idx().range() =")  << A.idx().range() << _T("\n");
  stk_cout << name << _T(".ptr() =")  << A.ptr();
  if (A.ptr().size() == 0) stk_cout <<_T("\n");
  stk_cout << name << _T(".idx().first =")  << idxFirst;
  if (A.idx().size() == 0) stk_cout <<_T("\n");
  stk_cout << name << _T(".idx().second =")  << idxSecond;
  if (A.idx().size() == 0) stk_cout <<_T("\n");
  // fill matrix
  for (int i = mat.beginRows(); i <mat.endRows(); ++i)
  {
    for (int j = mat.beginCols(); j <mat.endCols(); ++j)
    { mat(i,j) = A.getValue(i,j);}
  }
  stk_cout << _T("mat =\n")   << mat << _T("\n");
}

template< typename Type_=Real, int NzMax_ = UnknownSize, int Size_ = UnknownSize>
void test()
{
  MemSAllocator<Type_, NzMax_, Size_> m0;
  print(m0, "m0, Empty allocator");

  //
  MemSAllocator<Type_, NzMax_, Size_> m1(_R(1,10));
  print(m1, "m1(_R(1,10)), Allocator with 10 rows/columns");
  // add values
  stk_cout << "m1, add (2,2,1/10) entry\n";
  m1.addValue(2, 2, 1./10);
  print(m1, "1 entry");

  stk_cout << "m1, add (2,2,0) entry\n";
  m1.addValue(2, 2, 0.);
  print(m1, "0 entry");

  stk_cout << "m1, add (2,2,1/10) entry\n";
  m1.addValue(2, 2, 1./10);
  print(m1, "1 entry");

  stk_cout << "m1, add (2,4,2/10) entry\n";
  m1.addValue(2, 4, 2./10);
  print(m1, "2 entries");
  stk_cout << "m1, add (3,4,3/10) entry\n";
  m1.addValue(3, 4, 3./10);
  print(m1, "3 entries");
  stk_cout << "m1, add (1,1,4/10) entry\n";
  m1.addValue(1, 1, 4./10);
  print(m1, "4 entries");
  stk_cout << "m1, add (2,8,5/10) entry\n";
  m1.addValue(2, 8, 5./10);
  print(m1, "5 entries");

  stk_cout << "m1, add (2,6,6/10) entry\n";
  m1.addValue(2, 6, 6./10);
  print(m1, "6 entries");
  // remove entry
  stk_cout << "m1, add (2,6,0) entry\n";
  m1.addValue(2, 6, 0.);
  print(m1, "5 entries");

  stk_cout << "m1, add (2,6,6/10) entry\n";
  m1.addValue(2, 6, 6./10);
  print(m1, "6 entries");

  stk_cout << "m1, add (10,10,7/10) entry\n";
  m1.addValue(10, 10, 7./10);
  print(m1, "7 entries");
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
//  try
//  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ TestMemSAllocator.                                    +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with unknown NzMax_.                              +\n");
    test<Real, UnknownSize, UnknownSize>();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with NzMax_ fixed to 7. +\n");
    test<Real, 7, UnknownSize>();

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with Size_ fixed to 10. +\n");
    test<Real, UnknownSize, 10>();

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test with NzMax_ fixed to 7, Size_ fixed to 10. +\n");
    test<Real, 7, 10>();

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for TestMemSAllocator+\n");
    stk_cout << _T("+ No errors detected.                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
//  }
//  catch (Exception const& error)
//  {
//    stk_cerr << _T("In TestMemSAllocator: An error occured : ") << error.error() << _T("\n";);
//    return -1;
//  }

  return 0;
}
