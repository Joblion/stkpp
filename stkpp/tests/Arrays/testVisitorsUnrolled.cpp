/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::Arrays
 * created on: 21 June 2013
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testVisitorsUnrolled.cpp
 *  @brief In this file we test the behavior of the visitors when they are unrolled.
 **/

#include "Arrays.h"

using namespace STK;

/** @ingroup Arrays
  * @brief compute the difference between two 2D arrays
  */
template <typename Lhs, typename Rhs>
Real diff2D(Lhs const& lhs, Rhs const& rhs)
{
  int i,j;
  return ((lhs * rhs)-mult(lhs, rhs)).abs().maxElt(i,j);
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  int i,j;
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test unrolled visitors with CArray                  +\n");

    CArray<Real, 4, 4> c1;
    CArray<Real, 4, 4, Arrays::by_row_> c1bis;
    c1.setZeros(); c1bis.setZeros();
    c1.sum(); c1bis.sum();
    CArray<Real, 12, 4> c2;
    CArray<Real, 12, 4, Arrays::by_row_> c2bis;
    c2.setZeros(); c2bis.setZeros();
    c2.sum(); c2bis.sum();
    CArray<Real, 4, 12> c3;
    CArray<Real, 4, 12, Arrays::by_row_> c3bis;
    c3.setZeros(); c3bis.setZeros();
    c3.sum(); c3bis.sum();
    CArray<Real, 12, 12> c4;
    CArray<Real, 12, 12, Arrays::by_row_> c4bis;
    c4.setZeros(); c4bis.setZeros();
    c4.sum(); c4bis.sum();
    CArray<Real, 4, 22> c5;
    CArray<Real, 4, 22, Arrays::by_row_> c5bis;
    c5.setZeros(); c5bis.setZeros();
    c5.sum(); c5bis.sum();
    CArray<Real, 22, 4> c6;
    CArray<Real, 22, 4, Arrays::by_row_> c6bis;
    c6.setZeros(); c6bis.setZeros();
    c6.sum(); c6bis.sum();
    CArray<Real, 22, 22> c7;
    CArray<Real, 22, 22, Arrays::by_row_> c7bis;
    c7.setZeros(); c7bis.setZeros();
    c7.sum(); c7bis.sum();
    CArray<Real, 4, UnknownSize> c8;
    CArray<Real, 4, UnknownSize, Arrays::by_row_> c8bis;
    c8.setZeros(); c8bis.setZeros();
    c8.sum(); c8bis.sum();
    CArray<Real, 12, UnknownSize> c9;
    CArray<Real, 12, UnknownSize, Arrays::by_row_> c9bis;
    c9.setZeros(); c9bis.setZeros();
    c9.sum(); c9bis.sum();
    CArray<Real, 22, UnknownSize> c10;
    CArray<Real, 22, UnknownSize, Arrays::by_row_> c10bis;
    c10.setZeros(); c10bis.setZeros();
    c10.sum(); c10bis.sum();
    CArray<Real, UnknownSize, 4> c11;
    CArray<Real, UnknownSize, 4, Arrays::by_row_> c11bis;
    c11.setZeros(); c11bis.setZeros();
    c11.sum(); c11bis.sum();
    CArray<Real, UnknownSize, 12> c12;
    CArray<Real, UnknownSize, 12, Arrays::by_row_> c12bis;
    c12.setZeros(); c12bis.setZeros();
    c12.sum(); c12bis.sum();
    CArray<Real, UnknownSize, 22> c13;
    CArray<Real, UnknownSize, 22, Arrays::by_row_> c13bis;
    c13.setZeros(); c13bis.setZeros();
    c13.sum(); c13bis.sum();

    stk_cout << _T("\n\n");
    stk_cout << _T("+ Test unrolled visitors with CArray done             +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  Array2DSquare<Real> square(5);
  CArraySquare<Real, 5> csquare1;
  CArraySquare<Real, 5, Arrays::by_row_> csquare2;
  square  << 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9;
  csquare1  << 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9;
  csquare2  << 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9
           , 1, 3, 4, 6, 9;
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("Test visitors for square arrays.\n");
    stk_cout << _T("square =\n") << square;
    stk_cout << _T("csquare1 =\n") << csquare1;
    stk_cout << _T("csquare2 =\n") << csquare2;
    stk_cout << _T("square.sum() =") << square.sum() << _T("\n");
    stk_cout << _T("csquare1.sum() =") << csquare1.sum() << _T("\n");
    stk_cout << _T("csquare2.sum() =") << csquare2.sum() << _T("\n");
    stk_cout << _T("square.maxElt(i,j) =") << square.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
    stk_cout << _T("csquare1.maxElt(i,j) =") << csquare1.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
    stk_cout << _T("csquare2.maxElt(i,j) =") << csquare2.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  Array2DVector<Real> vector(5);
  CArrayVector<Real, 5> cvector(5);
  vector << 1, 3, 4, 6, 9;
  cvector << 1, 3, 4, 6, 9;
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("Test visitors for vector arrays.\n");
    stk_cout << _T("vector =\n") << vector;
    stk_cout << _T("cvector =\n") << cvector;
    stk_cout << _T("vector.sum() =") << vector.sum() << _T("\n");
    stk_cout << _T("cvector.sum() =") << cvector.sum() << _T("\n");
    stk_cout << _T("vector.maxElt(i,j) =") << vector.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
    stk_cout << _T("cvector.maxElt(i,j) =") << cvector.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n"); return -1;}

  Array2DPoint<Real> point(5);
  CArrayPoint<Real> cpoint(5);
  point << 1, 3, 4, 6, 9;
  cpoint << 1, 3, 4, 6, 9;
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("Test visitors for point arrays.\n");
    stk_cout << _T("point =\n") << point;
    stk_cout << _T("cpoint =\n") << cpoint;
    stk_cout << _T("point.sum() =") << point.sum() << _T("\n");
    stk_cout << _T("cpoint.sum() =") << cpoint.sum() << _T("\n");
    stk_cout << _T("point.maxElt(i,j) =") << point.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
    stk_cout << _T("cpoint.maxElt(i,j) =") << cpoint.maxElt(i,j);
    stk_cout << _T(" At position(") << i <<_T(",") << j << _T(")\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n");
    return -1;
  }


  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for dot products   +\n");
  stk_cout << _T("+ No errors detected.                                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");


  stk_cout << _T("\n\n");
  return 0;
}



