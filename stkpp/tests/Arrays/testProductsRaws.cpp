/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  stkpp::Arrays
 * created on: 1 janv. 2017
 * Author:   iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 **/

/** @file testProductsRaws.cpp
 *  @brief In this file we test the behavior of the matrix multiplication.
 **/

#include <STKernel.h>
#include "Arrays.h"
#include "STatistiK.h"

using namespace STK;

/** @ingroup Arrays
  * @brief allow to number to the element of an Arrays
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline Type const& operator() ()
  { ++value_; return value_;}
};

/** @ingroup
 *  Utility function for numbering an array
 * */
template<typename Array>
void numbering(Array& matrix)
{
  typedef typename hidden::Traits<Array>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

template<int M, int N>
void pointByArray(ArrayXX& results)
{
  enum
  {
    sizeRows_ = 1,
    nbRow_ = (M == UnknownSize) ? 20 : M,
    nbCol_ = (N == UnknownSize) ? 1000 : N,
    sizeCols_ = nbCol_,
    orient_ = Arrays::by_row_
  };

  // typedef
  typedef CArray<Real, M, N, Arrays::by_col_> ArrayByCol;
  typedef CArray<Real, M, N, Arrays::by_row_> ArrayByRow;
  typedef CArrayPoint<Real, M, Arrays::by_row_> Point;
  typedef CAllocator<Real, sizeRows_, sizeCols_, orient_> Allocator;

  typedef hidden::MultPointArray<Point, ArrayByCol, Allocator> MultByColNew;
  typedef hidden::vb<Point, ArrayByCol, Allocator> MultByColOld;

  typedef hidden::MultPointArray<Point, ArrayByRow, Allocator> MultByRowNew;
  typedef hidden::vb<Point, ArrayByRow, Allocator> MultByRowOld;

  //typedef CArrayPoint<Real, M, Arrays::by_row_> ResultByRow;
  Law::Normal gen;
  Real elapsedTime;

  //---------------------------------
  // create data sets
  Point ptc(nbRow_); numbering(ptc);
  Point ptr(nbRow_); numbering(ptr);
  ArrayByCol ac(nbRow_, nbCol_); ac.rand(gen);
  ArrayByRow ar(nbRow_, nbCol_); ar.rand(gen);
  ptc /= ptc.norm();
  for (int j=ac.beginCols(); j<ac.endCols(); ++j)
  { ac.col(j) -= ptc * ac.col(j).dot(ptc);}
  ptr /= ptr.norm();
  for (int j=ar.beginCols(); j<ar.endCols(); ++j)
  { ar.col(j) -= ptr * ar.col(j).dot(ptr);}

  //---------------------------------
  // Store results
  Allocator result_new(1, nbCol_);
  Allocator result_old(1, nbCol_);
  result_new.shift(ptc.beginRows(), ac.beginCols());
  result_old.shift(ptc.beginRows(), ac.beginCols());
  VectorX resByCol(_R(0,4)), resByRow(_R(0,4));
  resByCol[0] = nbRow_;
  resByRow[0] = nbRow_;

  //---------------------------------
  // by col
  result_new.setValue(0);
  Chrono::start();
  MultByColNew::run(ptc, ac, result_new);
  elapsedTime = Chrono::elapsed();
  resByCol[1] = elapsedTime;
  result_old.setValue(0);
  Chrono::start();
  MultByColOld::run(ptc, ac, result_old);
  elapsedTime = Chrono::elapsed();
  resByCol[2] = elapsedTime;

  resByCol[3] = result_new.elt(1);
  resByCol[4] = result_old.elt(1);

  results.merge(resByCol);

  //---------------------------------
  // by row
  result_new.setValue(0);
  Chrono::start();
  MultByRowNew::run(ptr, ar, result_new);
  elapsedTime = Chrono::elapsed();
  resByRow[1] = elapsedTime;
  result_old.setValue(0);
  Chrono::start();
  MultByRowOld::run(ptr, ar, result_old);
  //pres = ptc *ac;
  elapsedTime = Chrono::elapsed();
  resByRow[2] = elapsedTime;

  resByRow[3] = result_new.elt(1);
  resByRow[4] = result_old.elt(1);

  results.merge(resByRow);
}

void testPointByArray()
{
  ArrayXX results;
  pointByArray<26, UnknownSize>(results);
  pointByArray<260, UnknownSize>(results);
  pointByArray<261, UnknownSize>(results);
  pointByArray<26000, UnknownSize>(results);
  pointByArray<26001, UnknownSize>(results);
  stk_cout << _T(" results=\n") << results;
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  // time variables
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test  point by array product with Array2D           +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testPointByArray();

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for big dimension  +\n");
    stk_cout << _T("+  products. No errors detected.                      +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  { stk_cerr << error.error() << _T("\n");
    return -1;
  }
  return 0;
}



