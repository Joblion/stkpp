/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing binary operators classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testAllBinaryOperators.cpp
 *  @brief In this file we test the all the Binary operators.
 **/

#include "STKpp.h"

using namespace STK;

/** @ingroup Arrays
  * @brief Base class to implement min and max visitors.
  * Allow to find the
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline Type const& operator() ()
  { value_++; return value_;}
};

/** number the element of a container in the order they are visited.
  * @sa  maxElt(Derived const&, int&, int&), minElt()
  */
template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

/* main print. */
template<class CArray >
void printHead(CArray const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T("=\n");
}

/* main print.*/
template<class CArray >
void print(CArray const& A, String const& name)
{
  printHead(A, name);
  stk_cout << A;
}


template<int M, int N, class Type>
int Test()
{
  enum
  {
    sizeRows_ = (M == UnknownSize) ? 5 : M,
    sizeCols_ = (N == UnknownSize) ? 10 : N
  };
  CArray<Type, M, N> a; numbering(a);
  CArray<Type, M, N> b; numbering(b);
  CArrayPoint<Type, M> p; p = Type(1);
  CArrayVector<Type, M> v; v = Type(1);

  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator==\n");
  print(a==b, "EqualOp");
  print(a==(p*v), "equalWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator!=\n");
  print(a!=b, "notEqualOp_");
  print(a!=p*v, "notEqualWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator>\n");
  print(a>b, "greaterOp_");
  print(a>p*v, "greaterThanOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator<\n");
  print(a<b, "lessOp_");
  print(a<p*v, "lessThanOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator>=\n");
  print(a>=b, "geqOp_");
  print(a>=p*v, "greaterThanOrEqualOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator=\n");
  print(a<=b, "leqOp_");
  print(a<=p*v, "lessThanOrEqualOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator+\n");
  print(a+b, "sumOp_");
  print(a+p*v, "sumWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator-\n");
  print(a-b, "differenceOp_");
  print(a-p*v, "differenceWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator prod\n");
  print(a.prod(b), "productOp_");
  print(a.prod(p*v), "productWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator/\n");
  print(a/b, "divisionOp_");
  print(a/(p*v), "divisionWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator min\n");
  print(a.min(b), "minOp_");
  print(a.min(p*v), "minWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator max\n");
  print(a.max(b), "maxOp_");
  print(a.max(p*v), "maxWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator&&\n");
  print(a && b, "logicalAndOp_");
  print(a && (p*v), "logicalAndWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator||\n");
  print(a || b, "logicalOrOp_");
  print(a || (p*v), "logicalOrWithOp_");

  return 0;
}

template<int M, int N, class Type>
int TestAssign()
{
  enum
  {
    sizeRows_ = (M == UnknownSize) ? 5 : M,
    sizeCols_ = (N == UnknownSize) ? 10 : N
  };
  CArray<Type, M, M> a; numbering(a);
  CArray<Type, M, M> b; numbering(b);
  CArrayPoint<Type, M> p; p = Type(1);
  CArrayVector<Type, M> v; v = Type(1);


  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator+=\n");
  print(a+=b, "sumOp_");
  print(a+=p*v, "sumWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator-=\n");
  print(a-=b, "differenceOp_");
  print(a-=p*v, "differenceWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator/=\n");
  print(a/=b, "divisionOp_");
  print(a/=(p*v), "divisionWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator*=\n");
  print(a*=b, "multOp_");
  // print(a*=(p*v), "divisionWithOp_"); // TODO

  return 0;
}

template<int M, int N>
int TestInt()
{
  enum
  {
    sizeRows_ = (M == UnknownSize) ? 5 : M,
    sizeCols_ = (N == UnknownSize) ? 10 : N
  };
  CArray<int, M, N> a; numbering(a);
  CArray<int, M, N> b; numbering(b);
  CArrayPoint<int, M> p; p = int(1);
  CArrayVector<int, M> v; v = int(1);

  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator%\n");
  print(a%b, "moduloOp_");
  print(a%(p*v), "moduloWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator&\n");
  print(a&b, "bitwiseAndOp_");
  print(a&(p*v), "bitwiseAndWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator|\n");
  print(a|b, "bitwiseOrOp_");
  print(a|(p*v), "bitwiseOrWithOp_");
  stk_cout << _T("+++++++++++++++++++\n");
  stk_cout << _T("Test operator^\n");
  print(a^b, "bitwiseXorOp_");
  print(a^(p*v), "bitwiseXorWithOp_");
  return 0;
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  Real test_time;
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test all Binary operators                           +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test program with Real : \n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    Chrono::start();
    Test<5, 10, Real>();
    TestAssign<5, 10, Real>();
    test_time = Chrono::elapsed();
    stk_cout << _T("\n");
    stk_cout << _T("Time used : ") << test_time << _T("\n");
    stk_cout << _T("\n\n");

    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test program with int : \n");
    stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    Chrono::start();
    Test<5, 10, int>();
    TestAssign<5, 10, int>();
    TestInt<5, 10>();
    test_time = Chrono::elapsed();
    stk_cout << _T("\n");
    stk_cout << _T("Time used : ") << test_time << _T("\n");
    stk_cout << _T("\n\n");

  }
  catch (Exception const& error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n";);
    return -1;
  }
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for Binary operators+\n");
  stk_cout << _T("+ No errors detected.                                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}
