/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testSlicingOperators.cpp
 *  @brief In this file we test the row and col operators.
 **/


#include <STKernel.h>
#include "Arrays.h"
#include "STatistiK.h"

using namespace STK;

template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline void operator() ( Type& value)
  { value = value_; ++value_;}
};

template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

/* main print. */
template<class CArray >
void printHead(CArray const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")    << A.isRef()  << _T("\n");
  stk_cout << name << _T(".rows() =")   << A.rows() << _T("\n");
  stk_cout << name << _T(".cols() =")   << A.cols() << _T("\n");
  stk_cout << name << _T(".sizeRows() =")   << A.sizeRows() << _T("\n");
  stk_cout << name << _T(".sizeCols() =")   << A.sizeCols() << _T("\n");
  stk_cout << name << _T("=\n");
}

/* main print.*/
template<class CArray >
void print(CArray const& A, String const& name)
{
  printHead(A, name);
  stk_cout << A;
}


template<int N>
void testSym()
{
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test reshape operators with fixed size.             +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  CArray<double, N, N> a(4,4), b(4,4);
  a << 0, 1, 2, 3
     , 1, 5, 6, 7
     , 2, 6, 4, 4
     , 3, 7, 4, 7;
  b << 0, -1, -2, -3
     , 1,  5, -6, -7
     , 2,  6,  4, -4
     , 3,  7,  4,  7;
  CArraySquare<double, N> s(4), r(4);
  s << 0, 1, 2, 3
     , 1, 5, 6, 7
     , 2, 6, 4, 4
     , 3, 7, 4, 7;
  r << 0,   1,  2, 3
     , -1,  5,  6, 7
     , -2, -6,  4, 4
     , -3, -7, -4, 7;
  CArrayNumber<double> n =7;
//  n << 7;
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ lowerSymmetrize.                                    +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.lowerSymmetrize()=\n") << a.lowerSymmetrize();
  stk_cout << _T("b.lowerSymmetrize()=\n") << b.lowerSymmetrize();
  stk_cout << _T("s.lowerSymmetrize()=\n") << s.lowerSymmetrize();
  stk_cout << _T("n.lowerSymmetrize()=\n") << n.lowerSymmetrize();
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ UpperSymmetrize.                                    +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.UpperSymmetrize()=\n") << a.upperSymmetrize();
  stk_cout << _T("b.UpperSymmetrize()=\n") << b.upperSymmetrize();
  stk_cout << _T("s.UpperSymmetrize()=\n") << s.upperSymmetrize();
  stk_cout << _T("n.UpperSymmetrize()=\n") << n.upperSymmetrize();
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Symmetrize.                                         +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.Symmetrize()=\n") << a.symmetrize();
  stk_cout << _T("s.Symmetrize()=\n") << s.symmetrize();
  stk_cout << _T("n.Symmetrize()=\n") << n.symmetrize();
}

template< class Tab1, class Tab2, class Tab3, class Tab4, class Tab5, class Tab6>
int test(Tab1 const& a, Tab2 const& v, Tab3 const& s, Tab4 const& p, Tab5 const& d, Tab6 const& n)
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ transpose.                                          +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.transpose()=\n") << a.transpose();
  stk_cout << _T("v.transpose()= ")  << v.transpose();
  stk_cout << _T("p.transpose()=\n") << p.transpose();
  stk_cout << _T("(a*v).transpose()= ") << (a*v).transpose();
  stk_cout << _T("(p*a).transpose()=\n") << (p*a).transpose();

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ getDiagonal.                                        +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("s.getDiagonal()=\n") << s.getDiagonal();
  stk_cout << _T("n.getDiagonal()=\n") << n.getDiagonal();
  stk_cout << _T("d.getDiagonal()=\n") << d.getDiagonal();
  stk_cout << _T("(p*a*v).getDiagonal()=\n") << (p*a*v).getDiagonal();
  stk_cout << _T("(a*a.transpose()).getDiagonal()=\n") << (a*a.transpose()).getDiagonal();

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ diagonalize.                                        +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("v.diagonalize()=\n") << v.diagonalize();
  stk_cout << _T("p.diagonalize()=\n") << p.diagonalize();
  stk_cout << _T("d.diagonalize()=\n") << d.diagonalize();
  stk_cout << _T("n.diagonalize()=\n") << n.diagonalize();

  stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ upperTriangularize.                                  +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.upperTriangularize()=\n") << a.upperTriangularize();
  stk_cout << _T("s.upperTriangularize()=\n") << s.upperTriangularize();
  stk_cout << _T("v.upperTriangularize()=\n") << v.upperTriangularize();
  stk_cout << _T("p.upperTriangularize()=\n") << p.upperTriangularize();
  stk_cout << _T("n.upperTriangularize()=\n") << n.upperTriangularize();

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ lowerTriangularize.                                 +\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("a.lowerTriangularize()=\n") << a.lowerTriangularize();
  stk_cout << _T("s.lowerTriangularize()=\n") << s.lowerTriangularize();
  stk_cout << _T("v.lowerTriangularize()=\n") << v.lowerTriangularize();
  stk_cout << _T("p.lowerTriangularize()=\n") << p.lowerTriangularize();
  stk_cout << _T("n.lowerTriangularize()=\n") << n.lowerTriangularize();
  return true;
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test reshape operators with fixed size.             +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    CArray<double, 2, 4> a(2,4);
    a << 0, 1, 2, 3
       , 4, 5, 6, 7;
    CArraySquare<double, 4> s(4);
    s << 0, 1, 2, 3
       , 4, 5, 6, 7
       , 0, 1, 2, 3
       , 4, 5, 6, 7;
    CArrayVector<double, 4> v(4);
    v <<  1, 1, 1, 1;
    CArrayPoint<double, 2> p(2);
    p <<  1, 1;
    Array2DDiagonal<double> d(4);
    d <<  1, 2, 3, 4;
    CArrayNumber<double> n;
    n = 7;
    stk_cout << _T("a=\n") << a;
    stk_cout << _T("v=\n") << v;
    stk_cout << _T("s=\n") << s;
    stk_cout << _T("p= ") << p;
    stk_cout << _T("n= ") << n;
    test(a,v,s,p,d,n);
    testSym<4>();
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test reshape operators with unknown size.           +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    CArray<double> a1(2,4);
    a1 << 0, 1, 2, 3
       , 4, 5, 6, 7;
    CArraySquare<double> s1(4);
    s1 << 0, 1, 2, 3
       , 4, 5, 6, 7
       , 0, 1, 2, 3
       , 4, 5, 6, 7;
    CArrayVector<double> v1(4);
    v1 <<  1, 1, 1, 1;
    CArrayPoint<double> p1(2);
    p1 <<  1, 1;
    test(a1,v1,s1,p1,d,n);


    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for reshape operators+\n");
    stk_cout << _T("+ No errors detected.                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n";);
    return -1;
  }

  return 0;
}
