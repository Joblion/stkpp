/*--------------------------------------------------------------------*/
/*   Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the
  Free Software Foundation, Inc.,
  59 Temple Place,
  Suite 330,
  Boston, MA 02111-1307
  USA

  Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testArray1D.cpp
 *  @brief In this file we test the TContainer 1D classes.
 **/

//#include "Arrays.h"
#include "STKpp.h"

using namespace STK;

template< class Derived >
void print(IArray2D<Derived> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")                 << A.isRef()  << _T("\n");
  stk_cout << name << _T(".allocator().isRef() =")     << A.allocator().isRef() << _T("\n");
  stk_cout << name << _T(".rangeCols().isRef() =")     << A.rangeCols().isRef() << _T("\n");
  stk_cout << name << _T(".cols() =")                  << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")                  << A.rows()  << _T("\n");
  stk_cout << name << _T(".allocator().range() =")     << A.allocator().range()  << _T("\n");
  stk_cout << name << _T(".rangeCols().range() =")     << A.rangeCols().range() << _T("\n");
  stk_cout << name << _T(".availableCols() =")         << A.availableCols()  << _T("\n");
  stk_cout << name << _T(".rangeCols() = ")            << A.rangeCols() << _T("\n");
  stk_cout << name << _T("=\n")                        << A << _T("\n\n");
}

template< class TYPE>
void print(Array2DVector<TYPE> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")                 << A.isRef()  << _T("\n");
  stk_cout << name << _T(".allocator().isRef() =")     << A.allocator().isRef() << _T("\n");
  stk_cout << name << _T(".rangeCols().isRef() =")     << A.rangeCols().isRef() << _T("\n");
  stk_cout << name << _T(".cols() =")                  << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")                  << A.rows()  << _T("\n");
  stk_cout << name << _T(".allocator().range() =")     << A.allocator().range()  << _T("\n");
  stk_cout << name << _T(".rangeCols().range() =")     << A.rangeCols().range() << _T("\n");
  stk_cout << name << _T(".availableCols() =")         << A.availableCols()  << _T("\n");
  stk_cout << name << _T(".rangeCols() = ")            << A.rangeCols() << _T("\n");
  stk_cout << name << _T("=\n")                        << A.transpose() << _T("\n\n");
}

template< class TYPE, int Size_ >
void print(Array1D<TYPE, Size_> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")     << A.isRef()  << _T("\n");
  stk_cout << name << _T(".range() =")     << A.range()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T(".capacity() = ") << A.capacity() << _T("\n");
  stk_cout << name << _T(".p_data() = ")   << A.allocator().p_data() << _T("\n");
  stk_cout << name << _T("=\n");
  for (int i=A.begin(); i<A.end(); ++i)
  { stk_cout << A[i] << _T(" ");}
  stk_cout << _T("\n");
//  stk_cout << name << _T("=\n")            << A << _T("\n\n");
}

template< class Container1D >
void write_Array( Container1D const& C, String const& name, String const& message)
{
  stk_cout << _T("==>") << message;
  print(C, name);
}

/* template main test function for 1D arrays. */
template< class Container1D >
void testArray( int M, int N, Range J, bool output)
{
  enum{ size_ = hidden::Traits<Container1D>::size_};
  typedef typename hidden::Traits<Container1D>::SubVector SubVector;
  typedef typename hidden::Traits<Container1D>::Type Type;

  Container1D B(N,4);
  if (output)
  { write_Array(B, _T("B"), _T("Test constructor:  B(N,4)\n"));}

  Container1D C; C=0;
  if (output)
  { write_Array(C, _T("C"), _T("Test null constructor: C()\n"));}

  Container1D D(TRange<size_>(0,M, 0)); D= 3;
  if (output)
  { write_Array(D, _T("D"), _T("Test constructor: D(Range(0,M)); D = 3\n"));}

  Container1D E(TRange<size_>(1,M, 0)); E= 3;
  if (output)
  { write_Array(E, _T("E"), _T("Test constructor: E(Range(1,M)); E = 3\n"));}

  B.exchange(D);
  if (output)
  { write_Array(B, _T("B"), _T("Test B.exchange(D);\n"));
    write_Array(D, _T("D"), _T(""));
  }

  Container1D RefD(D, false);
  if (output)
  { write_Array(RefD, _T("RefD"), _T("Test copy constructor RefD(D, false)\n"));}

  Container1D DRefD(D, true);
  if (output)
  { write_Array(DRefD, _T("DRefD"), _T("Test copy constructor DRefD(D, true)\n"));}

  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(D.sub(J), _T("D.sub(J)"), _T("Test D.sub(J)\n"));
  }

  SubVector Dref(D.sub(J), true);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(Dref, _T("Dref"), _T("Test Dref(D.sub(J), true)\n"));
  }

  D.sub(J)= Type(5);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(D, _T("D"), _T("Test D.sub(J) = 5\n"));
  }

  Dref.sub(J)= Type(6);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(Dref, _T("Dref"), _T("Test Dref.sub(J) = 6\n"));
  }

  DRefD.sub(J)= Type(7);
  if (output)
  {
    stk_cout << _T("J=") << J << _T("\n");
    write_Array(DRefD, _T("DrefD"), _T("Test DrefD.sub(J) = 7\n"));
  }

  D.shift(1);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.shift(1)\n"));}

  D.pushBack(2); D.sub(Range(M, D.lastIdx(), 0)) = (1);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.pushBack(2); D.sub(Range(M,D.lastIdx()), 0) = 1\n"));}

  D.swap(D.begin(),D.lastIdx());
  if (output)
  { write_Array(D, _T("D"), _T("Test D.swap(D.begin(),D.lastIdx())\n"));}

  D.erase(D.lastIdx());
  if (output)
  { write_Array(D, _T("D"), _T("Test D.erase(D.lastIdx())\n"));}

  D.erase(2, 2);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.erase(2,2)\n"));}

  D.pushBack(M); D = (5);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.pushBack(M); D=5\n"));}

  D.push_back(6);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.push_back(6);\n"));}

  D.push_front(4);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.push_front(4);\n"));}

  D.resize(J);
  if (output)
  { write_Array(D, _T("D"), _T("Test D.resize(J)\n"));}

  C.resize(D.range());
  C= (0);
  if (output)
  { write_Array(C, _T("C"), _T("Test C.resize(D.range());C= TYPE(0);\n"));}

  C = D;
  if (output)
  { write_Array(C, _T("C"), _T("Test C = D\n"));
    write_Array(D, _T("D"), _T("\n"));
  }

  C.front() = (-2); C.back()  = (-2);
  if (output)
  { write_Array(C, _T("C"), _T("Test C.front() = -2 and C.back() = -2\n"));}

  C.elt(J.begin()) = C.front();
  C.elt(J.lastIdx())  = C.back();
  if (output)
  { write_Array(C, _T("C"), _T("Test C.at(J.begin()) = C.front() and C.at(J.lastIdx()) = C.back()\n"));}

  C.elt(C.lastIdx()-1) = -C.elt(C.lastIdx()-1);
  if (output)
  { write_Array(C, _T("C"), _T("Test C.elt(C.lastIdx()-1) = -C.elt(C.lastIdx()-1)\n"));}
  //
  C.erase(J.begin(), J.size());
  if (output)
  { stk_cout << _T("J= ") << J << _T("\n");
    write_Array(C, _T("C"), _T("Test C.erase(J.begin(), J.size())\n"));
  }
}

/* main. */
int main(int argc, char *argv[])
{
  int M, N;
  int Jbeg, Jend;
  bool output = true;

  if (argc < 5)
  {
    M=12;
    N=15;
    Jbeg = 3;
    Jend = 9;
  }
  else
  {
    M = atoi(argv[1]);
    N = atoi(argv[2]);
    Jbeg = atoi(argv[3]);
    Jend = atoi(argv[4]);
    if (argc == 6) output = atoi(argv[5]);
  }

  try
  {
    Range J(Jbeg, Jend, 0);
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test 1D Arrays                                      +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Using M  = ") << M    << _T(" N    = ") << N  << _T("\n")
             << _T("J = ") << J << _T("\n");
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Array1D<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Array1D<Real> >(M, N, J, output);
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Array1D<Real, 15> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Array1D<Real, 15> >(M, N, J, output);
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Array2DVector<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Array2DVector<Real> >(M, N, J, output);
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Array2DPoint<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Array2DPoint<Real> >(M, N, J, output);
    stk_cout << _T("\n\n");

    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Test Array2DDiagonal<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    testArray<Array2DDiagonal<Real> >(M, N, J, output);
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for STK::Array1D   +\n");
  stk_cout << _T("+ No errors detected.                                 +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}
