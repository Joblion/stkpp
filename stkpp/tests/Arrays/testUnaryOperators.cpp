/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testUnaryOperators.cpp
 *  @brief In this file we test UnaryOperator class
 **/


#include <STKernel.h>
#include "Arrays.h"
#include "STatistiK.h"

using namespace STK;

/** @ingroup Arrays
  * @brief Base class to implement min and max visitors.
  * Allow to find the
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline Type const& operator() ()
  { return ++value_;}
};

/** number the element of a container in the order they are visited.
  * @sa  maxElt(Derived const&, int&, int&), minElt()
  */
template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}

/* main print. */
template<class CArray >
void printHead(CArray const& A, String const& name)
{
  stk_cout << _T("\n";);
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")    << A.isRef()  << _T("\n");
  stk_cout << name << _T(".rows() =")   << A.rows() << _T("\n");
  stk_cout << name << _T(".cols() =")   << A.cols() << _T("\n");
  stk_cout << name << _T(".sizeRows() =")   << A.sizeRows() << _T("\n");
  stk_cout << name << _T(".sizeCols() =")   << A.sizeCols() << _T("\n");
  stk_cout << name << _T("=\n");
}

/* main print.*/
template<class CArray >
void print(CArray const& A, String const& name)
{
  printHead(A, name);
  stk_cout << A;
}

template< class Type>
void testProductByScalar( int M, int N)
{
  CArray<Type, UnknownSize, UnknownSize, Arrays::by_col_> C0;
  CArray<Type, 3, 4, Arrays::by_row_> C1(M, N, 1);
  numbering(C1);
  print(C1, "C1<Type, 3, 4, Arrays::by_row_>");
  C0 = C1 * Type(2); // C0 is now (3,4)
  print(C0,"C0 = C1 * Type(2)");

  CArray<bool, 3, 4, Arrays::by_col_> C3(M, N, true);
  print(C3, "C3<bool, 3, 4, Arrays::by_col_>");
  C0 -= Type(2)*C3;
  print(C0, "C0 -= Type(2)*C3");
  C0 -= C3.template cast<Type>() * Type(2);
  print(C0, "C0 -= C3.template cast<Type>() * Type(2)");
}

template< class Type>
void testAddScalar( int M, int N)
{
  CArray<Type, UnknownSize, UnknownSize, Arrays::by_col_> C0;
  CArray<Type, 3, 4, Arrays::by_row_> C1(M, N, 1);
  numbering(C1);
  print(C1, "C1<Type, 3, 4, Arrays::by_row_>");
  C0 = - C1;
  print(C0,"C0 = - C1");
  C0 = - C1 + 3;
  print(C0,"C0 = - C1 + 3");
  C0 = 1 - C1;
  print(C0,"C0 = 1 - C1");
  C0 = 1 - C1 + 3;
  stk_cout << _T("\n");
  print(C0,"C0 = 1 - C1 + 3");

  CArray<Type, UnknownSize, UnknownSize, Arrays::by_col_> C3(3, 4, 1);
  print(C3, "C3<bool, UnknownSize, UnknownSize, Arrays::by_col_>");
  C0 -= 1 - C3 + 3;
  print(C0, "C0 -= 1 - C3 + 3");
}


template< class Type>
void testDiagonalGetterOperator( int M, int N)
{
  CArraySquare<Type, 5> C0;
  Array2DVector<Type> V0(5);
  numbering(C0);
  numbering(V0);
  print(C0, "C0<Type, 5>");
  print(V0, "V0<Type>");

  Array2DDiagonal<Type> C1;

  C1 = C0.getDiagonal() + V0.diagonalize();
  print(C1, "C1 = C0.getDiagonal() + V0.diagonalize()");
}

template< class Type>
void testLawOperator()
{
  Law::Normal l(0,1);
  CArraySquare<Type, 5> C0;
  numbering(C0);
  print(C0.pdf(l), "C0.pdf(l)");
  print(C0.lpdf(l), "C0.lpdf(l)");
  print(C0.cdf(l), "C0.cdf(l)");
  C0.randGauss();
  print(C0, "C0.randGauss()");
  C0.randUnif();
  print(C0, "C0.randUnif()");
  print(C0.icdf(l), "C0.icdf(l)");
}

/*--------------------------------------------------------------------*/
/* main. */
int main(int argc, char *argv[])
{
  try
  {
    int M, N;
    if (argc < 7)
    {
      M = 3; N = 4;
    }
    else
    {
      // dimensions
      M      = std::atoi(argv[1]);
      N      = std::atoi(argv[2]);
    }
    Real test_time;
      stk_cout << _T("\n\n");
      stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("+ Test UnaryOperators. Using:                         +\n");
      stk_cout << _T("M = ") << M << _T(", N = ") << N << _T("\n");
      stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("Test product by scalar expressions : \n");
      // time variables
      Chrono::start();
      testProductByScalar<Real>(M, N);
      test_time = Chrono::elapsed();
      stk_cout << _T("test product by scalar terminated\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");

      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("Test product by scalar expressions : \n");
      // time variables
      Chrono::start();
      testAddScalar<Real>(M, N);
      test_time = Chrono::elapsed();
      stk_cout << _T("test product by scalar terminated\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");

      stk_cout << _T("\n\n");
      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("Test diagonal operators: \n");
      // time variables
      Chrono::start();
      testDiagonalGetterOperator<Real>(M, N);
      test_time = Chrono::elapsed();
      stk_cout << _T("test diagonal operators terminated.\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");
      stk_cout << _T("\n\n");

      stk_cout << _T("\n\n");
      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
      stk_cout << _T("Test diagonal operators: \n");
      // time variables
      Chrono::start();
      testLawOperator<Real>();
      test_time = Chrono::elapsed();
      stk_cout << _T("test diagonal operators terminated.\n");
      stk_cout << _T("Time used : ") << test_time << _T("\n");
      stk_cout << _T("\n\n");
//
//      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
//      stk_cout << _T("Test<int> : \n");
//      stk_cout << _T("++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
//      // time variables
//      Chrono::start();
//      Test<int>(M, N);
//      test_time = Chrono::elapsed();
//      stk_cout << _T("\n");
//      stk_cout << _T("Time used : ") << test_time << _T("\n");
//      stk_cout << _T("\n\n");
//
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for UnaryOperators.+\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    stk_cerr << _T("An error occured : ") << error.error() << _T("\n";);
    return -1;
  }

  return 0;
}
