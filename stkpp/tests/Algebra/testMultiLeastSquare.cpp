/*--------------------------------------------------------------------*/
/*     Copyright (C) 2003-2015  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Algebra
 * Purpose:  test program for testing MultiLeastSquare class.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testLapackMultiLeastSquare.cpp
 *  @brief In this file we test the lapack::MultiLeastSquare class.
 **/

#include "STKpp.h"
using namespace STK;

/** @ingroup Arrays
  * @brief allow to number to the element of an Arrays
  */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {}
  inline Type const& operator() ()
  { value_++; return value_;}
};

/** Utility function for numbering an array */
template<typename Derived>
void numbering(Derived& matrix)
{
  typedef typename hidden::Traits<Derived>::Type Type;
  numberingVisitor<Type> visitor;
  matrix.apply(visitor);
}


/* main print method. */
template< class Container2D >
void print(Container2D const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")     << A.isRef()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T("=\n")   << A << _T("\n\n");
}


template<class Array, template<class,class> class MultiLeastSquare>
void test_ml(int M, int N, int Nrhs)
{
  Array A(M, N), B,  X(N, Nrhs);
  A.randGauss(); numbering(X);
  B = A*X + Array(M,Nrhs).randGauss();
  Array Ab = A, Bb = B;
  // run
  MultiLeastSquare<Array, Array> ml(B,A);
  if (!ml.run())
  { stk_cout << _T("An error occur: ") << ml.error() << _T("\n"); return;}
  Real resmax = (Bb-Ab*X).abs().maxElt();
  stk_cout << _T("Maximal true residual =") << resmax << _T("\n");
  resmax = (Bb-Ab*ml.x()).abs().maxElt();
  stk_cout << _T("Maximal estimated residual =") << resmax << _T("\n");
  Real diff = (X-ml.x()).abs().maxElt();
  stk_cout << _T("Maximal X difference =") << diff << _T("\n");
}

template<class Array, template<class,class> class MultiLeastSquare>
void test_wml(int M, int N, int Nrhs)
{
  Array A(M, N), B,  X(N, Nrhs);
  VectorXi W(M);
  Law::Poisson wlaw(3);
  W.rand(wlaw); W+=1;
  A.randGauss(); numbering(X);
  B = A*X + Array(M,Nrhs).randGauss();
  Array Ab = A, Bb = B;
  // run
  MultiLeastSquare<Array, Array> ml(B,A);
  if (!ml.run(W.cast<Real>()))
  { stk_cout << _T("An error occur: ") << ml.error() << _T("\n"); return;}
  Real resmax = (Bb-Ab*X).abs().maxElt();
  stk_cout << _T("Weighted Maximal true residual =") << resmax << _T("\n");
  resmax = (Bb-Ab*ml.x()).abs().maxElt();
  stk_cout << _T("Weighted Maximal estimated residual =") << resmax << _T("\n");
  Real diff = (X-ml.x()).abs().maxElt();
  stk_cout << _T("Weighted Maximal X difference =") << diff << _T("\n");
}


template<template<class,class> class ML>
void main_test()
{
    stk_cout << _T("=======================================\n");
    stk_cout << _T("Test MultiLinear class with ArrayXX    \n");
    stk_cout << _T("=======================================\n");
    int M =100, N=2, Nrhs = 2;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<ArrayXX, ML>(M, N, Nrhs);
    test_wml<ArrayXX, ML>(M, N, Nrhs);
    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    Nrhs = 4;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<ArrayXX, ML>(M, N, Nrhs);
    test_wml<ArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    N = 7; Nrhs = 2;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<ArrayXX, ML>(M, N, Nrhs);
    test_wml<ArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    M = 5; N=7; Nrhs = 4;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<ArrayXX, ML>(M, N, Nrhs);
    test_wml<ArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    stk_cout << _T("Test MultiLinear class with CArrayXX   \n");
    stk_cout << _T("=======================================\n");
    N=2; Nrhs = 2;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<CArrayXX, ML>(M, N, Nrhs);
    test_wml<CArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    N=2; Nrhs = 4;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<CArrayXX, ML>(M, N, Nrhs);
    test_wml<CArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    N = 7; Nrhs = 4;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<CArrayXX, ML>(M, N, Nrhs);
    test_wml<CArrayXX, ML>(M, N, Nrhs);

    stk_cout << _T("\n\n");
    stk_cout << _T("=======================================\n");
    M = 5; N=7; Nrhs = 4;
    stk_cout << _T("M = ") << M << _T(", N = ") << N << _T(", Nrhs = ") << Nrhs << _T("\n\n");
    test_ml<CArrayXX, ML>(M, N, Nrhs);
    test_wml<CArrayXX, ML>(M, N, Nrhs);
}
// Main
int main(int argc, char *argv[])
{
 //  if (argc >= 2) {}

  try
  {
#ifdef STKUSELAPACK
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++""\n");
    stk_cout << _T("+ Test STK::lapack::MultiLeastSquare                  +""\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++""\n");
    main_test<lapack::MultiLeastSquare>();
#else
    stk_cout << _T("Warning STK++ has been compiled without lapack\n");
#endif
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++""\n");
    stk_cout << _T("+ Test STK::MultiLeastSquare                          +""\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++""\n");
    main_test<MultiLeastSquare>();

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for MultiLeastSquare.+\n")
             << _T("+ No errors detected.                                   +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;

}
