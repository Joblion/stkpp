/*--------------------------------------------------------------------*/
/*     Copyright (C) 2013-2016  Serge Iovleff, Quentin Grimonprez

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._DOT_I..._AT_stkpp.org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 juil. 2013
 * Author:   Serge Iovleff
 **/

/** @file testInvertTriangularMatrix.cpp
 *  @brief In this file we test the Inverting matrix algorithms with triangular matrices
 **/

#include "Algebra.h"
#include "Arrays.h"

using namespace STK;

/** test inverting functor for general lower triangular matrices */
template<int Size> void testLowerInvert(int size)
{
  Array2DLowerTriangular<Real> A(TRange<Size>(0, size),TRange<Size>(0, size));
  A.randGauss();
  Array2DLowerTriangular<Real> B = InvertMatrix<Array2DLowerTriangular<Real>, Size>(A)();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("A*A^{-1}=\n") << A*B << _T("\n");
  stk_cout << _T("Test base (1,1)\n");
  A.shift(1,1);
  B = InvertMatrix<Array2DLowerTriangular<Real>, Size>(A)();
  stk_cout << _T("A*A^{-1}=\n") << A*B << _T("\n");
}


/** test inverting functor for general lower triangular matrices */
template<int Size> void testUpperInvert(int size)
{
  Array2DUpperTriangular<Real> A(TRange<Size>(0, size),TRange<Size>(0, size));
  A.randGauss();
  Array2DUpperTriangular<Real> B = InvertMatrix<Array2DUpperTriangular<Real>, Size>(A)();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("A*A^{-1}=\n") << A*B << _T("\n");

  stk_cout << _T("Test base (1,1)\n");
  A.shift(1,1);
  B = InvertMatrix<Array2DUpperTriangular<Real>, Size>(A)();
  stk_cout << _T("A*A^{-1}=\n") << A*B << _T("\n");
}

// main
int main()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertLowerTriangular                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  try{
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testLowerInvert<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testLowerInvert<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testLowerInvert<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testLowerInvert<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testLowerInvert<5>(5);
  stk_cout << _T("+++++++----------\n");
  stk_cout << _T("Size= UnknownSize\n");
  testLowerInvert<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for InvertLowerMatrix+\n");
  stk_cout << _T("+ No errors detected.                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertUpperTriangular                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testUpperInvert<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testUpperInvert<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testUpperInvert<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testUpperInvert<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testUpperInvert<5>(5);
  stk_cout << _T("+++++++----------\n");
  stk_cout << _T("Size= UnknownSize\n");
  testUpperInvert<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for InvertUpperMatrix+\n");
  stk_cout << _T("+ No errors detected.                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  }
  catch (Exception const& e)
  {
    stk_cout << _T("+ An error occurred.                                    +\n");
    stk_cout << _T("What: ") << e.error();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    return -1;
  }
  return 0;
}
