/*--------------------------------------------------------------------*/
/*     Copyright (C) 2013-2016  Serge Iovleff, Quentin Grimonprez

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._DOT_I..._AT_stkpp.org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 juil. 2013
 * Author:   Serge Iovleff
 **/

/** @file testInvertMatrix.cpp
 *  @brief In this file we test the Inverting matrix algorithms.
 **/

#include "Algebra.h"
#include "Arrays.h"

using namespace STK;

/** test inverting functor for symmetric fixed size matrices */
template<int Size>
void testInvertSym(int size)
{
  CArrayXX A(TRange<Size>(0,size), TRange<Size>(0,size));
  A.randGauss();
  CSquareX C = A+A.transpose();
  CSquareX B = InvertMatrix<CSquareX, Size>(C)();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("(A+A')*(A+A')^{-1}=\n") << C*B << _T("\n");
  stk_cout << _T("Test base (1,1)\n");
  C.shift(1);
  B = InvertMatrix<CSquareX, Size>(C)();
  stk_cout << _T("(A+A')*(A+A')^{-1}=\n") << C*B << _T("\n");
}

/** test inverting functor for upper symmetric fixed size matrices */
template<int Size>
void testInvertUpperSym(int size)
{
  CArrayXX A(TRange<Size>(0,size), TRange<Size>(0,size));
  A.randGauss();
  CSquareX B = InvertMatrix< UpperSymmetrizeAccessor<CArrayXX>, Size>(A.upperSymmetrize())();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("(A)*(A)^{-1}=\n") << A.upperSymmetrize()*B << _T("\n");
  stk_cout << _T("Test base (1,1)\n");
  A.shift(1,1);
  B = InvertMatrix< UpperSymmetrizeAccessor<CArrayXX>, Size>(A.upperSymmetrize())();
  stk_cout << _T("A*A^{-1}=\n") << A.upperSymmetrize()*B << _T("\n");
}

/** test inverting functor for lower symmetric fixed size matrices */
template<int Size>
void testInvertLowerSym(int size)
{
  CArrayXX A(TRange<Size>(0,size), TRange<Size>(0,size));
  A.randGauss();
  CSquareX B = InvertMatrix< LowerSymmetrizeAccessor<CArrayXX>, Size>(A.lowerSymmetrize())();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("(A)*(A)^{-1}=\n") << A.lowerSymmetrize()*B << _T("\n");
  stk_cout << _T("Test base (1,1)\n");
  A.shift(1,1);
  B = InvertMatrix< LowerSymmetrizeAccessor<CArrayXX>, Size>(A.lowerSymmetrize())();
  stk_cout << _T("A*A^{-1}=\n") << A.lowerSymmetrize()*B << _T("\n");
}


int main()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertSymMatrix                          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  try
  {
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testInvertSym<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testInvertSym<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testInvertSym<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testInvertSym<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testInvertSym<5>(5);
  stk_cout << _T("++++++++++\n");
  stk_cout << _T("UnknownSize\n");
  testInvertSym<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                  +\n");
  stk_cout << _T("+ STK::InvertSymMatrix. No errors detected.             +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertUpperSymMatrix                    +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testInvertUpperSym<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testInvertUpperSym<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testInvertUpperSym<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testInvertUpperSym<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testInvertUpperSym<5>(5);
  stk_cout << _T("++++++++++\n");
  stk_cout << _T("UnknownSize\n");
  testInvertUpperSym<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                  +\n");
  stk_cout << _T("+ STK::testInvertUpperSym. No errors detected.          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertLowerSymMatrix                    +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testInvertLowerSym<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testInvertLowerSym<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testInvertLowerSym<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testInvertLowerSym<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testInvertLowerSym<5>(5);
  stk_cout << _T("++++++++++\n");
  stk_cout << _T("UnknownSize\n");
  testInvertLowerSym<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                  +\n");
  stk_cout << _T("+ STK::testInvertUpperSym. No errors detected.          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  }
  catch (Exception const& e)
  {
    stk_cout << _T("+ An error occurred.                                    +\n");
    stk_cout << _T("What: ") << e.error();
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    return -1;
  }

  return 0;
}
