/*--------------------------------------------------------------------*/
/*     Copyright (C) 2013-2016  Serge Iovleff, Quentin Grimonprez

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._DOT_I..._AT_stkpp.org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 juil. 2013
 * Author:   Serge Iovleff
 **/

/** @file testInvertMatrix.cpp
 *  @brief In this file we test the Inverting matrix algorithms.
 **/

#include "Algebra.h"
#include "Arrays.h"

using namespace STK;

/** test inverting functor for general square matrices */
template<int Size> void testInvert(int size)
{
  CArrayXX A(TRange<Size>(0, size),TRange<Size>(0, size));
  A.randGauss();
  CArrayXX B = InvertMatrix<CArrayXX, Size>(A)();
  stk_cout << _T("Test base (0,0)\n");
  stk_cout << _T("(A)*(A)^{-1}=\n") << A*B << _T("\n");
  stk_cout << _T("Test base (1,1)\n");
  A.shift(1,1);
  B = InvertMatrix<CArrayXX, Size>(A)();
  stk_cout << _T("A*A^{-1}=\n") << A*B << _T("\n");
}

// main
int main()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::InvertMatrix                            +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 1\n");
  testInvert<1>(1);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 2\n");
  testInvert<2>(2);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 3\n");
  testInvert<3>(3);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 4\n");
  testInvert<4>(4);
  stk_cout << _T("+++++++\n");
  stk_cout << _T("Size= 5\n");
  testInvert<5>(5);
  stk_cout << _T("+++++++----------\n");
  stk_cout << _T("Size= UnknownSize\n");
  testInvert<UnknownSize>(5);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for STK::InvertMatrix+\n");
  stk_cout << _T("+ No errors detected.                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}
