/*--------------------------------------------------------------------*/
/*     Copyright (C) 2013-2013  Serge Iovleff, Quentin Grimonprez

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._DOT_I..._AT_stkpp.org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 3 juil. 2013
 * Author:   Serge Iovleff
 **/

/** @file testCG.cpp
 *  @brief In this file we test the Conjugate Gradient algorithm.
 **/

#include "Algebra.h"
#include "Arrays.h"
using namespace STK;

struct TestToyMultiplicator
{
    CVectorX operator()(CVectorX const& x) const
    {
      CVectorX a = A_ * x;
      return   a ;
    }
    // with fixed size
    TestToyMultiplicator():A_(2,2)
    {
      A_ << 4, 1
          , 1, 3;
    }
    // fixed matrix
    TestToyMultiplicator(CArrayXX const& A) : A_(A) {}
    CArrayXX A_;
};
struct TestToyInit
{
    CVectorX operator()() const
    {
      CVectorX a(2);
      a << 2, 1;
      return   a ;
    }
};


/** @ingroup Arrays */
struct TestMultiplicator
{
    CVectorX operator()(CVectorX const& x) const
    {
      CVectorX a = A_.transpose() * (A_ * x);
      return   a ;
    }
    // with fixed size
    TestMultiplicator(int n = 3)
    {
      A_.resize(n, 2*n);
      A_.randGauss();
    }
    // fixed matrix
    TestMultiplicator(CArrayXX const& A) : A_(A) {}
    CArrayXX A_;
};

/** @ingroup Arrays */
struct IdMultiplicator
{
  CVectorX operator()(CVectorX const& x) const
  { return   x ;}
};

/** @ingroup Arrays */
struct DiagonalMultiplicator
{
  CVectorX operator()(CVectorX const& x) const
  {
    CVectorX a;
    a = M_ * x;
    return   a ;
  }
  DiagonalMultiplicator(CArrayXX const& A) : M_(A.rows())
  { for (int i = M_.begin(); i< M_.end() ;i++) M_[i]=1/A(i,i);}
  Array2DDiagonal<Real> M_;
};


int main()
{
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test GC                                          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ test0 :\n");
  TestToyMultiplicator functor0;
  TestToyInit init;
  stk_cout<<"A ="<< functor0.A_ << _T("\n");
  CVectorX b0(2);
  b0 << 1, 2;
  stk_cout<<"b ="<< b0 << _T("\n");
  CG<TestToyMultiplicator,CVectorX,TestToyInit> testcg0(functor0,b0,&init);
  if (testcg0.run()<0)
  { stk_cout << "An errror occur:" << testcg0.error();}
  stk_cout<<"CG Residuals = " << testcg0.r().norm2() <<_T("\n");

  stk_cout << _T("\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ test1 :\n");
  TestMultiplicator functor1(10);
  CVectorX x1(20, 1.);
  CVectorX b1 = functor1.A_.transpose() * (functor1.A_ * x1);
  CG<TestMultiplicator,CVectorX> testcg1(functor1,b1);
//  testcg1.setMaxIter(10);
  if (testcg1.run() < 0)
  { stk_cout << "An errror occur:" << testcg1.error();}
  stk_cout<<_T("CG iter = ") << testcg1.iter() <<_T("\n");
  stk_cout<<_T("CG Residuals = ") << testcg1.r().norm2() <<_T("\n");

  CVectorX bEstim = functor1(testcg1.x());
  Real res = (bEstim-b1).norm2();
  stk_cout<<"Computed Residuals = " << res <<_T("\n");

  stk_cout << _T("\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ test2 :\n");
  TestMultiplicator functor2(1000);
  CVectorX x2(2000,1.);
  CVectorX b2 = functor2.A_.transpose() * (functor2.A_ * x2);

  CG<TestMultiplicator,CVectorX> testcg2(functor2,b2);
  testcg2.setEps(1e-08);
  if (testcg2.run()<0)
  { stk_cout << "An errror occur:" << testcg2.error();}
  stk_cout<<_T("CG iter = ") << testcg2.iter() <<_T("\n");
  stk_cout<<_T("CG Residuals = ") << testcg2.r().norm2() <<_T("\n");

  bEstim = functor2(testcg2.x());
  res = (bEstim-b2).norm2();
  stk_cout<<"Computed Residuals = " << res <<_T("\n");

  stk_cout << _T("\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ test3\n");
  int n(5), p(3);
  CArrayXX data(n,p);
  CVectorX b3(3, 2.);
  b3[1]=5;
  data << 1, 2, 3
        , 2, 5, 2
        , 3, 2, 5
        , 3, 2, 5
        , 1, 2, 3;
  TestMultiplicator functor3(data);
  CG<TestMultiplicator,CVectorX> testcg3(functor3,b3);

  if (testcg3.run()<0)
  { stk_cout << "An errror occur:" << testcg3.error();}
  stk_cout<<_T("CG iter = ") << testcg3.iter() <<_T("\n");
  stk_cout<<_T("CG Residuals = ") << testcg3.r().norm2() <<_T("\n");

  bEstim = functor3(testcg3.x());
  stk_cout<<"bEstim = " << bEstim <<_T("\n");
  res = (bEstim-b3).norm2();
  stk_cout<<"Computed Residuals = " << res <<_T("\n");


  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test PGC                                          +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  stk_cout<<"Preconditioned CG with Id :"<<_T("\n");
  IdMultiplicator idFunctor1;
  PCG<TestMultiplicator,IdMultiplicator,CVectorX> testpcg1(functor1, idFunctor1, b1);
  if (!testpcg1.run())
  { stk_cout << "An errror occur:" << testpcg1.error();}
  stk_cout<<"PCG Residuals = " << testpcg1.r().norm2() <<_T("\n");

  CVectorX bEstimPCG = functor1(testpcg1.x());
  Real resPCG = (bEstimPCG-b1).norm2();
  stk_cout<<"Computed Residuals = " << resPCG <<_T("\n");


  stk_cout<<"Preconditioned CG with Diag(A) :"<<_T("\n");
  DiagonalMultiplicator diagFunctor1(functor1.A_.transpose() * functor1.A_);
  PCG<TestMultiplicator,DiagonalMultiplicator,CVectorX> testpcg1b(functor1, diagFunctor1, b1);
  if (!testpcg1b.run())
  { stk_cout << "An errror occur:" << testpcg1b.error();}
  stk_cout<<"PCG Residuals = " << testpcg1b.r().norm2() <<_T("\n");

  CVectorX bEstimPCGb = functor1(testpcg1b.x());
  Real resPCGb = (bEstimPCGb-b1).norm2();
  stk_cout<<"Computed Residuals = " << resPCGb <<_T("\n");

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for STK::CG          +\n");
  stk_cout << _T("+ No errors detected.                                   +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}
