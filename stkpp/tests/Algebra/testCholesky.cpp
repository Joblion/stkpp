/*--------------------------------------------------------------------*/
/*     Copyright (C) 2013-2017  Serge Iovleff

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._DOT_I..._AT_stkpp.org (see copyright for ...)
*/

/*
 * Project:  stkpp::tests
 * created on: 29 mars 2017
 * Author:   Serge Iovleff
 **/

/** @file testCholesky.cpp
 *  @brief In this file we test the Cholesky algorithm.
 **/

#include "Algebra.h"
#include "Arrays.h"

using namespace STK;

/** test Cholesky matrices */
void testCholesky(int N, bool verbose)
{
  stk_cout << _T("+++++++\n");
  stk_cout << _T("+ Test Cholesky with size = ") << N << _T("\n");
  CArrayXX A(N, N);
  A.randGauss();
  ArrayDiagonalX D;
  ArrayLowerTriangularXX L;
  //
  if (!cholesky(A*A.transpose(), D, L))
  { stk_cout << _T("Cholesky decomposition failed\n");}
  if (verbose)
  {
    stk_cout << _T("AA' =\n") << A*A.transpose() << _T("\n");
    stk_cout << _T("L =\n") << L << _T("\n");
    stk_cout << _T("D =\n") << D << _T("\n");
    stk_cout << _T("LDL' =\n") << L*D*L.transpose() << _T("\n");
  }
  stk_cout << _T("|AA'-L*D*L'| =\n") << (A*A.transpose()-L*D*L.transpose()).abs().maxElt() << _T("\n");
}


int main(int argc, char *argv[])
{
  int N =100;
  bool verbose = false;
  if (argc >= 2) { N = atoi(argv[1]);}
  if (argc >= 3) { verbose = atoi(argv[2]);}

  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Test STK::Cholesky                                +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  testCholesky(N, verbose);
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Successful completion of testing for                  +\n");
  stk_cout << _T("+ STK::Cholesky. No errors detected.                    +\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("\n\n");
  return 0;
}
