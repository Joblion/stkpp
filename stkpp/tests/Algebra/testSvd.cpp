/*--------------------------------------------------------------------*/
/*     Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the
    Free Software Foundation, Inc.,
    59 Temple Place,
    Suite 330,
    Boston, MA 02111-1307
    USA

    Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project:  Algebra
 * Purpose:  test program for testing Svd class.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testSvd.cpp
 *  @brief In this file we test the Svd class.
 **/

#include "STKpp.h"


using namespace STK;

template<class Array>
void writeResult( Svd<Array> const& s)
{
  // write result
  stk_cout << _T("U=\n");
  stk_cout << s.U() << _T("\n");
  stk_cout << _T("D=");
  stk_cout << s.D().transpose() << _T("\n");
  stk_cout << _T("V=\n");
  stk_cout << s.V() << _T("\n");
  stk_cout << _T("UDV'=\n");
  // UDV'
  Array res;
  // compute D
  if (s.U().cols() != s.D().range())
  {
    Array2DDiagonal<Real> D(s.U().cols(), 0.0);
    for (int i= s.D().begin(); i< s.D().end(); ++i) { D[i]=s.D()[i];}
    res = s.U() * D * s.V().transpose();
  }
  else
  {
    res = s.U() * s.D() * s.V().transpose();
  }
  stk_cout << res << _T("\n");
  stk_cout << _T("Generalized inverse=\n");
  Array inv;
  s.ginv(inv);
  if(s.U().sizeRows() <= s.U().sizeCols())
  {
    stk_cout << _T("AA^{-1}=\n");
    stk_cout << res*inv << _T("\n");
  }
  else
  {
    stk_cout << _T("A^{-1}*A=\n");
    stk_cout << inv*res << _T("\n");
  }
}

void writeResult( lapack::Svd const& s)
{
  // write result
  stk_cout << _T("U=\n");
  stk_cout << s.U() << _T("\n");
  stk_cout << _T("D=");
  stk_cout << s.D().transpose() << _T("\n");
  stk_cout << _T("V'=\n");
  stk_cout << s.V().transpose() << _T("\n");
  stk_cout << _T("UDV'=\n");
  // UDV'
  CArrayXX res = s.U() * s.D().diagonalize() * s.V().transpose();
  stk_cout << res << _T("\n");
  stk_cout << _T("Generalized inverse\n");
  CArrayXX inv;
  s.ginv(inv);
  if(s.U().sizeRows() <= s.U().sizeCols())
  {
    stk_cout << _T("AA^{-1}=\n");
    stk_cout << res*inv << _T("\n");
  }
  else
  {
    stk_cout << _T("A^{-1}*A=\n");
    stk_cout << inv*res << _T("\n");
  }
}

/* testing svd. */
template<class Array, class Svd>
void test_svd(int M, int N, int base)
{
  int j,k;

  // Test svd
  Array A(Range(1,N),Range(1,M));
  int size = std::min(M,N);

  // First test
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("preliminary test R0(T)\n");
  Array T(Range(1,2), Range(1,2), 0.0);
  T(1,2) = 1.0;
  T(2,2) = 1.0;
  Svd R0(T);
  R0.run();
  stk_cout << _T("Svd Done\n");
  writeResult(R0);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("First test R1(A)\n");
  A = 0.0;
  for (int i=1; i<=size; i++) { A(i,i) = 1.0;}
  for (int i=1; i<size; i++) { A(i,i+1) = 2;}
  if (A.sizeCols()>size) A(size, size+1) = 2.0;
  if (size>=2) A(2,2) = 0.0;

  stk_cout << _T("A =\n") << A << _T("\n");
  Svd R1(A);
  R1.run();
  writeResult(R1);

  // First test
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("First test (bis) R1.setData(A)\n");
  A.resize(Range(1,M), Range(1,N));
  A =0.0;
  for (int i=1; i<size; i++) { A(i,i+1) = 2.0; A(i,i) = 1.0;}
  if (A.sizeCols()>size) A(size, size+1) = 2.0;
  if (size>=2) A(2,2) = 0.0;

  stk_cout << _T("A =\n") << A << _T("\n");
  R1.setData(A);
  R1.run();
  writeResult(R1);

  // Second test
  A.resize(Range(1,M),Range(1,N));
  for (int i=1, k=1; i<=M; i++)
    for (j=1; j<=N; j++)
    { A(i,j) = M*N-k++;}

  for (int i=1; i<size; i++) { A(i,i+1) = 2.0; A(i,i) = 1.0;}
  if (A.sizeCols()>size) A(size, size+1) = 2.0;
  if (size>=2) A(2,2) = 0.0;

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Second test : R1.setData(A, true)\n");
  stk_cout << _T("A =\n") << A << _T("\n");
  R1.setData(A, true);
  R1.run();
  writeResult(R1);

  // Third test
  A.resize(Range(1,N),Range(1,M));
  for (int i=N, k=1; i>=1; i--)
    for (j=1; j<=M; j++)
      A(i,j) = M*N-k++;

  for (int i=1; i<size; i++) { A(i,i+1) = 2.0; A(i,i) = 1.0;}
  if (A.sizeCols()>size) A(size, size+1) = 2.0;
  if (size>=2) A(2,2) = 0.0;

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Third test : Svd R3(A, false, true, true, M)\n");
  stk_cout << _T("A =\n") << A << _T("\n");
  Svd R3(A, false, true, true);
  R3.run();
  writeResult(R3);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Other test : R3 = R1\n");
  R3 = R1;
  writeResult(R3);

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Other test : Svd  R4(R3)\n");
  Svd R4(R3);
  writeResult(R4);

  // Fourth test
  A.resize(Range(1,2*M),Range(1,N));
  A = 0.0;
  for (j=2, k=0; j<=N; j++, k++)
    for (int i=1; i<=2*M; i++)
    { A(i,j) = k;}
  for (int i=2; i<size; i++) { A(i,i+1) = 2.0; A(i,i) = 1.0;}
  if (A.sizeCols()>size) A(size, size+1) = 2.0;
  if (size>=2) A(2,2) = 0.0;

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("Fourth test with A.resize(2*M,N)\n");
  stk_cout << _T("A =\n") << A << _T("\n");
  R3.setData(A);
  R3.run();
  writeResult(R3);

  // Fifth test
  Array* B = new Array(Range(1,2*M), Range(1,N), 0.0);
  for (j=2, k=0; j<=N; j++, k++)
    for (int i=1; i<=2*M; i++)
    { (*B)(i,j) = k;}
  for (int i=2; i<size; i++) { (*B)(i,i+1) = 2.0; (*B)(i,i) = 1.0;}
  if (size>=2) (*B)(2,2) = 0.0;

  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Fifth test (with pointer) : setData(*B)\n");
  stk_cout << _T("B =\n") << *B << _T("\n");
  R3.setData(*B);
  R3.run();
  writeResult(R3);

  // Fifth test
  stk_cout << _T("\n\n");
  stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
  stk_cout << _T("+ Fifth test 2 (with pointer and reference) : Svd(B)\n");
  stk_cout << _T("B =\n") << *B << _T("\n");
  B->shift(base,base);
  Svd R5(*B, true);
  R5.run();
  writeResult(R5);
  delete B;
}

// Main
int main(int argc, char *argv[])
{
  int M;
  int N;
  if (argc < 3)
  {
    stk_cout << _T("Usage: M, N\n");
    stk_cout << _T("Setting: M=16 and N=10\n");
    M = 16;
    N =10;
  }
  else
  {
    M = atoi(argv[1]);
    N = atoi(argv[2]);
  }
  try
  {
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::Svd                                       +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("M = ") << M << _T(" N = ") << N;

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::Svd with Array2D                          +\n");
    test_svd<ArrayXX, Svd<ArrayXX> >(M,N,1);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::Svd with CArray                           +\n");
    test_svd<CArrayXX, Svd<CArrayXX> >(M,N,1);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::lapack::Svd with Array2D                  +\n");
    test_svd<ArrayXX, lapack::Svd >(M,N,0);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::lapack::Svd with CArray                   +\n");
    test_svd<CArrayXX, lapack::Svd>(M,N,0);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for Svd            +\n");
    stk_cout << _T("+ No errors detetected.                               +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
