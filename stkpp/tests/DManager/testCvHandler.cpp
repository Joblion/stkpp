/*--------------------------------------------------------------------*/
/*   Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the
  Free Software Foundation, Inc.,
  59 Temple Place,
  Suite 330,
  Boston, MA 02111-1307
  USA

  Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::DManager
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testCvHandler.cpp
 *  @brief In this file we test the CvHandler class.
 **/

#include "DManager.h"
#include "Arrays.h"

using namespace STK;

/** @brief allow to number to the element of an Arrays */
template <typename Type>
struct numberingVisitor
{
  Type value_;
  numberingVisitor() : value_(1) {};
  inline Type const& operator() ()
  { value_++; return value_;}
};

/** Utility function for numbering an array */
template<typename Derived>
void numbering(Derived& matrix)
{
typedef typename hidden::Traits<Derived>::Type Type;
numberingVisitor<Type> visitor;
matrix.apply(visitor);
}

/* main print */
template<class Array >
void print(Array const& A, String const& name)
{
  stk_cout << _T("Print: ") << name << _T("\n";);
  stk_cout << name << _T(".rows() =")  << A.rows()  << _T("\n");
  stk_cout << name << _T(".cols() =")  << A.cols()  << _T("\n");
  if (A.sizeCols() == 1)
  { stk_cout << name << _T("= ") << A.transpose() << _T("\n");}
  else
    if (A.sizeRows() == 1)
    { stk_cout << name << _T("= ") << A << _T("\n");}
    else
    { stk_cout << name << _T("=\n") << A << _T("\n");}
}

template<class Array >
void shortPrint(Array const& A, String const& name)
{
  if (A.sizeCols() == 1)
  { stk_cout << name << _T("= ") << A.transpose();}
  else
  if (A.sizeRows() == 1)
  { stk_cout << name << _T("= ") << A;}
  else
  { stk_cout << name << _T("=\n") << A;}
}


template< class Data>
void TestCvHandler(Range I, Range J, bool output)
{
    //
    if (output) stk_cout << _T("Test A(") << I << _T(",")<< J << _T("); \n");
    Data A(I, J);
    Data Y(I,1);
    Data xFold, xTest, yFold, yTest;
    numbering(A);
    numbering(Y);
    if (output) { print(A,"A");}
    if (output) { print(Y,"Y");}
    CvHandler handler1(I, I.size());
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    if (output) stk_cout << _T("handler.sizePartitions()= ") << handler1.sizePartitions().transpose();

    if (output) stk_cout << _T("TestgetKFold(k,...,x)\n");
    for (int k= handler1.sizePartitions().begin(); k<handler1.sizePartitions().end(); ++k)
    { handler1.getKFold(k, A, xFold, xTest);
    if (output)
    { stk_cout << _T("Fold ") << k << _T(". "); shortPrint(xTest,"xTest");}}

    if (output) stk_cout << _T("TestgetKFold(k,...,x,y)\n");
    for (int k= handler1.sizePartitions().begin(); k<handler1.sizePartitions().end(); ++k)
    { handler1.getKFold(k, A, xFold, xTest, Y, yFold, yTest);
      if (output)
      { stk_cout << _T("Fold ") << k << _T(". "); shortPrint(xTest,"xTest");
        stk_cout << _T("Fold ") << k << _T(". "); shortPrint(yTest,"yTest");
      }
    }

    // second test
    if (output) stk_cout << _T("Test nbFold = 4\n");
    handler1.setData(I,4);
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    if (output) stk_cout << _T("handler.sizePartitions()= ") << handler1.sizePartitions().transpose();

    if (output) stk_cout << _T("TestgetKFold(k,...,x)\n");
    for (int k= handler1.sizePartitions().begin(); k<handler1.sizePartitions().end(); ++k)
    { handler1.getKFold(k, A, xFold, xTest);
    if (output)
    { stk_cout << _T("Fold ") << k << _T(". "); shortPrint(xTest,"xTest");}}

    if (output) stk_cout << _T("TestgetKFold(k,...,x,y)\n");
    for (int k= handler1.sizePartitions().begin(); k<handler1.sizePartitions().end(); ++k)
    { handler1.getKFold(k, A, xFold, xTest, Y, yFold, yTest);
      if (output)
      { stk_cout << _T("Fold ") << k << _T(". "); shortPrint(xTest,"xTest");
        stk_cout << _T("Fold ") << k << _T(". "); shortPrint(yTest,"yTest");
      }
    }
}

template< class Data>
void TestPartitionHandler(Range I, Range J, bool output)
{
    //
    if (output) stk_cout << _T("Test I= ") << I << _T(", prop = 0.2\n");
    Data A(I, J);
    Data Y(I,1);
    Data xFold, xTest, yFold, yTest;
    numbering(A);
    numbering(Y);
    if (output) stk_cout << _T("A(") << I << _T(",")<< J << _T("); \n");
    if (output) { print(A,"A");}
    if (output) { print(Y,"Y");}
    PartitionHandler handler1(I, 0.2);
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    handler1.getPartitions( A, xFold, xTest);
    if (output) { shortPrint(xTest,"xTest");}
    handler1.getPartitions( A, xFold, xTest, Y, yFold, yTest);
    if (output) { shortPrint(xTest,"xTest");shortPrint(yTest,"yTest");}
    // second test
    if (output) stk_cout << _T("Test I= ") << I << _T(", prop = 0.3\n");
    handler1.setData(I, 0.3);
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    handler1.getPartitions( A, xFold, xTest);
    if (output) { shortPrint(xTest,"xTest");}
    handler1.getPartitions( A, xFold, xTest, Y, yFold, yTest);
    if (output) { shortPrint(xTest,"xTest");shortPrint(yTest,"yTest");}
    // limit test
    if (output) stk_cout << _T("Test I= ") << I << _T(", prop = 0.\n");
    handler1.setData(I, 0.);
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    handler1.getPartitions( A, xFold, xTest);
    if (output) { shortPrint(xTest,"xTest");}
    handler1.getPartitions( A, xFold, xTest, Y, yFold, yTest);
    if (output)
    { shortPrint(xTest,"xTest");
      shortPrint(yTest,"yTest");
    }
    // limit test
    if (output) stk_cout << _T("Test I= ") << I << _T(", prop = 1.\n");
    handler1.setData(I, 1.);
    handler1.run();
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    handler1.getPartitions( A, xFold, xTest);
    if (output) { shortPrint(xTest,"xTest");}
     // third test
    if (output) stk_cout << _T("handler.partitions()= ") << handler1.partitions().transpose();
    handler1.getPartitions( A, xFold, xTest, Y, yFold, yTest);
    if (output)
    { shortPrint(xTest,"xTest");
      shortPrint(yTest,"yTest");
    }
}

/* main. */
int main(int argc, char *argv[])
{
  try
  {
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::CvHandler                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    TestCvHandler<CArrayXX>(31, 3, true);
    TestCvHandler<CArrayXX>(Range(5,26), 3, true);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for STK::CvHandler +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test STK::PartitionHandler                          +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    TestPartitionHandler<CArrayXX>(31, 3, true);
    TestPartitionHandler<CArrayXX>(Range(5,26), 3, true);
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for STK::CvHandler +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception const& error)
  {
    std::cerr << _T("An error occured : ") << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
