/*--------------------------------------------------------------------*/
/*   Copyright (C) 2004-2016  Serge Iovleff, Université Lille 1, Inria

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the
  Free Software Foundation, Inc.,
  59 Temple Place,
  Suite 330,
  Boston, MA 02111-1307
  USA

  Contact : S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
*/

/*
 * Project: stkpp::Arrays
 * Purpose:  test program for testing Arrays classes.
 * Author:   Serge Iovleff, S..._Dot_I..._At_stkpp_Dot_org (see copyright for ...)
 */

/** @file testVariable1D.cpp
 *  @brief In this file we test the TContainer 1D classes.
 **/

#include "Arrays.h"

using namespace STK;

template< class Type >
void print(Variable<Type> const& A, String const& name)
{
  stk_cout << "print: " << name << _T("\n";);
  stk_cout << name << _T(".isRef() =")     << A.isRef()  << _T("\n");
  stk_cout << name << _T(".range() =")     << A.range()  << _T("\n");
  stk_cout << name << _T(".cols() =")      << A.cols()  << _T("\n");
  stk_cout << name << _T(".rows() =")      << A.rows()  << _T("\n");
  stk_cout << name << _T(".capacity() = ") << A.capacity() << _T("\n");
  stk_cout << name << _T(".p_data() = ")   << A.allocator().p_data() << _T("\n");
  stk_cout << name << _T("=\n");
  stk_cout << name << ".name() =" << A.name() << _T("\n");
  stk_cout << name << _T("=\n")   << A << _T("\n\n");
  //  for (int i=A.begin(); i<A.end(); ++i)
  //  { stk_cout << A[i] << _T(" ");}
  //  stk_cout << _T("\n");
}

// test Variable method (see testArray1D file)
template< typename Type >
void writeVariable( Variable<Type> const& C, String const& name, String const& message)
{
  stk_cout << _T("==>") << message;
  print(C, name);
}

/* template main test function for 1D arrays. */
template< typename Type  >
void testVariable( int M, int N, Range J, bool output)
{
  Variable<Type> B(N,4,"var B");
  if (output)
  { writeVariable(B, _T("B"), _T("Test constructor:  B(N,4,\"var1\")\n"));}

  Variable<Type> C("var C");
  if (output)
  { writeVariable(C, _T("C"), _T("Test null constructor: C()\n"));}

  Variable<Type> D(Range(0,M, 0), "var D"); D= (3);
  if (output)
  { writeVariable(D, _T("D"), _T("Test constructor: D(Range(0,M)); D = 3\n"));}

  B.exchange(D);
  if (output)
  { writeVariable(B, _T("B"), _T("Test B.exchange(D);\n"));
    writeVariable(D, _T("D"), _T(""));
  }

  D.sub(J)= (5);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.sub(J) = 5 (modification of a lhs object !)\n"));}
  Range J1 = J.dec();
  Variable<Type> Dref(D.sub(J1), true);
  if (output)
  { writeVariable(Dref, _T("Dref"), _T("Test Dref(D.sub(J.dec()), true)\n"));}

  if (output)
  { writeVariable(B.sub(J), _T("B.sub(J)"), _T("Test B.sub(J)\n"));}

  Dref.assign(B.sub(J));
  if (output)
  { writeVariable(D, _T("D"), _T("Test Dref.copy(B.sub(J))\n"));}

  D.shift(1);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.shift(1)\n"));}

  D.pushBack(2); D.sub(Range(M,D.lastIdx(), 0)) = (1);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.pushBack(2); D.sub(Range(M,D.lastIdx()), 0) = 1\n"));}

  D.swap(D.begin(),D.lastIdx());
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.swap(D.begin(),D.lastIdx())\n"));}

  D.erase(2, D.size()-1);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.erase(2,D.size()-1)\n"));}

  D.erase(D.lastIdx());
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.erase(D.lastIdx())\n"));}

  D.pushBack(M); D = (5);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.pushBack(M); D=5\n"));}

  D.push_back(6);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.push_back(6);\n"));}

  D.push_front(4);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.push_front(4);\n"));}

  D.resize(J);
  if (output)
  { writeVariable(D, _T("D"), _T("Test D.resize(J)\n"));}

  C.resize(D.range());
  C= (0);
  if (output)
  { writeVariable(C, _T("C"), _T("Test C.resize(D.range());C= TYPE(0);\n"));}

  C = D;
  if (output)
  { writeVariable(C, _T("C"), _T("Test C = D\n"));
    writeVariable(D, _T("D"), _T("\n"));
  }

  C.front() = (-2); C.back()  = (-2);
  if (output)
  { writeVariable(C, _T("C"), _T("Test C.front() = -2 and C.back() = -2\n"));}

  C.elt(J.begin()) = C.front();
  C.elt(J.lastIdx())  = C.back();
  if (output)
  { writeVariable(C, _T("C"), _T("Test C.at(J.begin()) = C.front() and C.at(J.lastIdx()) = C.back()\n"));}

  C.elt(C.lastIdx()-1) = -C.elt(C.lastIdx()-1);
  if (output)
  { writeVariable(C, _T("C"), _T("Test C.elt(C.lastIdx()-1) = -C.elt(C.lastIdx()-1)\n"));}

  C.erase(J.begin(), J.size());
  if (output)
  { stk_cout << _T("J= ") << J << _T("\n");
    writeVariable(C, _T("C"), _T("Test C.erase(J.begin(), J.size())\n"));
  }
}

/* main. */
int main(int argc, char *argv[])
{ // N=15
  int  M=12, N=15, Jbeg = 3, Jend = 9;
  bool output = true;
  try
  {
    Range J(Jbeg, Jend, 0);
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Test Variable class                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("Using M  = ") << M    << _T(" N    = ") << N  << _T("\n")
             << _T("J = ") << J << _T("\n");
    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("testVariable<Real> : \n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");

    testVariable<Real>(M, N, J, output);

    stk_cout << _T("\n\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("+ Successful completion of testing for STK::Variable  +\n");
    stk_cout << _T("+ No errors detected.                                 +\n");
    stk_cout << _T("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    stk_cout << _T("\n\n");
  }
  catch (Exception & error)
  {
    std::cerr << "An error occured : " << error.error() << _T("\n";);
    return -1;
  }
  return 0;
}
