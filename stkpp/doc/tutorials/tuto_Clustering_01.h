/*
 * tuto_Clustering.h
 *
 *  Created on: 25 oct. 2013
 *      Author: iovleff
 */


/** @page TutorialClustering1 Clustering: How to add new mixture models (Part 1)
 *
 * @section ClusterIntroduction Introduction
 *
 * This tutorial will show how to add a mixture model to STK++.
 *
 * The design pattern of the Clustering project is based on the idea of plugin
 * so that you can add your own mixture model to the architecture without many
 * effort. If you want a better integration of your mixture model to the library,
 * the Clustering project proposes a set of interface classes with predefined
 * behavior that can be used at your convenience.
 *
 * In short, the main Interface to derive is the @link STK::IMixture IMixture @endlink
 * class. This class is  well documented and pure virtual methods to implement
 * are self explaining.
 * The class @link STK::MixtureComposer MixtureComposer @endlink estimates the
 * mixture model using only pointers on this interface.
 *
 * It is thus possible to implement and integrate mixture models to the project
 * with many freedom. However, if you want a closer integration of the mixture model
 * to the Clustering project, it can be convenient to use  general
 * design pattern adopted by the project and to implement the Interface classes
 * furnished.
 *
 * This tutorial will describe step by step how to integrate new models to the
 * project Clustering.
 *
 * @section ClusterAbout About Mixture Models
 *
 * A mixture model on some subset \f$ J\subset \mathbb{R}^d \f$ is a density of
 * the form
 * \f[
 *     f(\mathbf{x}|\boldsymbol{\theta})
 *     = \sum_{k=1}^K p_k f(\mathbf{x};\boldsymbol{\lambda}_k,\boldsymbol{\alpha})
 *     \quad \mathbf{x} \in J.
 * \f]
 *
 * The \f$ p_k > 0\f$ with  \f$ \sum_{k=1}^K p_k =1\f$ are the mixing proportions.
 * The density \e f is called the component of the model. The parameters
 * \f$\boldsymbol{\lambda}_k, \, k=1,\ldots K \f$ are the cluster specific parameters
 * and the parameters \f$ \boldsymbol{\alpha} \f$ are the shared parameters.
 *
 * The whole set of parameters is thus
 * \f[
 * \boldsymbol{\theta} = \{ (p_k)_{k=1}^K, (\boldsymbol{\lambda}_k)_{k=1}^K, \boldsymbol{\alpha} \}.
 * \f]
 *
 * For example the diagonal Gaussian mixture model @c STK::DiagGaussian_sjk is
 * the most general diagonal Gaussian model and has a density function of the form
 * \f[
 *  f(\mathbf{x}|\theta) = \sum_{k=1}^K p_k \prod_{j=1}^d
 *    \frac{1}{\sqrt{2\pi}\sigma^j_{k}} \exp\left\{-\frac{1}{2}\left(\frac{x^j-\mu^j_k}{\sigma^j_{k}}\right)^2\right\}
 * \f]
 * All the parameters are cluster specific. There is no shared parameters
 * and \f$ \boldsymbol{\lambda}_k = \{ (\mu^j_k)_{j=1}^d, (\sigma^j_{k})_{j=1}^d \} \f$
 *
 * On the other side the diagonal Gaussian mixture model @c STK::DiagGaussian_s is
 * the most parsimonious diagonal Gaussian model and has density function
 * \f[
 *  f(\mathbf{x}|\theta) = \sum_{k=1}^K p_k \prod_{j=1}^d
 *    \frac{1}{\sqrt{2\pi}\sigma} \exp\left\{-\frac{1}{2}\left(\frac{x^j-\mu^j_k}{\sigma}\right)^2\right\}.
 * \f]
 *
 * In both cases the means are class specific parameters, i.e.
 * \f$ \boldsymbol{\lambda}_k = \{ (\mu^j_k)_{j=1}^d \}\f$.
 * In the second model the common standard deviation is a shared parameter, i.e.
 * \f$ \boldsymbol{\alpha} = \{ \sigma \} \f$.
 *
 *
 * We will illustrate this tutorial with the intermediate mixture model
 * @c STK::DiagGaussian_sjsk with density function
 * \f[
 *  f(\mathbf{x}|\theta) = \sum_{k=1}^K p_k \prod_{j=1}^d
 *    \frac{1}{\sqrt{2\pi}\sigma^j\sigma_{k}}
 *    \exp\left\{-\frac{1}{2}\left(\frac{x^j-\mu^j_k}{\sigma^j\sigma_{k}}\right)^2\right\}.
 * \f]
 * This mixture model was not implemented in previous version
 * of the Clustering project  (before 2017/09/05).
 *
 *
 *
 * @section ClusterMixtureIdentification Creating an Identification (Id)
 *
 * The first step is to be able to identify the new model as a model recognized
 * by the STK interfaces. This is achieved by updating the files @link STK_Clust_Util.h @endlink
 * and @link  STK_Clust_Util.cpp @endlink.
 *
 * In the first file, we just add a new line in the
 * @link STK::Clust::Mixture Mixture @endlink enumeration
 * <table class="example">
 * <tr><th>Before:</th><th>After:</th></tr>
 * <tr><td>
 * @code
 * enum Mixture
 * {
 *   Gamma_ajk_bjk_ =0,
 *   //...
 *   Gamma_a_bk_, // = 11
 *   Gaussian_sjk_ =20,
 *   Gaussian_sk_,
 *   Gaussian_sj_,
 *   Gaussian_s_, // = 23
 *   //...
 * }
 * @endcode
 * </td>
 * <td>
 * @code
 * enum Mixture
 * {
 *   Gamma_ajk_bjk_ =0,
 *   //...
 *   Gamma_a_bk_, // = 11
 *   Gaussian_sjk_ =20,
 *   Gaussian_sk_,
 *   Gaussian_sj_,
 *   Gaussian_s_,
 *   Gaussian_sjsk_, // = 24
 *   //...
 * }
 * @endcode
 * </td></tr>
 * </table>
 *
 * In the second file we update the input/output utilities functions
 * @code
 *   MixtureClass mixtureToMixtureClass( Mixture const& type);
 *   Mixture stringToMixture( std::string const& type);
 *   Mixture stringToMixture( std::string const& type, bool& freeProp);
 *   std::string mixtureToString( Mixture const& type);
 *   std::string mixtureToString(Mixture type, bool freeProp);
 * @endcode
 * by adding the following pieces of code
 * @code
 * // The model Gaussian_sjsk_ is part of the DiagGaussian_ models
 * MixtureClass mixtureToMixtureClass( Mixture const& type)
 * {
 *   //...
 *  if (type == Gaussian_sjsk_)     return DiagGaussian_;
 *   //...
 * }
 * // The model Gaussian_sjsk_ is encoded as "Gaussian_sjsk"
 * Mixture stringToMixture( std::string const& type)
 * {
 *   //...
 *   if (toUpperString(type) == toUpperString(_T("Gaussian_sjsk"))) return Gaussian_sjsk_;
 *   //...
 * }
 * // The model Gaussian_sjsk_ is encoded as "Gaussian_p_sjsk" (fixed proportions)
 * // or "Gaussian_pk_sjsk" (free proportions)
 * Mixture stringToMixture( std::string const& type, bool& freeProp)
 * {
 *   freeProp = false;
 *   //...
 *   if (toUpperString(type) == toUpperString(_T("Gaussian_p_sjsk"))) return Gaussian_sjsk_;
 *   //...
 *   freeProp = true;
 *   //...
 *   if (toUpperString(type) == toUpperString(_T("Gaussian_pk_sjsk"))) return Gaussian_sjsk_;
 *   //...
 * }
 * // The model Gaussian_sjsk_ is encoded as "Gaussian_sjsk"
 * std::string mixtureToString( Mixture const& type)
 * {
 *   //...
 *   if (type == Gaussian_sjsk_)      return String(_T("Gaussian_sjsk"));
 *   //...
 * }
 * // The model Gaussian_sjsk_ is encoded as "Gaussian_p_sjsk" (fixed proportions)
 * // or "Gaussian_pk_sjsk" (free proportions)
 * std::string mixtureToString(Mixture type, bool freeProp)
 * {
 *   if (freeProp == false)
 *   {
 *     //...
 *     if (type == Gaussian_sjsk_)     return String(_T("Gaussian_p_sjsk"));
 *     //...
 *   }
 *   else
 *   {
 *     //...
 *     if (type == Gaussian_sjsk_)     return String(_T("Gaussian_pk_sjsk"));
 *     //...
 *   }
 * @endcode
 *
 * @section ClusterMixtureParameters Encoding the Parameters
 *
 * The @c struct STK::ModelParameters encapsulates the parameters of the mixture model.
 * It is a template struct that must be fully specialized and store the parameters of
 * the mixture model \f$ \mu_{kj}, \sigma_j, \sigma_k \f$. This class also compute
 * statistics about the values of the parameters along the iterations (the mean and the standard
 * deviation).
 *
 * @code
 * template<>
 * struct ModelParameters<Clust::Gaussian_sjsk_>
 * {
 *   Array1D<CPointX> mean_; // An array of size K with the row-vector of the mean
 *   CVectorX sigmak_;       // Standard deviations of the classes
 *   CPointX sigmaj_;        // Standard deviations of the variables
 *   Array1D< Stat::Online<CPointX, Real> > stat_mean_; // An array with the statistics of the means
 *   Stat::Online<CVectorX, Real> stat_sigmak_;  // Statistics for the sd of the classes
 *   Stat::Online<CPointX, Real> stat_sigmaj_;   // Statistics for the sd of the variables
 *
 * //....
 * }
 * @endcode
 * This class structure must also implement:
 * - a constructor with the number of cluster given,
 * - a copy constructor,
 * - an access to the mean and to the standard deviation independent of the model,
 * - a method which resize the arrays (mean_ and sigmaj_, ) when the data
 *   will be set and the number of variables will be known,
 * - updating/getting/releasing the statistics computed online,
 * - a way to set the parameter to some values (for initialization to a specific value purpose).
 *
 * @code
 *   ModelParameters(int nbCluster);
 *   ModelParameters( ModelParameters const& param);
 *   inline Real const& mean(int k, int j) const { return mean_[k][j];}
 *   inline Real sigma(int k, int j) const { return sigmak_[k] * sigmaj_[j];}
 *   void resize(Range const& range);
 *   void updateStatistics();
 *   void setStatistics();
 *   void releaseStatistics();
 *   template<class Array>
 *   void setParameters( ExprBase<Array> const& params);
 * @endcode
 *
 * This structure is defined in file @link STK_DiagGaussianParameters.h @endlink. We don't detail
 * the implementation which can be found in file @link STK_DiagGaussianParameters.cpp @endlink.
 *
 * @section ClusterMixtureDensity Creating the Mixture
 *
 * The class @link STK::DiagGaussian_sjsk DiagGaussian_sjsk @endlink is a template terminal class
 * derived (recursively) from the class @link STK::DiagGaussianBase DiagGaussianBase @endlink.
 * The template parameter is the type of the Array storing the data. This class must implement
 * - a constructor with the number of cluster given,
 * - a copy constructor,
 * - a random initialization methods of the parameters,
 * - a @c run method updating the values of the parameters,
 * - compute the number of parameters.
 *
 * @code
 *      DiagGaussian_sjsk( int nbCluster);
 *      DiagGaussian_sjsk( DiagGaussian_sjsk const& model);
 *      void randomInit( CArrayXX const* const& p_tik, CPointX const* const& p_tk);
 *      bool run( CArrayXX const* const& p_tik, CPointX const* const& p_tk) ;
 *      inline int computeNbFreeParameters() const
 *      { return (this->nbCluster()+1)*p_data()->sizeCols();}
 * };
 * @endcode
 * The @c run method update the parameters \f$ (\mu_{k}^j), (\sigma^j), (\sigma_{k}) \f$ by maximizing
 * \f[
 *  \sum_{i=1}^n \sum_{k=1}^K t_{ik}
 *  \left(
 *    -\log({\sigma^j\sigma_{k}})
 *    -\frac{1}{2}\left(\frac{x^j-\mu^j_k}{\sigma^j\sigma_{k}}\right)^2
 *  \right).
 * \f]
 *
 * Moreover, the traits class @link STK::hidden::MixtureTraits MixtureTraits @endlink must be
 * instanced (it is used by base class which don't know the parameter type)
 * @code
 *   namespace hidden
 *   {
 *   template<class Array_>
 *   struct MixtureTraits< DiagGaussian_sjsk<Array_> >
 *   {
 *     typedef Array_ Array;
 *     typedef ModelParameters<Clust::Gaussian_sjsk_> Parameters;
 *   };
 *   } // namespace hidden
 * @endcode
 * Concrete implementation can be found in file @link STK_DiagGaussian_sjsk.h @endlink
 **/
