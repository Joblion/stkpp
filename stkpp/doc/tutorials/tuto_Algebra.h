/*
 * tuto_Algebra.h
 *
 *  Created on: 17 august 2018
 *      Author: iovleff
 */

/** @page TutorialAlgebra Use of the Algebra utilities
 *
 *  @section ShortAlgebraDescription Introduction to Algebra project
 *
 *  The Algebra project provides structures, tools and methods of the
 *  usual algebra techniques on matrices and vectors. If lapack is installed
 *  on your system, most of the tools presented here use it in background. Otherwise
 *  built-in (but maybe less efficient) methods are used.
 *
 *  Main operations that can be performed are:
 *    @li Qr decomposition of an arbitrary matrix, @sa STK::Qr, STK::lapack::Qr
 *    @li SVD decomposition of an arbitrary matrix, @sa STK::Svd,STK::lapack::Svd
 *    @li Eigenvalue decomposition for symmetric (square) matrices and a
 *    generalized inverse method for such matrices, @sa STK::SymEigen, STK::lapack::SymEigen.
 *    @li Solving the linear least square problem @sa STK::MultiLeastSquare, STK::lapack::MultiLeastSquare.
 *    @li Matrix inversion @sa STK::InvertMatrix
 *    @li utilities methods like Cholesky decomposition, Conjugate Gradient algorithme, Givens
 *    rotation GramSchmidt orthogonalization,.... @sa STK::CG, STK::cholesky, STK::compGivens,
 *    STK::gramSchmidt
 *
 * @section MatrixDecompositions Matrix decomposition
 *
 * All methods for matrix decomposition are enclosed in classes. Computations are launched
 * using the run method.
 *
 * @subsection QRDecompositions QR decomposition
 *
 * QR decomposition (also called a QR factorization) of a matrix is a decomposition of
 * a matrix @b A into a product <B>A = QR</B> of an orthogonal matrix Q and an upper triangular matrix R.
 * Given a matrix @c A of size @c (m,n), QR decomposition (also called a QR factorization) of @c A
 * is a decomposition into a product @c A=QR of an orthogonal matrix @c Q of size @c (m,m)
 * and an upper triangular matrix @c R of size @c (m,n).
 *
 * QR decomposition can be achieved using either the class
 * @code
 *  STK::Qr
 * @endcode
 * or if lapack is available
 * @code
 *  STK::lapack::Qr
 * @endcode
 * In the later case, your code have to be compiled using @c -DSTKUSELAPACK flag and linked using
 * @c -llapack (for GNU-like compilers).
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoQr.cpp
 * </td>
 * <td>
 * \verbinclude tutoQr.out
 * </td></tr>
 * </table>
 *
 * @note By default the matrix @em Q is represented as a product of elementary reflectors
 * \f$ Q = H_1 H_2 . . . H_k\f$, where \f$k = \min(m,n)\f$
 * each \f$ H_i \f$ has the form \f$ H_i = I - \tau v * v' \f$.
 * It is possible to get the @c Q matrix (of size @c (m,m)) by using the <code>compQ()</code> method.
 * @c Q will be overwritten.
 *
 * It is possible to update a QR decomposition when a column is added or removed to the original
 * matrix. In the following example, we remove the second column of matrix and then insert a
 * column with value 1 on the second column.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoQrUpdate.cpp
 * </td>
 * <td>
 * \verbinclude tutoQrUpdate.out
 * </td></tr>
 * </table>
 * @note the printing mechanism of stk++ does not round small values to zero.
 *
 * @subsection SVDDecompositions SVD decomposition
 *
 * The singular-value decomposition of an @c (m,n) real (or complex) matrix
 * @b M is a factorization of the form \f$ \mathbf{U\Sigma V^*}\f$, where
 * @b U is an @c (m,m) real (or complex) unitary matrix,
 * \f$\mathbf{\Sigma}\f$ is an (m,n) rectangular diagonal matrix with non-negative
 * real numbers on the diagonal, and \f$\mathbf{V}\f$ is an @c (n,n) real
 * (or complex) unitary matrix.
 *
 * The diagonal entries \f$\sigma_i\f$ of \f$\mathbf{\Sigma}\f$ are known as the
 * singular values of @b M.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoSvd.cpp
 * </td>
 * <td>
 * \verbinclude tutoSvd.out
 * </td></tr>
 * </table>
 * @note Singular values are stored in a vector in lapack method and in a diagonal matrix
 * in STK++ method. It is also possible to compute only @b U and/or @b V matrix.
 *
 * @subsection SymEigenDecompositions Eigenvalues decomposition
 *
 * Let @b A be a square (N,N) matrix with N linearly independent eigenvectors,
 * \f$ q_i \,\, (i = 1, \dots, N).\f$  Then @b A can be factorized as
 * \f[ \mathbf{A}=\mathbf{Q}\mathbf{\Lambda}\mathbf{Q}^{-1} \f]
 * where @b Q is the square (N,N) matrix whose @e ith column is the eigenvector
 * \f$ q_i\f$ of @b A and @b Λ is the diagonal matrix whose diagonal elements are
 * the corresponding eigenvalues, i.e., \f$ \Lambda_{ii}=\lambda_i \f$.
 *
 * If @b A is a symmetric matrix then @b Q is an orthogonal matrix.
 *
 * STK provide native and Lapack interface classes allowing to compute the eigenvalue
 * decomposition of a @em symmetric square matrix.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoSymEigen.cpp
 * </td>
 * <td>
 * \verbinclude tutoSymEigen.out
 * </td></tr>
 * </table>
 *
 * @note STK++ eigenvalues computation need a full symmetric matrix as input while
 * lapack version use only upper part of the input data.
 * It is also possible to use the lower part of the matrix in the lapack version.
 *
 * @section LeastSquareProblems Solving least square problems
 *
 * In linear regression, the observations are assumed to be the result of
 * random deviations from an underlying relationship between the dependent
 * variables @b y and independent variable @b x.
 *
 * Given a data set
 * \f[ \{y_{i1},\ldots,y_{id},\, x_{i1}, \ldots, x_{ip}\}_{i=1}^n \f]
 * of @e n statistical units, a linear regression model assumes that the
 * relationship between the dependent variable @b y and the regressors @b x
 * is linear. This relationship is modeled through a disturbance term
 * that adds "noise" to the linear relationship between the dependent
 * variable and regressors. Thus the model takes the form
 * \f[
 * \mathbf{y}_i = \mathbf{x}^\mathsf{T}_i \boldsymbol\beta + \varepsilon_i,
 * \qquad i = 1, \ldots, n.
 * \f]
 * Often these @e n equations are stacked together and written in matrix notation
 *  as
 * \f[
 * \mathbf{Y} = X\boldsymbol\beta + \boldsymbol\varepsilon, \,
 * \f]
 *
 * STK++ provide native and Lapack interface classes allowing to solve the
 * least square regression problem.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoLeastSquare.cpp
 * </td>
 * <td>
 * \verbinclude tutoLeastSquare.out
 * </td></tr>
 * </table>
 *
 * @note It is also possible to solve weighted least-square regression problems.
 *
 * @section InversionMatrices Inverting matrices
 *
 * Matrices can be inverted using either the templated functor STK::InvertMatrix or the templated
 * function STK::invert. The first example below deals with general square and/or symmetric matrices.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoInvert.cpp
 * </td>
 * <td>
 * \verbinclude tutoInvert.out
 * </td></tr>
 * </table>
 *
 * The second example deals with lower and upper triangular arrays.
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoInvertTriangular.cpp
 * </td>
 * <td>
 * \verbinclude tutoInvertTriangular.out
 * </td></tr>
 * </table>
 *
 * @note If the matrix is not invertible, the result provided will be a generalized inverse.
 **/

