/* Quick reference for Arrays **/

/** @page ArraysQuickRef Arrays Quick Reference guide
 *
 * The containers/arrays you use in order to store and process the data in your
 * application greatly influence the speed and the memory usage of your application.
 * STK++ proposes a large choice of containers/arrays and methods that you can used in
 * conjunction with them.
 *
 * @section ArraysArraysAndVectors Existing Arrays and Vectors
 *
 * @p There are mainly two kinds of arrays you can use with STK++:
 * @li The %Array2D family which was defined and used in the oldest versions of STK++,
 * @li the %CArray family which has been introduced in version 0.4 of STK++ library.
 *
 * The special features of the %Arrays2D family are described in the
 * @link UsingArray2D Using Array2D @endlink tutorial.
 *
 * @subsection ArraysTypes Containers
 *
 * Data can be encapsulate in one of the following array
 * @code
    typedef CArray<Type, SizeRows, SizeCols, Orient> MyCArray;
    typedef CArraySquare<Type, Size, Orient> MyCSquare;
    typedef CArrayVector<Type, SizeRows, Orient> MyCVector;
    typedef CArrayPoint<Type, SizeCols, Orient> MyCPoint;

    typedef Array2D<Type> MyArray2D;
    typedef Array2DVector<Type> MyVector2D;
    typedef Array2DPoint<Type> MyPoint2D;
    typedef Array2DUpperTriangular<Type> MyUpperTriangular2D;
    typedef Array2DLowerTriangular<Type> MyLowerTriangular2D;
    typedef Array2DDiagonal<Type> MyDiagonal2D;
 * @endcode
 *
 * <ul>
 * <li> @c Type is the type of the elements like @c double, @c float, etc.
 * <li> @c Orient can be either @link STK::Arrays::by_col_ Arrays::by_col_ @endlink
 *  (= 1, the default template) or @link STK::Arrays::by_row_ Arrays::by_row_ @endlink (= 0)
 * <li> If you don't know the size of your array/vector just use @c STK::UnknownSize
 * (the default template)
 * </ul>
 * @note Only the first template argument is mandatory.
 *
 * @subsection ConstArrays Constant Arrays
 * It is possible to use constant arrays with all values equal to 1 using
 * the type predefined in the \code{namespace STK::Const}
 * @code
    Const::Identity<Type, 10> i; // size fixed at compile time
    Const::Identity<Type> i(10); // size fixed at runtime
    Const::Square<Type, Size> s;
    Const::Square<Type> s(10);
    Const::Vector<Type, Size> v;
    Const::Point<Type, Size> p;
    Const::Array< Type, SizeRows, SizeCols> a;
    Const::Array< Type> a(10, 20);
    Const::UpperTriangular< Type, SizeRows, SizeCols> u;
    Const::LowerTriangular< Type, SizeRows, SizeCols> l;
 * @endcode
 * As usual, only the first template parameter is mandatory.
 *
 * @section ArraysManipulating Manipulating Arrays and Vectors
 *
 * @subsection ArraysBasic Basic operations

 * Constructors are detailed in the tutorial @link ArraysConstructors Arrays Constructors. @endlink
 * After creation %Arrays can be initialized using comma initializer
 * @code
 *   CVector3  v;     v << 1, 2, 3;
 *   CSquareXX m(3);  v << 1, 2, 3,
 *                         2, 3, 4,
 *                         3, 4, 5;
 * @endcode
 * and elements can be accessed using the parenthesis (for arrays), the brackets
 * (for vectors) and the @c elt method
 * @code
 *   v[0] = 3; v.elt(1) = 1; v.at(2) = 2;       // at check index range
 *   m(0,0) = 5; m.elt(1,1) = 1; m.at(2,2) = 3; // at check indexes range
 * @endcode
 *
 * The whole array/vector can be initialized using a value, either at the construction
 * or during execution by using
 * @code
    v.setZeros()             // fill v with value 0
    m.setOnes();             // fill m with value 1
    Array2D<int> a(3, 3, 1); // arrays of size (3,3) with all the elements equal to 1
    a.setValue(2);           // fill a with value 2
 * @endcode
 *
@subsection SubPartArrays Getting parts of Arrays and Vectors
 *
 * It is possible to access to a row, a column, a sub-part of an array using
 * slicing operators.
 * @code
    m.row(i);              // get the ith row
    m.col(j);              // get the jth row
    m.row(Trange<4>(3,4)); // get sub-array m[3:6,]
    m.col(Trange<4>(1,4)); // get sub-array m[,1:3]
    m.sub(Trange<4>(3,4), Trange<4>(1,4)); // get sub-array m[3:6,1:3]
 * @endcode
 *
 * @subsection ArraysGetters Arrays basic informations
 *
 * For all the containers it is possible to get the status (reference or array owning its data)
 * the  range, the beginning, the end and the size using
 * @code
    CArrayXX m(3,4);
    bool mf = m.isRef(); // mf is false

    typename CArrayXX::ColRange mc= m.cols();
    int mbc= m.beginCols(), mec= m.endCols(), msc= m.sizeCols();

    typename CArrayXX::RowRange mr= m.rows();
    int mbr= m.beginRows(), mer= m.endRows(), msr= m.sizeRows();

    CArrayVectorX v(m.col(0), true); // v is a reference on the column 0 of m
    bool vf = v.isRef(); // vf is true

    CArrayVectorX::RowRange vr= m.range();
    int vb= m.begin(), ve= m.end(), vs= m.size();
 * @endcode
 *
 * For the %Array2D family, it is also possible to get informations about the
 * allocated memory of the containers. It can be interested in order to known
 * if the container will have to reallocate the memory if you try to resize it.
 * @code
 *   ArrayXX m(3,4);
 *   m.capacityCol(1);  // capacity of the column 1
 *   Array1D< Range > r = m.rangeCols () // vector storing the range of each columns (think to triangular matrices)
 * @endcode
 * The %Array2D family of container allocate a small amount of supplementary memory
 * so that in case of a @ resize, it is possible to expand the container without
 * data copy.
 *
 * @subsection ArraysVisitors Visitors and Appliers
 *
 * Visitors and appliers visit/are applied to an array or vector (or expression)
 * and are automatically unrolled if the array is of fixed (small) size.
 * <table class="manual">
 * <tr><th>Visitors</th><th>Appliers</th></tr>
 * <tr><td>
 * @code
    int  i= m.count();         // count the number of true values
    bool f= m.any();           // is there any true value ?
    bool f= m.all();           //
    Type r= m.minElt(i,j);     // can be v.minElt(i) or m.minElt()
    Type r= m.maxElt(i,j);     // can be v.maxElt(i) or m.maxElt()
    Type r= m.minEltSafe(i,j); // can be v.minEltSafe(i) or m.minEltSafe()
    Type r= m.maxEltSafe(i,j); // can be v.maxEltSafe(i) or m.maxEltSafe()
    Type r= m.sum(),  s= m.sumSafe();
    Type r= m.mean(), s= m.meanSafe();
 * @endcode
 * </td>
 * <td>
 * @code
    m.randUnif();  // fill m with uniform random numbers between 0 and 1
    m.randGauss(); // fill m with standardized Gaussian random numbers
    m.setOnes();   // fill m with value 1
    m.setValues(2);// fill m with value 2
    m.setZeros();  // fill m with value 0
    Law::Gamma law(1,2); // create a Gamma distribution
    m.rand(law);        // fill m with gamma(1,2) random numbers
 * @endcode
 * </td></tr>
 * </table>
 *
 * The next methods use visitors in order to compute the result, eventually safely
 * @code
    m.norm();  m.normSafe();
    m.norm2(); m.norm2Safe(); // norm without sqrt
    m.normInf();              // superior norm
    m.variance(); m.varianceSafe();
    // variance with fixed mean
    m.variance(mean); m.varianceSafe(mean);
    // weighted visitors
    m.wsum(weights);  m.wsumSafe(weights);
    m.wnorm(weights);  m.wnormSafe(weights);
    m.wnorm2(weights); m.wnorm2Safe(weights);
    m.wmean(weights);  m.wmeanSafe(weights);
    m.wvariance(weights); m.wvarianceSafe(weights);
    m.wvariance(mean, weights); m.wvarianceSafe(mean, weights);
 * @endcode
 *
 * @subsection ArraysFunctors functors on Arrays/vectors and Expressions
 *
 * If there is the possibility of missing/NaN values add the word
 * Safe to the name of the functor. If the mean by row is needed, just add ByRow
 * to the name of the functor. If you want a safe computation by row add SafeByRow.
 *
 * All functors applied on an array by column will return by value an STK::Array2DPoint and a number
 * if it is applied on a vector or a point.
 *
 * All functors applied on an array by row will return by value an STK::Array2DVector
 * and a number if it is applied on a vector or a point.
 *
 * @subsection StatFunctors Statistical functors
 *
 * All the functors applied on arrays are currently in the namespace STK::Stat.
 *
 * <table class="manual">
 * <tr><th>Functors by column </th> <th>Functors by row</th> </tr>
 * <tr><td>
 * @code
    Stat::min(a); Stat::minSafe(a); Stat::min(a, w); Stat::minSafe(a, w);
    Stat::max(a); Stat::maxSafe(a); Stat::max(a, w); Stat::maxSafe(a, w);
    Stat::sum(a); Stat::sumSafe(a); Stat::sum(a, w); Stat::sumSafe(a, w);
    Stat::mean(a); Stat::meanSafe(a); Stat::mean(a, w); Stat::meanSafe(a, w);
    // could also be
    Stat::minByCol(a); Stat::minSafeByCol(a); Stat::minByCol(a, w); Stat::minSafeByCol(a, w);
    Stat::maxByCol(a); Stat::maxSafeByCol(a); Stat::maxByCol(a, w); Stat::maxSafeByCol(a, w);
    Stat::sumByCol(a); Stat::sumSafeByCol(a); Stat::sumByCol(a, w); Stat::sumSafeByCol(a, w);
    Stat::meanByCol(a); Stat::meanSafeByCol(a); Stat::meanByCol(a, w); Stat::meanSafeByCol(a, w);
    // unbiased variance (division by n-1) when unbiased is true, false is the default
    Stat::variance(a, false); Stat::varianceSafe(a, false);
    Stat::variance(a, w, false); Stat::varianceSafe(a, w, false);
    // fixed mean. Must be a vector/point for arrays and a number for vectors/points
    // unbiased (false in examples below) has to be given
    Stat::varianceWithFixedMean(a, mean, false);
    Stat::varianceWithFixedMean(a, w, mean, false);
    Stat::varianceWithFixedMeanSafe(a, mean, false);
    Stat::varianceWithFixedMeanSafe(a, w, mean, false);
 * @endcode
 * </td>
 * <td>
 * @code
    Stat::minByRow(a); Stat::minSafeByRow(a); Stat::minByRow(a, w); Stat::minSafeByRow(a, w);
    Stat::maxByRow(a); Stat::maxSafeByRow(a); Stat::maxByRow(a, w); Stat::maxSafeByRow(a, w);
    Stat::sumByRow(a); Stat::sumSafeByRow(a); Stat::sumByRow(a, w); Stat::sumSafeByRow(a, w);
    Stat::meanByRow(a); Stat::meanSafeByRow(a); Stat::meanByRow(a, w); Stat::meanSafeByRow(a, w);
    // unbiased variance (division by n-1) when unbiased is true, false is the default
    Stat::varianceByRow(a, false); Stat::varianceSafeByRow(a, false);
    Stat::varianceByRow(a, w, false); Stat::varianceSafeByRow(a, w, false);
    // fixed mean. Must be a vector/point for arrays and a number for vectors/points
    // unbiased (false in examples below) has to be given
    Stat::varianceWithFixedMeanByRow(a, mean, false);
    Stat::varianceWithFixedMeanByRow(a, w, mean, false);
    Stat::varianceWithFixedMeanSafeByRow(a, mean, false);
    Stat::varianceWithFixedMeanSafeByRow(a, w, mean, false);
 * @endcode
 * </td></tr>
 * </table>
 *
 * @section ArraysArithmeticOperators Arithmetic Operations on Arrays/Vectors and expressions
 *
 * Available operations on arrays/vectors and expressions are summarized in the next table.
 * Many operations are similar to the operations implemented
 * in <a href="eigen.tuxfamily.org/">Eigen</a> library.
 *
 * <table class="manual">
 * <tr>
 * <td> add, subtract, divide, multiply, etc. arrays term by term</td>
 * <td>
 * @code
 *    m= m1+m2; m+= m1;
 *    m= m1-m2; m-= m1;
 *    m= m1/m2; m/= m1;
 *    m= m1.prod(m2);   // don't use m1*m2 if you want a product element by element
 *    m= m1%m2; m%= m1; // work only with integer values
 *    m= m1&&m2; m= m1||m2;       // logical operations
 *    m= m1&m2; m=m1|m2; m=m1^m2; // bitwise operations, work only with integers
 * @endcode
 * </td>
 * </tr>
 * <tr>
 *
 * <td> add, subtract, divide, multiply, etc. by a number</td>
 * <td>
 * @code
    m= m1+s; m= s+m1; m+= s;
    m= m1-s; m= s-m1; m-= s;
    m= m1/s; m= s/m1; m/= s;
    m= m1*s; m= s*m1; m*= s;
    m= m1%s; m= s%m1; m%= s; // work only with integers
    bool f;
    m= m1&&f; m= m1||f;      // logical operations
    int r;
    m= m1&r; m=m1|r; m=m1^r; // bitwise operations, work only with integers
 * @endcode
 * </td>
 * </tr>
 * <tr>
 *
 * <td> matrix by matrix/vector products</td>
 * <td>
 * @code
 *   ArrayXX m2(5,4), m1(4,5), m; PointX p2(5), p1(4), p; VectorX v2(4), v1(5), v;
 *   Real s = p2 * v1; // dot product
 *   v= m2*v2; v= m1 * p2.transpose(); // get a vector
 *   p= p2*m2; p= v1.transpose()*m2;   // get a point (row-vector)
 *   m= m2*m1; m = m1.transpose() * m; // matrix multiplication
 *   m= m2.prod(m1.transpose());       // product element by element
 *   m*=m1;                            // same as m = m * m1
 * @endcode
 * </td>
 * </tr>
 * <tr>
 *
 * <td>Comparisons operators between m and an array or a value </td>
 * <td>
 * @code
    Real s;
    // count for each columns the number of true comparisons
    (m1 < m2).count();  (m1 > m2).count();  (m1 < s).count();  (m1 > s).count();
    (m1 <= m2).count(); (m1 >= m2).count(); (m1 <= s).count(); (m1 >= s).count();
    (m1 == m2).count(); (m1 != m2).count(); (m1 == s).count(); (m1 != s).count();
 * @endcode
 * </td></tr>
 *
 * <tr>
 * <td> Probabilities and related operations</td>
 * <td>
 * @code
    Law::Normal l(0,1);
    // these methods return an array of the same size as m
    m.pdf(l);   // compute pdf values to m using distribution l
    m.lpdf(l);  // compute log-pdf values to m using distribution l
    m.cdf(l);   // compute cdf values using distribution l
    m.lcdf(l);  // compute log-cdf values using distribution l
    m.cdfc(l);  // compute complementary cdf values using distribution l
    m.lcdfc(l); // compute log-complementary cdf values using distribution l
    m.icdf(l);  // compute inverse cdf values using distribution l
 * @endcode
 * </td></tr>
 *
 * <tr>
 * <td> Mathematical functions</td>
 * <td>
 * @code
 *   m.isNa();       // boolean expression with true if m(i,j) is a NA value
 *   m.isFinite();   // boolean expression with true if m(i,j) is a finite value
 *   m.isInfinite(); // boolean expression with true if m(i,j) is an infinite value
 *   m.neg();        // Type expression with !m(i,j)
 *   m.abs();        // Type expression with abs(m(i,j))
 *   m.sqrt();       // Type expression with sqrt(m(i,j))
 *   m.log();        // Type expression with log(m(i,j))
 *   m.exp();        // Type expression with exp(m(i,j))
 *   m.pow(number);  // Type expression with m(i,j)^number
 *   m.square();     // Type expression with m(i,j)*m(i,j)
 *   m.cube();       // Type expression with m(i,j)*m(i,j)*m(i,j)
 *   m.inverse();    // Type expression with 1/m(i,j)
 *   m.sin();        // Type expression with sin(m(i,j))
 *   m.cos();        // Type expression with cos(m(i,j))
 *   m.tan();        // Type expression with tan(m(i,j))
 *   m.asin();       // Type expression with asin(m(i,j))
 *   m.acos();       // Type expression with acos(m(i,j))
 * @endcode
 * </td></tr>
 * <tr>
 * <td> Miscellaneous functions</td>
 * <td>
 * @code
    m.cast<OtherType>();        // expression with static_cast<OtherType>(m(i,j)))
    m.funct0<Functor>();        // expression with Functor(m(i,j)))
    m.funct1<Functor>(functor); // same thing with an instance of Functor given
 * @endcode
 * </td></tr>
 * </table>
 *
 * @section ArraysPredefinedTypes Convenience typedef
 *
 * There exists predefined typedef for the arrays that can be used.
 * Hereafter we give a sample for the CArray and Array2D classes
 * <table class="manual">
 * <tr><th>CArrays</th> <th>Array2D</th> </tr>
 * <tr><td>
 * @code
 * // CArray with Real (double by default) data
 * typedef CArray<Real, UnknownSize, UnknownSize, Arrays::by_col_> CArrayXX;
 * typedef CArray<Real, UnknownSize, 2, Arrays::by_col_>           CArrayX2;
 * typedef CArray<Real, UnknownSize, 3, Arrays::by_col_>           CArrayX3;
 * typedef CArray<Real, 2, UnknownSize, Arrays::by_col_>           CArray2X;
 * typedef CArray<Real, 3, UnknownSize, Arrays::by_col_by>         CArray3X;
 * typedef CArray<Real, 2, 2, Arrays::by_col_>                     CArray22;
 * typedef CArray<Real, 3, 3, Arrays::by_col_>                     CArray33;
 * // CArray with double data (add d to the type name)
 * typedef CArray<double, UnknownSize, UnknownSize, Arrays::by_col_> CArrayXXd;
 * ...
 * // CArray with int data (add i to the typename)
 * typedef CArray<int, UnknownSize, UnknownSize, Arrays::by_col_>  CArrayXXi;
 * ...
 * // Arrays with data stored by rows, the same as above with ByRow added
 * typedef CArray<Real, UnknownSize, UnknownSize, Arrays::by_row_> CArrayByRowXX;
 * ...
 * // CArraySquare (like CArray with the same number of rows and columns)
 * typedef CArraySquare<Real, UnknownSize, Arrays::by_col_>   CSquareX;
 * typedef CArraySquare<Real, 2, Arrays::by_col_>             CSquare2;
 * ...
 * typedef CArraySquare<int, 3, Arrays::by_col_>              CSquare3i;
 * ...
 * typedef CArraySquare<Real, UnknownSize, Arrays::by_row_>   CSquareByRowX;
 * @endcode
 * </td>
 * <td>
 * @code
 *   typedef Array2D<Real>   ArrayXX;
 *   typedef Array2D<double> ArrayXXd;
 *   typedef Array<int>      ArrayXXi;
 *   typedef Array<float>    ArrayXXf;
 * @endcode
 * </td></tr>
 * </table>
 * The predefined type STK::Real can be either @c double (the default) or @c float.
 *
 * For the other kind of containers there exists also predefined types
 * <table class="example">
 * <tr><th>CArrays family</th> <th>Array2D family</th> </tr>
 * <tr><td>
 * @code
 * typedef CArrayVector<Real, UnknownSize, Arrays::by_col_>   CVectorX;
 * typedef CArrayVector<Real, 2, Arrays::by_col_>             CVector2;
 * typedef CArrayVector<Real, 3, Arrays::by_col_>             CVector3;
 * typedef CArrayVector<double, UnknownSize, Arrays::by_col_> CVectorXd;
 * typedef CArrayVector<double, 2, Arrays::by_col_>           CVector2d;
 * typedef CArrayVector<double, 3, Arrays::by_col_>           CVector3d;
 * typedef CArrayVector<int, UnknownSize, Arrays::by_col_>    CVectorXi;
 * typedef CArrayVector<int, 2, Arrays::by_col_>              CVector2i;
 * typedef CArrayVector<int, 3, Arrays::by_col_>              CVector3i;
 * // CArrayPoint
 * typedef CArrayPoint<Real, UnknownSize, Arrays::by_col_>   CPointX;
 * typedef CArrayPoint<Real, 2, Arrays::by_col_>             CPoint2;
 * typedef CArrayPoint<Real, 3, Arrays::by_col_>             CPoint3;
 * ...
 * typedef CArrayPoint<int, 3, Arrays::by_col_>              CPoint3i;
 * @endcode
 * </td>
 * <td>
 * @code
 *   typedef Array2DPoint<Real>    PointX;
 *   typedef Array2DPoint<double>  PointXd;
 *   typedef Array2DVector<Real>   VectorX;
 *   typedef Array2DVector<double> VectorXd;
 *   typedef Array2DDiagonal<Real> ArrayDiagonalX;
 *   typedef Array2DDiagonal<int>  ArrayDiagonalXi;
 * @endcode
 * </td></tr>
 * </table>
 *
 **/
