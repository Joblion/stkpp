/*
 * tuto_Clustering.h
 *
 *  Created on: 25 oct. 2013
 *      Author: iovleff
 */


/** @page TutorialClustering2 Clustering: How to add new mixture models (Part 2)
 *
 * @section ClusterAdvanced Integration of new models from scratch
 *
 * This tutorial will show how to add a mixture model to STK++ from the ground.
 * It suppose you are familiar with the STK++ library and that you have already
 * read the first tutorial @link TutorialClustering1 @endlink.
 *
 **/
