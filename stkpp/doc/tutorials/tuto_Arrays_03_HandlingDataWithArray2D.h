/* Arrays tutorial: how to modify arrays
 **/

/** @page UsingArray2D Arrays Tutorial 3 : Using Array2D
 *
 *  This page explain how you can add and or remove columns/rows to an array of
 *  the Array2D family and how to merge arrays with the same number of rows
 *  without duplicating data.
 *
 * @section Array2DDesctiption General features of the Arrays2D
 *
 * The Array2D is a family of array/vectors/points propose many functionnalities
 * allowing to assd/remove rows/columns. It is also possible to resize them
 * in a conservative way. It is also possible to merge them if the dimension
 * agreed.
 *
 * Each columns in an Array2D is in a contiguous memory block. Thus, all operations
 * on the columns is fast as it does not involve data copy. This is not the case
 * for the rows. Inserting or appending rows will cause in the best cases a copy
 * of the data. In the worse case, that is, if the number of rows required is
 * greater than the capacities of the rows, it will even cause a re-allocation
 * of a part or all the columns.
 *
 * @section AddingRowsAndColumns Adding rows and columns to Array2D
 *
 * All the Array2D (That is the arrays deriving from the interface
 * class STK::IArray2D) can add, insert, remove rows or columns to an existing
 * data set.
 *
 * <ul>
 * <li> A first example with the columns
 * <table class="example">
 * <tr><th>Adding columns</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoAddingCols.cpp
 * </td>
 * <td>
 * \verbinclude tutoAddingCols.out
 * </td></tr>
 * </table>
 *
 * <li> A Second example with the rows
 * <table class="example">
 * <tr><th>Adding rows</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoAddingRows.cpp
 * </td>
 * <td>
 * \verbinclude tutoAddingRows.out
 * </td></tr>
 * </table>
 * </ul>
 *
 * @note It is important to observe that adding/inserting columns to an Array2D
 * does not produce any copy of the data stored, while the same operations
 * involving the rows of the array will produce data movement.
 *
 * The next graphic give the execution time of the following code
 * @code
 *    A.pushFrontCols(Const::VectorX(m));
 *    A.insertCols(2, 2);
 *    A.col(2) = A.col(1).square();
 *    A.col(3) = A.col(1).cube();
 *    A.pushBackCols(2);
 *    A.col(5) = A.col(4).square();
 *    A.col(6) = A.col(4).cube();
 * @endcode
 * and of the same code by replacing "Cols" by "Rows". The size of the array
 * @c A was (m,m). It is easily observed that adding rows is time consuming.
 * Observe also that a small amount of space is added at the creation of the
 * object and that the usage of the @c reserve method can be wise in order to
 * save time.
 *
 * @image html benchAdd.png
 *
 * @section RemovingRowsAndColumns Removing Rows and Columns
 *
 * Rows and columns can be also removed from arrays of the Array2D family
 * using @c remove and @c popBack methods.
 *
 * <table class="example">
 * <tr><th>Removing rows and columns</th><th>Output:</th></tr>
 * <tr><td>
 * \include tutoRemoveRowsAndCols.cpp
 * </td>
 * <td>
 * \verbinclude tutoRemoveRowsAndCols.out
 * </td></tr>
 * </table>
 *
 * @section MergingArrays2D Merging two arrays
 *
 * It is possible to merge an array with an other array or vector without
 * copying the data using the merge method.
 *
 * <table class="example">
 * <tr><th>Merge an Array2D with an Array and a Vector</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoMergeArrays2D.cpp
 * </td>
 * <td>
 * \verbinclude tutoMergeArrays2D.out
 * </td></tr>
 * </table>
 *
 **/
