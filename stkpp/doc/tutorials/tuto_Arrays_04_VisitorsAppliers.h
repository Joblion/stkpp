/* Arrays tutorial: how to use
 **/

/** @page ArraysVisitorsApplyers Arrays Tutorial 4 : Using Visitors, Appliers and Functors
 *
 * This page explains STK++'s visitors and appliers and how they
 * can be used on the arrays and in expressions.
 *
 * @section TutorialReductionsAppliers Visitors
 *
 * A visitor is a constant function applied to an expression or array
 * returning a single value. One of the most useful visitor is ExprBase::sum(),
 * returning the sum of all the coefficients of a given expression or array.
 *
 * Visitors can also be used to obtain the location of an element
 * inside an Expression or Array. This is the case  of ExprBase::maxElt(i,j),
 * ExprBase::maxElt(i), ExprBase::minElt(i,j) and ExprBase::minElt(i), which
 * can be used to find the location of the greatest or smallest coefficient in
 * an Expression or an Array.
 *
 * The @c minElt method should not be confunded with the @c min method which compute
 * the minimum between two arrays.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoVisitors.cpp
 * </td>
 * <td>
 * \verbinclude tutoVisitors.out
 * </td></tr>
 * </table>
 *
 * @section TutorialSlicingVisitors Slicing Visitors
 *
 * A visitor can also be applied on each column/row of an Expression
 * or Array by using global functions in the STK namespace. All the visitor
 * can be used as slicing visitors except the @c minElt with arguments.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoSlicingVisitors.cpp
 * </td>
 * <td>
 * \verbinclude tutoSlicingVisitors.out
 * </td></tr>
 * </table>
 *
 * @section TutorialAppliers Appliers
 *
 * An applier is like a visitor except that it can only be applied to arrays
 * as it will modify the content of the array. Slicing appliers does not have
 * any meaning and thus an applier can only be used on a whole expression. If
 * you need to use an applier on a row or column use the @c col or @c row method.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoAppliers.cpp
 * </td>
 * <td>
 * \verbinclude tutoAppliers.out
 * </td></tr>
 * </table>
 *
 * @section TutorialFunctors Functors
 *
 * Functors can fundamentally be used in the same way that slicing visitors. The
 * only difference is that they return an STK::Array2DPoint if they are applied
 * by column or an STK::Array2DVector if they are applied by rows. It is possible
 * to use the move method in order to store the results of the computation
 * without data copy.
 *
 * All the functors are in the STK::Stat namespace and compute the usual statistics
 * by columns.  If there is the possibility of missing/NaN values add the word
 * Safe to the name of the functor. If the mean by row is needed, just add ByRow
 * to the name of the functor. If you want both add SafeByRow.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoStatFunctors.cpp
 * </td>
 * <td>
 * \verbinclude tutoStatFunctors.out
 * </td></tr>
 * </table>
 *
 **/
