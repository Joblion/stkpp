/* Arrays tutorial: how to use
 **/

/** @page ArraysSlicing Arrays Tutorial 5 : Slicing arrays and expressions
 *
 * This page explains the slicing operations.
 * A slice is a rectangular part of an array or expression.
 * Arrays slices can be used both as rvalues and as lvalues while Expression slice
 * can only be used as rvalue.
 *
 * @section TutorialRowsAndColumns Rows and Columns
 *
 * Individual columns and rows of arrays and expressions can be accessed using
 * STK::ExprBase::col() and STK::ExprBase::row() as rvalue
 * and using STK::ArrayBase::col() and STK::ArrayBase::row() methods as
 * lvalue. The argument for col() and row() is the index of the column or row
 * to be accessed.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoRowsAndCols.cpp
 * </td>
 * <td>
 * \verbinclude tutoRowsAndCols.out
 * </td></tr>
 * </table>
 *
 * @section TutorialSubArraysAndVectors Sub-Arrays and sub-Vectors
 *
 * The most general sub operation  is called sub(). There are two versions, one
 * for arrays, an other for vectors. The sub-Arrays and sub-vectors can be
 * employed as rvalue or lvalue, meaning you can assign or reference a
 * sub-array or sub-vector.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoSubArrays.cpp
 * </td>
 * <td>
 * \verbinclude tutoSubArrays.out
 * </td></tr>
 * </table>
 *
 **/
