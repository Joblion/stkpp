/*
 * tuto_Clustering.h
 *
 *  Created on: 25 oct. 2013
 *      Author: iovleff
 */


/** @page TutorialSTatisitK STatistiK: Tools for usual statistics
 *
 * @section STatistiKPresentation Presentation
 *
 * This project contains functions and classes for statistical calculations, kernel
 * computations and usual distribution computations (as probability distribution computation,
 * random number generation...)
 *
 * It is divided in three sub-projects :
 * - The StatDesc sub-project contains functions and classes for computing the
 * usual descriptive statistics,
 * - The Kernel sub-project contains functions and classes for computing the
 * usual kernels,
 * - The Law sub-project contains functions and classes for usual probability
 * density functions (or discrete probability distribution) computations,
 * cumulative probability distribution, inverse cumulative distribution
 * computations (quantile), random number generation.
 *
 * @section STatistiKDistributionsLaws Distributions laws
 *
 * Density, log-density, cumulative distribution function, quantile function and
 * random variate generation for many standard probability distributions are
 * available in the Law sub-project. The complete list is
 * - @link STK::Law::Bernoulli Bernoulli @endlink distribution,
 * - @link STK::Law::Binomial Binomial @endlink distribution,
 * - @link STK::Law::Categorical Categorical @endlink distribution,
 * - @link STK::Law::Cauchy Cauchy @endlink distribution,
 * - @link STK::Law::ChiSquared Chi-squared @endlink distribution,
 * - @link STK::Law::Exponential Exponential @endlink distribution,
 * - @link STK::Law::FisherSnedecor Fisher-Snedecor @endlink distribution,
 * - @link STK::Law::Gamma Gamma @endlink distribution,
 * - @link STK::Law::Geometric Geometric @endlink distribution,
 * - @link STK::Law::HyperGeometric Hyper-Geometric @endlink distribution,
 * - @link STK::Law::Normal Normal @endlink distribution,
 * - @link STK::Law::Student t-Student @endlink distribution,
 * - @link STK::Law::Uniform Uniform @endlink distribution,
 * - @link STK::Law::Weibull Weibull @endlink distribution.
 *
 * All these distributions are implemented in the namespace STK::Law and derive
 * from the Interface base class @link STK::Law::IUnivLaw IUnivLaw @endlink
 * and should implement the following pure virtual methods :
 * @code
 * virtual Type rand() const =0;
 * virtual Real pdf(Type const& x) const =0;
 * virtual Real lpdf(Type const& x) const = 0;
 * virtual Real cdf(Real const& t) const =0;
 * virtual Real cdfc(Real const& t) const =0;
 * virtual Type icdf(Real const& p) const=0;
 * @endcode
 *
 * @section STatistiKMultiDistributionsLaws Multivariate Distributions laws
 *
 * Support for some multivariate distribution laws is given.
 *
 **/
