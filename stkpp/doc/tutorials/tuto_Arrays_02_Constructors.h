/* Arrays tutorial: how to modify arrays
 **/

/** @page  ArraysConstructors Arrays Tutorial 2 : The Constructors
 *
 * Constructors depend of the structure of the array you want to obtain. In
 * this tutorial we will review all the existing arrays and the different way
 * to create them.
 *
 * @section AllArraysClasses Review of the classes
 *
 * <table border >
 * <tr> <th> class </th> <th> Description </th> </tr>
 * <tr>
 * <td>
 * @code
 * template<typename Type> class Array2D;
 * template< typename Type, int SizeRows_ = UnknownSize, int SizeCols_ = UnknownSize, bool Orient_ = Arrays::by_col_>
 * class CArray;
 * @endcode
 * </td>
 * <td>
 * The @ref STK::Array2D and @ref STK::CArray classes are the most general
 * classes for storing scalars. The values are stored in anarray of range
 * (beginRows:lastIdxRows)x(beginCols:lastIdxCols).
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * @code
 * template<typename Type> Array2DVector;
 * template<typename Type> Array2DPoint;
 * template< typename Type, int SizeRows_=UnknownSize, bool Orient_ = Arrays::by_col_>
 * class CArrayVector;
 * template< typename Type, int SizeCols_=UnknownSize, bool Orient_ = Arrays::by_row_>
 * class CArrayPoint;
 * @endcode
 * </td>
 * <td>
 * The @ref STK::Array2DVector, @ref STK::Array2DPoint, @ref STK::CArrayVector
 * and @ref STK::CArrayPoint classes allow to store scalars in column and row
 * vectors. The values are stored in arrays of range (begin:lastIdx).
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * @code
 * template<typename Type> Array2DSquare;
 * template< typename Type_, int Size_ = UnknownSize, bool Orient_ = Arrays::by_col_>
 * class CArraySquare;
 * @endcode
 * </td>
 * <td>
 * The @ref STK::Array2DSquare class is a general class for storing scalars in
 * a square matrix. The values are stored in an array of range
 * (begin:lastIdx)x(begin:lastIdx).
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * @code
 * template<typename Type> Array2DDiagonal;
 * @endcode
 * </td>
 * <td>
 * The @ref STK::Array2DDiagonal class is a general class for storing scalars in
 * a diagonal matrix. The values are stored in an array of range
 * (begin:lastIdx)x(begin:lastIdx) with zero outside the diagonal.
 * @note Only the diagonal values are effectively stored in an Array2DDiagonal
 * array.
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * @code
 * template<typename Type> Array2DUpperTriangular;
 * template<typename Type> Array2DLowerTriangular;
 * @endcode
 * </td>
 * <td>
 * The @ref STK::Array2DUpperTriangular and  STK::Array2DLowerTriangular classes are
 * general classes for storing scalars in triangular matrices.
 * The values are stored in an array of range
 * (beginRows:lastIdxRows)x(beginCols:lastIdxCols) with zero in the
 * respectively lower and upper part of the array.
 * @note Only the non-zeros values are effectively stored in the arrays and
 * the upper part (respectively the lower part) is the part of the array such
 * that j>i (resp. i>j), where i represents the index of the row and j the index
 * of the column.
 * </td>
 * </tr>
 *
 * </table>
 *
 * @section AllArray2DConstructors Constructors for the Array2D family containers
 *
 * <table border >
 * <tr> <th>Constructors </th> <th> Examples </th> </tr>
 * <tr>
 * <td>
 * Default constructors. Create an empty Array of scalar @c Type.
 * @code
 * Array2D<Type>();
 * Array2DVector<Type>();
 * Array2DPoint<Type>();
 * Array2DSquare<Type>();
 * Array2DDiagonal<Type>();
 * Array2DLowerTriangular<Type>();
 * Array2DUpperTriangular<Type>();
 * @endcode
 * </td>
 * <td>
 * In the following example
 * @code
 * Array2D<Real> a; // a is an empty array of Real (double by default)
 * a.resize(10,10); a=0.; // a is now an array of size 10x10 with the value 0.
 * Array2DVector<Real> b; // a is an empty column vector of Real
 * b.resize(10); b=0.; // b is now an array of size 10x1 with the value 0.
 * @endcode
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * Construct an array with fixed ranges for the rows and the columns and
 * Optionally an initial value.
 * @code
 * Array2D<Type>( Range const& I, Range const& J);
 * Array2D<Type>( Range const& I, Range const& J, Type const& v);
 * Array2DVector<Type>( Range const& I);
 * Array2DVector<Type>( Range const& I, Type const& v);
 * Array2DPoint<Type>( Range const& J);
 * Array2DPoint<Type>( Range const& J, Type const& v);
 * Array2DSquare<Type>( Range const& I);
 * Array2DSquare<Type>( Range const& I, Type const& v);
 * Array2DDiagonal<Type>( Range const& I);
 * Array2DDiagonal<Type>( Range const& I, Type const& v);
 * Array2DLowerTriangular<Type>( Range const& I, Range const& J);
 * Array2DLowerTriangular<Type>( Range const& I, Range const& J, Type const& v);
 * Array2DUpperTriangular<Type>( Range const& I, Range const& J);
 * Array2DUpperTriangular<Type>( Range const& I, Range const& J, Type const& v);
 * @endcode
 * </td>
 * <td>
 * The following code build an array of Real (double by default) with rows in
 * the range 2:4 and columns in the range 0:9 (this is the default, but can be
 * 1:10 if the macro STKBASEARRAYS=1 is activated). The array is initialized with
 * the value 2.
 * @code
 * Array2D<Real> a(Range(2,3), 10, 2.);
 * @endcode
 * The following code build a vector of Real (double by default) with columns
 * in the range 0:9 (this is the default, but can be 1:10 if the macro STKBASEARRAYS=1
 * is activated). The array is initialized with the value 2.
 * @code
 * Array2DVector<Real> a( 10, 2.);
 * @endcode
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * Copy constructor. The resulting array is either a copy of the array @c T (if @c ref == @c false)
 * or a reference of the array @c T  (if @c ref == @c true)
 * @code
 * Array2D<Type>( const Array2D<Type> &T, bool ref=false);
 * Array2DVector<Type>( const Array2DVector<Type> &T, bool ref=false);
 * Array2DPoint<Type>( const Array2DPoint<Type> &T, bool ref=false);
 * Array2DSquare<Type>( const Array2DSquare<Type> &T, bool ref=false);
 * Array2DDiagonal<Type>( const Array2DDiagonal<Type> &T, bool ref=false);
 * Array2DLowerTriangular<Type>( const Array2DLowerTriangular<Type> &T, bool ref=false);
 * Array2DUpperTriangular<Type>( const Array2DUpperTriangular<Type> &T, bool ref=false);
 * @endcode
 * </td>
 *
 * <td>
 * The following code copy the array @c a in a new array @c b and create a
 * reference of the rows and columns of @c c with range I for the rows and
 * range J for the columns.
 * @code
 * Array2D<Real> a(10, 8);
 * Array2D<Real> b(a); // b is now a copy of a
 * // c is a reference, if an element of c is modified, the corresponding element of a is modified
 * Array2D<Real> c(a.sub(I,J), true);
 * @endcode
 * The following show how to get a reference on a column and a reference on a row
 * of an Array2D.
 * @code
 * Array2D<Real> a(10, 8);
 * Array2DVector<Real> c(a.col(1), true); // c is now a reference on the column 1 of a
 * Array2DPoint<Real> p(a.row(1), true); // p is now a reference on the row 1 of a
 * @endcode
 * </td>
 * </tr>
 * </table>
 *
 * @section AllCArrayConstructors Constructors for the CArray family containers
 *
 * <table border >
 * <tr> <th>Constructors </th> <th> Examples </th> </tr>
 * <tr>
 * <td>
 * Default constructors. Create an empty Array of scalar @c Type.
 * @code
 * CArray<Type>();
 * Array2DVector<Type>();
 * CArrayPoint<Type>();
 * CArraySquare<Type>();
 * @endcode
 * </td>
 * <td>
 * In the following example
 * @code
 * CArray<Real> a; // a is an empty array of Real (double by default)
 * a.resize(10,10); a=0.; // a is now an array of size 10x10 with the value 0.
 * CArrayVector<Real> b; // a is an empty column vector of Real
 * b.resize(10); b=0.; // b is now an array of size 10x1 with the value 0.
 * @endcode
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * Construct an array with fixed ranges for the rows and the columns and
 * Optionally an initial value.
 * @code
 * CArray<Type>( Range const& I, Range const& J);
 * CArray<Type>( Range const& I, Range const& J, Type const& v);
 * CArrayVector<Type>( Range const& I);
 * CArrayVector<Type>( Range const& I, Type const& v);
 * CArrayPoint<Type>( Range const& J);
 * CArrayPoint<Type>( Range const& J, Type const& v);
 * CArraySquare<Type>( Range const& I);
 * CArraySquare<Type>( Range const& I, Type const& v);
 * @endcode
 * </td>
 * <td>
 * The following code build an array of Real (double by default) with rows in
 * the range 2:4 and columns in the range 0:9 (this is the default, but can be
 * 1:10 if the macro STKBASEARRAYS=1 is activated). The array is initialized with
 * the value 2.
 * @code
 * CArray<Real> a(Range(2,3), 10, 2.);
 * @endcode
 * The following code build a vector of Real (double by default) with columns
 * in the range 0:9 (this is the default, but can be 1:10 if the macro STKBASEARRAYS=1
 * is activated). The array is initialized with the value 2.
 * @code
 * CArrayVector<Real> a( 10, 2.);
 * @endcode
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * Copy constructor. The resulting array is either a copy of the array @c T (if @c ref == @c false)
 * or a reference of the array @c T  (if @c ref == @c true)
 * @code
 * CArray<Type>( const CArray<Type> &T, bool ref=false);
 * CArrayVector<Type>( const CArrayVector<Type> &T, bool ref=false);
 * CArrayPoint<Type>( const CArrayPoint<Type> &T, bool ref=false);
 * CArraySquare<Type>( const CArraySquare<Type> &T, bool ref=false);
 * @endcode
 * </td>
 *
 * <td>
 * The following code copy the array @c a in a new array @c b and create a
 * reference of the rows and columns of @c c with range I for the rows and
 * range J for the columns.
 * @code
 * CArray<Real> a(10, 8);
 * CArray<Real> b(a); // b is now a copy of a
 * // c is a reference, if an element of c is modified, the corresponding element of a is modified
 * CArray<Real> c(a.sub(I,J), true);
 * @endcode
 * The following show how to get a reference on a column and a reference on a row
 * of an CArray.
 * @code
 * CArray<Real> a(10, 8);
 * CArrayVector<Real> c(a.col(1), true); // c is now a reference on the column 1 of a
 * CArrayPoint<Real> p(a.row(1), true); // p is now a reference on the row 1 of a
 * @endcode
 * </td>
 * </tr>
 *
 * </table>
 *
 **/
