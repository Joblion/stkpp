/* Arrays tips: how to modify arrays
 **/

/** @page  TipsComputeMeanByColumn Tips: Compute the mean for each column of an array
 *
 * You can easily get the mean of a whole vector or a matrix containing
 * missing values using the expression
 * @code
 *   CArray<Real> A(100, 20);
 *   Law::Normal law(1,2);
 *   A.rand(law);
 *   Real m = A.meanSafe();
 * @endcode
 *
 * In some cases you may want to get the mean for each column of an array
 * with missing values. You can get it in a @c PointX vector using
 * either the code
 * @code
 *   PointX m;
 *   m = meanByCol(A.safe()); // mean(A.safe()); work too
 * @endcode
 * or the code
 * @code
 *   Array2DPoint<Real> m;
 *   m.move(Stat::mean(A.safe()));
 * @endcode
 *
 * The method @c A.safe() will replace any missing (or NaN) values by zero.
 * In some cases it's not sufficient, Suppose you know your data are all
 * positive and you want to compute the log-mean of your data. In this case,
 * you will rather use
 * @code
 *   m = Stat::mean(A.safe(1.).log());
 * @endcode
 * and all missing (or NaN) values will be replaced by one.
 *
 * @note You can also compute the variance. If you want to compute the mean of
 * each row, you will have to use the functor Stat::meanByRow. In this latter
 * case, VectorX as a result.
 *
 * @sa STK::Stat::mean, STK::Stat::meanByRow, STK::Stat::variance, STK::Stat::varianceByRow,
 * STK::Stat::varianceWithFixedMean
 **/
