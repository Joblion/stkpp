/* Arrays tutorial*/

/** @page ArraysRefArrays Arrays Tutorial 6 : Using reference Arrays
 *
 * This page explains how to create reference on existing arrays.
 *
 * @section TutorialReferenceSubArray Creating reference to sub-arrays
 *
 * Any slice of an existing array can be referenced by an other array
 * and modified using this "reference" container.
 *
 * <table class="example">
 * <tr><th>Example</th><th>Output</th></tr>
 * <tr><td>
 * \include tutoReference.cpp
 * </td>
 * <td>
 * \verbinclude tutoReference.out
 * </td></tr>
 * </table>
 *
 * This method allow to get a safe access to some sub-part of a big array that
 * you want to modify.
 *
 *
 * @section TutorialReferenceRaw Creating reference to raw data
 *
 * It is possible to map a single pointer Type* or a double pointer Type**
 * in respectively CArrays and Array2Ds using the constructors.
 *
 * <table border >
 * <tr> <th>Wrapper Constructors </th> <th> Examples </th> </tr>
 * <tr>
 * <td>
 * Wrap the data pointed by @c q in an Array with rows in
 * the range I and columns in the range J.
 * @code
 * Array2D<Type>( Type** q, Range const& I, Range const& J);
 * @endcode
 * </td>
 * <td>
 * In this example we create an array of size (10, 5) and wrap it in an Array2.
 * @code
 * Real** q = new Real*[10];
 * for (int j=0; j<10; j++) { q[j] = new Real[5];}
 * //wrap q. Note that q will not be freed when a is deleted
 * Array2D<Real> a(q, Range(0,10), Range(0,5)); //
 * @endcode
 * </td>
 * </tr>
 *
 * <tr>
 * <td>
 * Wrapper constructor. Wrap the data pointed by @c q in an Array with rows
 * and columns of size m and n.
 * @code
 * CArray<Type>( Type* q, int m, int n);
 * @endcode
 * </td>
 * <td>
 * In this example we create an array of size (10, 5) and wrap it in an Array2D.
 * @code
 * Real* q = new Real[10];
 * for (int j=0; j<10; j++) { q[j] = j;}
 * //wrap q in a fixed size array of size (2,5).
 * // Note that q will not be freed when a is deleted
 * CArray<Real, 2, 5> a(q, 2, 5);
 * @endcode
 * </td>
 * </tr>
 *
 * </table>
 *
 **/
