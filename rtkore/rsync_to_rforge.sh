#/bin/sh
rsync --delete -avz -L \
      --exclude="makefile" \
      --exclude="rsync_to_rforge.sh" \
      --exclude=".svn" \
      --exclude=".gitignore" \
      --exclude=".settings" \
      --exclude="*.Rproj" \
      --exclude=".R*" \
      --exclude=".cproject" \
      --exclude=".project" \
      --filter "- *.log" \
      --filter "- *.aux" \
      --filter "- *.toc" \
      --filter "- *.gz" \
      --filter "- *.bbl" \
      --filter "- *.a" \
      --filter "- *.o" \
      --filter "- *.so"\
      --filter "- *.local" \
      . ../../../workspace/rtkore-rforge/
